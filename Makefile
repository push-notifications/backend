###################  Docker development helpful directives  ####################
#
# Usage:
# make lint						# runs lint script
# make logs           # displays log outputs from running services
# make env            # build environment, create and start containers
# make dbdump					# fills the local deployment's DB with the current data of the https://notifications-dev.web.cern.ch/

lint:
	npm run lint
.PHONY: lint

local:
	npm install && npm start
.PHONY: local

local-staging:
	npm install && npm run start:staging
.PHONY: local

shared-network: 
	docker network create shared || true
.PHONY: shared-network

delete-shared-network:
	docker network rm shared || true
.PHONY: delete-shared-network

shared-services:
	docker-compose -f docker-compose-services-shared.yml up -d --remove-orphans
.PHONY: shared-services

stop-shared-services:
	docker-compose -f docker-compose-services-shared.yml down --volumes
.PHONY: stop-shared-services

delete-shared-services: stop-shared-services
	docker-compose -f docker-compose-services-shared.yml rm -f
.PHONY: delete-shared-services

delete-shared: delete-shared-network
	docker-compose -f docker-compose-services-shared.yml rm -f
.PHONY: delete-shared

env-shared: shared-network shared-services local
.PHONY: docker-env-local

env-shared-staging: shared-network shared-services local-staging
.PHONY: docker-env-staging

env:
	docker-compose -f docker-compose.yml up -d --remove-orphans && \
	cd src && npm install && npm start
.PHONY: env

# It's necessary to sed the gen_random_uuid() due to https://github.com/typeorm/typeorm/issues/7700
dbdump:
	pg_dump -h dbod-pnsdev.cern.ch -p 6602 -U admin push_dev --column-inserts > schema.sql && \
	sed -i 's/public.gen_random_uuid()/gen_random_uuid()/g' ./schema.sql
.PHONY: dbdump

env-full:
	docker-compose -f docker-compose.full.yml up --remove-orphans
.PHONY: env-full

logs:
	docker-compose -f docker-compose.full.yml logs -f
.PHONY: logs

destroy-env:
	docker-compose -f docker-compose.full.yml down --volumes
	docker-compose -f docker-compose.full.yml rm -f
.PHONY: destroy-env

docker-rebuild-env:
	docker-compose -f docker-compose.full.yml build --force-rm --pull --no-cache
	docker-compose -f docker-compose.full.yml  up --force-recreate --remove-orphans
.PHONY: docker-rebuild-env

stop-env:
	docker-compose -f docker-compose.full.yml down --volumes
.PHONY: stop-env

shell-env:
	docker-compose -f docker-compose.full.yml exec notifications-backend /bin/sh
.PHONY: shell-env
