// Based on https://github.com/getsentry/sentry-javascript/blob/master/packages/integrations/src/captureconsole.ts

import { EventProcessor, Hub, Integration } from '@sentry/types';
import { fill, getGlobalObject, safeJoin, severityFromString } from '@sentry/utils';

const global = getGlobalObject<Window | NodeJS.Global>();

/** Send Console API calls as Sentry Events */
export class CustomCaptureConsole implements Integration {
  public static id = 'CustomCaptureConsole';
  public name: string = CustomCaptureConsole.id;

  private readonly _allLevels: string[] = ['log', 'info', 'warn', 'error', 'debug', 'assert'];
  private readonly _sentryLevels: string[] = ['warn', 'error'];
  private readonly _consoleLevels: string[] = ['log', 'info', 'warn', 'error', 'debug', 'assert'];

  public constructor(options: { sentryLevels?: string[]; consoleLevels?: string[] } = {}) {
    if (options.sentryLevels) {
      this._sentryLevels = options.sentryLevels;
    }
    if (options.consoleLevels) {
      this._consoleLevels = options.consoleLevels;
    }
  }

  public setupOnce(_: (callback: EventProcessor) => void, getCurrentHub: () => Hub): void {
    if (!('console' in global)) {
      return;
    }
    console.debug('Initializing logging to Sentry for ', this._sentryLevels);
    console.debug('Initializing logging to Console for ', this._consoleLevels);

    this._allLevels.forEach((level: string) => {
      if (!(level in global.console)) {
        return;
      }

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      fill(global.console, level, (originalConsoleLevel: () => any) => (...args: any[]): void => {
        // Map enabled console Log Levels
        if (this._consoleLevels.includes(level)) {
          if (originalConsoleLevel) {
            Function.prototype.apply.call(originalConsoleLevel, global.console, args);
          }
        }

        // Map enabled sentry Log Levels
        if (!this._sentryLevels.includes(level)) {
          return;
        }

        // Exclude exceptions that where already handled by the error handler
        if (level === 'error' && this._isStackTrace(args[0])) {
          return;
        }

        // log to sentry
        const hub = getCurrentHub();
        if (hub.getIntegration(CustomCaptureConsole)) {
          hub.withScope(scope => {
            scope.setLevel(severityFromString(level));
            scope.setExtra('arguments', args);
            scope.addEventProcessor(event => {
              event.logger = 'console';
              return event;
            });

            let message = safeJoin(args, ' ');
            if (level === 'assert') {
              if (args[0] === false) {
                message = `Assertion failed: ${safeJoin(args.slice(1), ' ') || 'console.assert'}`;
                scope.setExtra('arguments', args.slice(1));
                hub.captureMessage(message);
              }
            }
            if (level === 'error' && args[0] instanceof Error) {
              hub.captureException(args[0]);
            } else {
              hub.captureMessage(message);
            }
          });
        }
      });
    });
  }

  private _isStackTrace(message: any): boolean {
    if (!(typeof message === 'string' || message instanceof String)) {
      return;
    }
    const lines = message.split('\n');
    return lines.length > 1 && lines.slice(1).every(line => line.trim().startsWith('at'));
  }
}
