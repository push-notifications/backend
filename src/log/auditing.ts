import { Etcd3, SortTarget, SortOrder } from 'etcd3';
import { v4 as uuidv4 } from 'uuid';
import * as moment from 'moment';
import { NotFoundError } from 'routing-controllers';

class BaseEtcd {
  private baseClient = new Etcd3({
    hosts: `http://${process.env.ETCD_HOST || 'localhost'}:${process.env.ETCD_PORT || '2379'}`,
    auth: process.env.ETCD_USER &&
      process.env.ETCD_PASSWORD && {
        password: process.env.ETCD_PASSWORD,
        username: process.env.ETCD_USER,
      },
    grpcOptions: {
      'grpc.max_receive_message_length': 15 * 1024 * 1024,
    },
    defaultCallOptions: context =>
      context.isStream ? {} : { deadline: Date.now() + (parseInt(process.env.ETCD_DEADLINE) || 5) * 1000 },
    dialTimeout: (parseInt(process.env.ETCD_DEADLINE) || 5) * 1000,
  });

  private namespaceClient = null;
  constructor(prefix) {
    if (!process.env.AUDITING || process.env.AUDITING === 'false') {
      console.info('Audit disabled', prefix);
      return;
    }
    console.debug('etcd connecting for audit', prefix);
    this.namespaceClient = this.baseClient.namespace('/' + prefix);
  }

  public async setValue(path, value) {
    if (!this.namespaceClient) {
      console.debug('etcd setValue namespaceClient is null');
      return;
    }

    await this.namespaceClient
      .put(path)
      .value(JSON.stringify({ date: moment().format('DD/MM/YYYY HH:mm:ss'), ...value }))
      .catch(error => console.error('etcd setValue:', error.message));
  }

  async getAllValues() {
    if (!this.namespaceClient) {
      console.debug('etcd getAllValues namespaceClient is null');
      return;
    }
    const allFValues = await this.namespaceClient
      .getAll()
      .sort(SortTarget.Create, SortOrder.Ascend)
      .strings()
      .catch(err => console.error('etcd getAllValues:', err.message));
    console.debug('All keys :', allFValues);

    return allFValues;
  }

  public async getValues(prefix) {
    if (!this.namespaceClient) {
      console.debug('etcd getValues namespaceClient is null');
      return;
    }
    if (!prefix) {
      console.error('etcd getValues prefix is null');
      return;
    }
    const allFValues = await this.namespaceClient
      .getAll()
      .sort(SortTarget.Create, SortOrder.Ascend)
      .prefix(`/${prefix}`)
      .strings()
      .catch(err => console.error('etcd getValues:', err.message));

    return allFValues;
  }

  public async getValuesAsJson(prefix): Promise<JSON> {
    if (!this.namespaceClient) {
      console.debug('etcd getValues namespaceClient is null');
      return;
    }

    const allFValues = await this.getValues(prefix);
    if (!allFValues) {
      throw new NotFoundError('Audit does not exist.');
    }

    const ret: JSON = {} as JSON;
    Object.keys(allFValues).forEach(function (key) {
      const value = allFValues[key];
      const splitKeys = key.split('/');
      let tmpret = ret;
      for (let j = 0; j < splitKeys.length - 1; j++) {
        if (splitKeys[j] === '') continue;
        if (!tmpret[splitKeys[j]]) tmpret[splitKeys[j]] = {};
        tmpret = tmpret[splitKeys[j]];
      }
      try {
        tmpret[splitKeys[splitKeys.length - 1]] = JSON.parse(value);
      } catch {
        tmpret[splitKeys[splitKeys.length - 1]] = value;
      }
    });

    return ret;
  }
}

class NotificationsBase extends BaseEtcd {
  private applicationName = 'backend';

  public async setValue(notificationId, value, key?, userId?) {
    if (!key) key = uuidv4();
    const path = `/${notificationId}/${this.applicationName}/${userId ? 'target_users/' + userId + '/' : ''}${key}`;

    await super.setValue(path, value);
  }
}

class AuditNotifications extends NotificationsBase {
  private static _instance: AuditNotifications;
  constructor() {
    super('notifications');
  }
  public static get Instance() {
    return this._instance || (this._instance = new this());
  }
}

class AuditChannels extends NotificationsBase {
  private static _instance: AuditChannels;
  constructor() {
    super('channels');
  }
  public static get Instance() {
    return this._instance || (this._instance = new this());
  }
}

class AuditExternal extends NotificationsBase {
  private static _instance: AuditExternal;
  constructor() {
    super('external/notifications');
  }
  public static get Instance() {
    return this._instance || (this._instance = new this());
  }
}

class AuditIPAddress extends BaseEtcd {
  private static _instance: AuditIPAddress;
  constructor() {
    super('security');
  }
  public static get Instance() {
    return this._instance || (this._instance = new this());
  }

  public async setValue(ip, value) {
    await super.setValue(`/${ip}/${uuidv4()}`, value);
  }
}

const auditNotifications = AuditNotifications.Instance;
export { auditNotifications as AuditNotifications };
const auditChannels = AuditChannels.Instance;
export { auditChannels as AuditChannels };
const auditIPAddress = AuditIPAddress.Instance;
export { auditIPAddress as AuditIPAddress };
const auditExternal = AuditExternal.Instance;
export { auditExternal as AuditExternal };
