import * as Sentry from '@sentry/node';
import * as Tracing from '@sentry/tracing';
import * as express from 'express';
import { CustomCaptureConsole } from './custom-capture-console';
import * as process from 'process';

export function sentryInit(app) {
  Sentry.init({
    // We recommend adjusting this value in production, or using tracesSampler
    // for finer control
    //tracesSampleRate: 1.0,
    environment: process.env.NODE_ENV,
    // release: '1.0',
    defaultIntegrations: false,
    integrations: [
      // array of console methods that should be captured in sentry and console
      // sentry defaults to ['warn', 'error']
      // console defaults to ['log', 'info', 'warn', 'error', 'debug', 'assert']
      new CustomCaptureConsole({
        sentryLevels: process.env.SENTRY_SENTRY_LEVELS?.split(','),
        consoleLevels: process.env.SENTRY_CONSOLE_LEVELS?.split(','),
      }),
      new Tracing.Integrations.Express({
        // to trace all requests to the default router
        app,
      }),
      // duplicates express errors, disabled for now
      new Sentry.Integrations.OnUncaughtException(),
    ],
    debug: true,
  });
}

export function sentryHandleRequests(app) {
  // The request handler must be the first middleware on the app
  app.use(Sentry.Handlers.requestHandler() as express.RequestHandler);
}

export function sentryHandleErrors(app) {
  // The error handler must be before any other error middleware and after all controllers
  // Capture all 400 and 500 errors, and ignore what was set in  SENTRY_IGNORE_HTTP_ERRORS
  let errorsToIgnore = [401, 403];
  if (process.env.SENTRY_IGNORE_HTTP_ERRORS) {
    errorsToIgnore = process.env.SENTRY_IGNORE_HTTP_ERRORS.split(',').map(x => +x);
  }
  console.debug('Sentry ignoring http errors: ', errorsToIgnore);
  app.use(
    Sentry.Handlers.errorHandler({
      shouldHandleError(error: any) {
        let err;
        if (error.httpCode) {
          err = error.httpCode as number;
        } else if (error.status) {
          err = error.status as number;
        }

        return !(err && errorsToIgnore.includes(err));
      },
    }) as express.ErrorRequestHandler,
  );
}

/*
setTimeout(() => {
     throw new Error('My Sentry error!');
}, 99);*/
