import fetch, { AbortError, URL, RequestInit } from 'node-fetch';

const checkStatus = async response => {
  if (response.ok) {
    // response.status >= 200 && response.status < 300
    return response;
  }

  const text = await response.text();
  throw new Error(`HTTP Error Response: ${response.status} ${response.statusText} - ${text}`);
};

export async function postWithTimeout(uri: URL, options: RequestInit): Promise<any> {
  options.method = 'post';
  return fetchWithTimeout(uri, options);
}

export async function getWithTimeout(uri: URL, options: RequestInit): Promise<any> {
  options.method = 'get';
  return fetchWithTimeout(uri, options);
}

async function fetchWithTimeout(uri: URL, options: RequestInit): Promise<any> {
  const controller = new AbortController();
  options.controller = controller.signal;

  const timeout = setTimeout(() => {
    controller.abort();
  }, 5 * 1000);

  try {
    const res = await fetch(uri, options);

    await checkStatus(res);

    return res.json();
  } catch (error) {
    console.error(error);

    // if (error instanceof AbortError) {
    //   throw new Error('Request timed out.');
    // }

    throw error;
  } finally {
    clearTimeout(timeout);
  }
}

export async function deleteWithTimeout(uri: URL, options: RequestInit): Promise<any> {
  options.method = 'delete';
  return fetchWithTimeout(uri, options);
}
