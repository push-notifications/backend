export const enum StatusCodes {
  Accepted = '202',
  BadGateway = '502',
  BadRequest = '400',
  Conflict = '409',
  Continue = '100',
  Created = '201',
  ExpectationFailed = '417',
  FailedDependency = '424',
  Forbidden = '403',
  GatewayTimeout = '504',
  Gone = '410',
  HTTPVersionNotSupported = '505',
  ImATeapot = '418',
  InsufficientSpaceOnResource = '419',
  InsufficientStorage = '507',
  InternalServerError = '500',
  LengthRequired = '411',
  Locked = '423',
  MethodFailure = '420',
  MethodNotAllowed = '405',
  MovedPermanently = '301',
  MovedTemporarily = '302',
  MultiStatus = '207',
  MultipleChoices = '300',
  NetworkAuthenticationRequired = '511',
  NoContent = '204',
  NonAuthoritativeInformation = '203',
  NotAcceptable = '406',
  NotFound = '404',
  NotImplemented = '501',
  NotModified = '304',
  OK = '200',
  PartialContent = '206',
  PaymentRequired = '402',
  PermanentRedirect = '308',
  PreconditionFailed = '412',
  PreconditionRequired = '428',
  Processing = '102',
  ProxyAuthenticationRequired = '407',
  RequestHeaderFieldsTooLarge = '431',
  RequestTimeout = '408',
  RequestEntityTooLarge = '413',
  RequestURITooLong = '414',
  RequestedRangeNotSatisfiable = '416',
  ResetContent = '205',
  SeeOther = '303',
  ServiceUnavailable = '503',
  SwitchingProtocols = '101',
  TemporaryRedirect = '307',
  TooManyRequests = '429',
  Unauthorized = '401',
  UnavailableForLegalReasons = '451',
  UnprocessableEntity = '422',
  UnsupportedMediaType = '415',
  UseProxy = '305',
  MisdirectedRequest = '421',
}

export const StatusCodeDescriptions: Record<string, unknown> = {
  [StatusCodes.Accepted]: { description: 'Accepted' },
  [StatusCodes.BadGateway]: { description: 'Bad Gateway' },
  [StatusCodes.BadRequest]: { description: 'Bad Request' },
  [StatusCodes.Conflict]: { description: 'Conflict' },
  [StatusCodes.Continue]: { description: 'Continue' },
  [StatusCodes.Created]: { description: 'Created' },
  [StatusCodes.ExpectationFailed]: { description: 'Expectation Failed' },
  [StatusCodes.FailedDependency]: { description: 'Failed Dependency' },
  [StatusCodes.Forbidden]: { description: 'Forbidden' },
  [StatusCodes.GatewayTimeout]: { description: 'Gateway Timeout' },
  [StatusCodes.Gone]: { description: 'Gone' },
  [StatusCodes.HTTPVersionNotSupported]: { description: 'HTTP Version Not Supported' },
  [StatusCodes.ImATeapot]: { description: "I'm a teapot" },
  [StatusCodes.InsufficientSpaceOnResource]: { description: 'Insufficient Space on Resource' },
  [StatusCodes.InsufficientStorage]: { description: 'Insufficient Storage' },
  [StatusCodes.InternalServerError]: { description: 'Internal Server Error' },
  [StatusCodes.LengthRequired]: { description: 'Length Required' },
  [StatusCodes.Locked]: { description: 'Locked' },
  [StatusCodes.MethodFailure]: { description: 'Method Failure' },
  [StatusCodes.MethodNotAllowed]: { description: 'Method Not Allowed' },
  [StatusCodes.MovedPermanently]: { description: 'Moved Permanently' },
  [StatusCodes.MovedTemporarily]: { description: 'Moved Temporarily' },
  [StatusCodes.MultiStatus]: { description: 'Multi - Status' },
  [StatusCodes.MultipleChoices]: { description: 'Multiple Choices' },
  [StatusCodes.NetworkAuthenticationRequired]: { description: 'Network Authentication Required' },
  [StatusCodes.NoContent]: { description: 'No Content' },
  [StatusCodes.NonAuthoritativeInformation]: { description: 'Non Authoritative Information' },
  [StatusCodes.NotAcceptable]: { description: 'Not Acceptable' },
  [StatusCodes.NotFound]: { description: 'Not Found' },
  [StatusCodes.NotImplemented]: { description: 'Not Implemented' },
  [StatusCodes.NotModified]: { description: 'Not Modified' },
  [StatusCodes.PartialContent]: { description: 'Partial Content' },
  [StatusCodes.PaymentRequired]: { description: 'Payment Required' },
  [StatusCodes.PermanentRedirect]: { description: 'Permanent Redirect' },
  [StatusCodes.PreconditionFailed]: { description: 'Precondition Failed' },
  [StatusCodes.PreconditionRequired]: { description: 'Precondition Required' },
  [StatusCodes.Processing]: { description: 'Processing' },
  [StatusCodes.ProxyAuthenticationRequired]: { description: 'Proxy Authentication Required' },
  [StatusCodes.RequestHeaderFieldsTooLarge]: { description: 'Request Header Fields Too Large' },
  [StatusCodes.RequestTimeout]: { description: 'Request Timeout' },
  [StatusCodes.RequestEntityTooLarge]: { description: 'Request Entity Too Large' },
  [StatusCodes.RequestURITooLong]: { description: 'Request - URI Too Long' },
  [StatusCodes.RequestedRangeNotSatisfiable]: { description: 'Requested Range Not Satisfiable' },
  [StatusCodes.ResetContent]: { description: 'Reset Content' },
  [StatusCodes.SeeOther]: { description: 'See Other' },
  [StatusCodes.ServiceUnavailable]: { description: 'Service Unavailable' },
  [StatusCodes.SwitchingProtocols]: { description: 'Switching Protocols' },
  [StatusCodes.TemporaryRedirect]: { description: 'Temporary Redirect' },
  [StatusCodes.TooManyRequests]: { description: 'Too Many Requests' },
  [StatusCodes.Unauthorized]: { description: 'Unauthorized' },
  [StatusCodes.UnavailableForLegalReasons]: { description: 'Unavailable For Legal Reasons' },
  [StatusCodes.UnprocessableEntity]: { description: 'Unprocessable Entity' },
  [StatusCodes.UnsupportedMediaType]: { description: 'Unsupported Media Type' },
  [StatusCodes.UseProxy]: { description: 'Use Proxy' },
  [StatusCodes.MisdirectedRequest]: { description: 'Misdirected Request' },
};
