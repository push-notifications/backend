export function usernameFromEmailIfValid(userIdentifier: string): string {
  if (!userIdentifier) return null;
  const posAt = userIdentifier.indexOf('@cern.ch');
  if (posAt > 0) {
    const tmpLogin = userIdentifier.substring(0, posAt);
    // Verify it's not a full mail with . or an egroup with -
    if (tmpLogin.indexOf('.') < 0 && tmpLogin.indexOf('-') < 0) return tmpLogin;
  }
  return null;
}
