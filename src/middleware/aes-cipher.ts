import * as crypto from 'crypto';

const IV_LENGTH = 16; // For AES, this is always 16

export class AESCypher {

    static encrypt(text) {
        let iv = crypto.randomBytes(IV_LENGTH);
        let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(process.env.EMAIL_AES_SECRET_KEY), iv);
        let encrypted = cipher.update(text);
        encrypted = Buffer.concat([encrypted, cipher.final()]);
        return Buffer.concat([iv, encrypted]).toString('base64');
    }

    static decrypt(text) {
        const encrypted_bytes = Buffer.from(text, 'base64');
        const iv = encrypted_bytes.slice(0, IV_LENGTH);
        const encryptedText = encrypted_bytes.slice(IV_LENGTH);
        let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(process.env.EMAIL_AES_SECRET_KEY), iv);
        decipher.setAutoPadding(false);
        let decrypted = decipher.update(encryptedText);
        decrypted = Buffer.concat([decrypted, decipher.final()]);

        // Trim trailing 0 from python
        return decrypted.toString().replace(/\0/g, '');
    }

}
