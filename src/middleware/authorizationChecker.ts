import { Action, UnauthorizedError } from 'routing-controllers';
import * as jwt from 'jsonwebtoken';
import { CernOauth } from '../models/cern-oauth';
import { ServiceFactory } from '../services/services-factory';
import { AuthService } from '../services/auth-service';
import { AuthorizationBag } from '../models/authorization-bag';
import { APIKey } from './api-key';
import { JwtPayload } from 'jsonwebtoken';

export const API_KEY_ACCESS_ROLE = 'API_KEY_ACCESS';
export const RECOMMENDER_SYSTEM_ROLE = 'RECOMMENDER_SYSTEM_ACCESS';
export const PUBLIC_ACCESS = 'PUBLIC_ACCESS';

const UNAUTHORIZED = new UnauthorizedError("You're not authenticated");

export class AuthorizedRequest extends Request {
  authorizationBag: AuthorizationBag;
}

export class AuthorizationChecker {
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
  static async check(action: Action, roles: string[]) {
    console.info('AuthorizationChecker: Start');

    // Create empty bag, equivalent to anonymous
    const ip = action.request.header('x-forwarded-for') || action.request.socket.remoteAddress;
    action.request.authorizationBag = new AuthorizationBag(ip, null, null, null);

    if (roles.includes(PUBLIC_ACCESS) && !AuthorizationChecker.hasAccessToken(action.request)) {
      return true;
    }

    const token = AuthorizationChecker.getAccessToken(action.request);
    if (!token) {
      throw UNAUTHORIZED;
    }

    if (roles.includes(RECOMMENDER_SYSTEM_ROLE) && AuthorizationChecker.checkRecommenderSystemToken(action, token)) {
      return true;
    }

    const verified = await CernOauth.verifyToken(token);
    if (!verified) {
      console.debug('AuthorizationChecker: Unable to verify and decode token, invalid.');

      // Must be the last check after token verification because it reuses the authorization header
      if (roles.includes(API_KEY_ACCESS_ROLE)) {
        await AuthorizationChecker.checkAPIToken(action, token);
        return true;
      }

      console.debug('AuthorizationChecker: denied');
      throw UNAUTHORIZED;
    }

    const decodedToken = jwt.decode(token) as JwtPayload;
    if (!AuthorizationChecker.decodedTokenHasAccess(decodedToken, roles)) {
      throw UNAUTHORIZED;
    }
    const authService: AuthService = ServiceFactory.getAuthenticationService();
    const user = await authService.getAuthenticatedUser(decodedToken.cern_upn);
    if (!user) {
      console.debug('AuthorizationChecker: denied, user not found in DB: %s', decodedToken.cern_upn);
      throw UNAUTHORIZED;
    }

    action.request.authorizationBag = new AuthorizationBag(
      ip,
      user.id,
      decodedToken.cern_upn,
      decodedToken.resource_access[process.env.OAUTH_CLIENT_ID].roles,
    );
    // Store email for use in actions like Send
    action.request.authorizationBag.email = user.email;

    console.debug(
      'AuthorizationChecker: accepted for role(s) %s matching incoming %s',
      roles,
      decodedToken.resource_access[process.env.OAUTH_CLIENT_ID].roles,
    );
    return true;
  }

  static async checkAPIToken(action: Action, token: string): Promise<void> {
    try {
      action.request.authorizationBag.APIKey = await APIKey.load(token);
    } catch (error) {
      console.debug('AuthorizationChecker: Denied API_KEY_ACCESS', error);
      throw UNAUTHORIZED;
    }

    console.debug('AuthorizationChecker: Accepted for role API_KEY_ACCESS');
  }

  static decodedTokenHasAccess(decodedToken: JwtPayload, roles: string[]): boolean {
    const tokenRoles = decodedToken?.resource_access?.[process.env.OAUTH_CLIENT_ID]?.roles;
    return !!tokenRoles && !!roles.find(role => tokenRoles.includes(role));
  }

  static hasAccessToken(request: AuthorizedRequest): boolean {
    return !!request.headers['authorization'];
  }

  static getAccessToken(request: AuthorizedRequest): string {
    if (!request.headers['authorization']) return undefined;

    const token = request.headers['authorization'].replace('Bearer ', '');
    if (token === null) {
      console.debug('AuthorizationChecker: denied, token is null');
      throw new UnauthorizedError("You're not authenticated");
    }

    return token;
  }

  static checkRecommenderSystemToken(action: Action, token: string): boolean {
    console.debug('checking recommender system role');
    if (process.env.RECOMMENDER_SYSTEM_SECRET !== token) {
      return false;
    }

    action.request.authorizationBag.roles = [RECOMMENDER_SYSTEM_ROLE];
    console.debug('AuthorizationChecker: Accepted for role RECOMMENDER_SYSTEM_ROLE');
    return true;
  }
}
