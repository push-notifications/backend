import { ServiceFactory } from '../services/services-factory';
import { ApiKeyService } from '../services/api-key-service';

export enum APIKeyTypeEnum {
  Channel = 'ck',
}

const InvalidKeyError = 'Invalid';

export class APIKey {
  private readonly services: Record<APIKeyTypeEnum, ApiKeyService> = {
    [APIKeyTypeEnum.Channel]: ServiceFactory.getChannelsService(),
  };

  private constructor(public readonly type: string, public readonly id: string, public readonly value: string) {}

  static async load(key: string): Promise<APIKey> {
    const parts = key.split('_');
    if (parts.length != 3 || !APIKey.isValidType(parts[0])) {
      return Promise.reject(new Error(InvalidKeyError));
    }

    const api = new APIKey(parts[0], parts[1], parts[2]);

    if (!(await api._isValid)) {
      return Promise.reject(new Error(InvalidKeyError));
    }

    return api;
  }

  private get service(): ApiKeyService {
    return this.services[this.type];
  }

  private get _isValid(): Promise<boolean> {
    if (!this.type || !this.id || !this.service || !this.value) {
      return Promise.resolve(false);
    }

    return this.service.verifyAPIKey(this.id, this.value);
  }

  // check api key has rights over entity:
  //
  // validates entity type and id match
  // eg check current key has permissions over channel with id x
  // usage: authorizationBag.APIKey.hasRights(APIKeyTypeEnum.Channel, this.id)
  public hasRights(type: string, id: string): boolean {
    return this.id == id && this.type == type;
  }

  public static isValidType(type: string): boolean {
    return (<any>Object).values(APIKeyTypeEnum).includes(type);
  }
}
