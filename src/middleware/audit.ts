/* eslint-disable */
// lint disable because typescript does not recognize Response and Request properties

import { AuditIPAddress } from '../log/auditing';
import { Response, Request, NextFunction } from 'express';

export async function audit(req: Request, _res: Response, next: NextFunction): Promise<void> {
  // @ts-ignore
  if (req.method === 'OPTIONS' || req.originalUrl === '/ping') {
    next();
    return;
  }

  // @ts-ignore
  const ip = req.header('x-forwarded-for') || req.socket.remoteAddress;

  const auditData = {
    // @ts-ignore
    originalUrl: req.originalUrl, // @ts-ignore
    protocol: req.protocol, // @ts-ignore
    body: req.body, // @ts-ignore
    method: req.method,
  };
  await AuditIPAddress.setValue(ip, auditData);

  //console.debug('security audit for ip: ', ip, auditData);

  next();
}
