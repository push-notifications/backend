export interface PingService {
  ping(): Promise<string>;
}
