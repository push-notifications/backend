import { AuthorizationBag } from "../models/authorization-bag";
import { Tag } from "../models/tag";

export interface TagsServiceInterface {
  createTag(tag: Tag, authorizationBag: AuthorizationBag): Promise<Tag>;

  getTags(authorizationBag: AuthorizationBag): Promise<Tag[]>;

  getTagsByPrefix(prefix: string, authorizationBag: AuthorizationBag): Promise<Tag[]>;
}
