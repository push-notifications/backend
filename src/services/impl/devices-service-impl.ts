import { AbstractService } from './abstract-service';
import { DevicesServiceInterface } from '../devices-service';
import { CreateUserDevice } from './devices/create-user-device';
import { GetUserDevices } from './devices/get-user-devices';
import { DeleteUserDevice } from './devices/delete-user-device';
import { TestUserDevice } from './devices/test-user-device';
import { UpdateUserDevice } from './devices/update-user-device';
import { AuthorizationBag } from '../../models/authorization-bag';
import { DeviceRequest, DeviceResponse, DeviceValuesRequest, GetDevicesResponse } from '../../controllers/devices/dto';
import { UpdateUserDeviceStatus } from './devices/update-user-device-status';
import { DeviceStatus } from '../../models/device';

export class DevicesService extends AbstractService implements DevicesServiceInterface {
  createUserDevice(device: DeviceRequest, authorizationBag: AuthorizationBag): Promise<DeviceResponse> {
    return this.commandExecutor.execute(new CreateUserDevice(device, authorizationBag));
  }

  getUserDevices(authorizationBag: AuthorizationBag): Promise<GetDevicesResponse> {
    return this.commandExecutor.execute(new GetUserDevices(authorizationBag));
  }

  deleteUserDeviceById(deviceId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new DeleteUserDevice(deviceId, authorizationBag));
  }

  tryBrowserPushNotification(deviceId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new TestUserDevice(deviceId, authorizationBag));
  }

  updateUserDeviceById(
    deviceId: string,
    authorizationBag: AuthorizationBag,
    newDeviceValues: DeviceValuesRequest,
  ): Promise<DeviceResponse> {
    return this.commandExecutor.execute(new UpdateUserDevice(deviceId, authorizationBag, newDeviceValues));
  }

  updateUserDeviceStatusById(
    deviceId: string,
    authorizationBag: AuthorizationBag,
    status: DeviceStatus,
  ): Promise<DeviceResponse> {
    return this.commandExecutor.execute(new UpdateUserDeviceStatus(deviceId, authorizationBag, status));
  }
}
