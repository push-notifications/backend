import { AbstractService } from './abstract-service';
import { Group } from '../../models/group';
import { GroupsServiceInterface } from '../groups-service';
import { GetOrCreateGroup } from './groups/get-or-create-group';

export class GroupsServiceImpl
  extends AbstractService
  implements GroupsServiceInterface
{
  GetOrCreateGroup(user: string): Promise<Group> {
    return this.commandExecutor.execute(new GetOrCreateGroup(user));
  }
}
