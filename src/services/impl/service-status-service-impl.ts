import { AbstractService } from './abstract-service';
import { ServiceStatusServiceInterface } from '../service-status-service';
import {
  ServiceStatusResponse,
  ServiceStatusMessage,
  ServiceStatusListResponse,
  Query,
} from '../../controllers/service-status/dto';
import { GetServiceStatus } from './service-status/get-service-status';
import { GetAllServiceStatus } from './service-status/get-all-service-status';
import { SetServiceStatus } from './service-status/set-service-status';
import { DeleteServiceStatus } from './service-status/delete-service-status';
import { UpdateServiceStatus } from './service-status/update-service-status';

export class ServiceStatusService extends AbstractService implements ServiceStatusServiceInterface {
  getServiceStatus(): Promise<ServiceStatusResponse> {
    return this.commandExecutor.execute(new GetServiceStatus());
  }

  getAllServiceStatus(query: Query): Promise<ServiceStatusListResponse> {
    return this.commandExecutor.execute(new GetAllServiceStatus(query));
  }

  setServiceStatus(message: string, validity?: number): Promise<void> {
    return this.commandExecutor.execute(new SetServiceStatus(message, validity));
  }

  updateServiceStatus(serviceStatusMessage: ServiceStatusMessage): Promise<void> {
    return this.commandExecutor.execute(new UpdateServiceStatus(serviceStatusMessage));
  }

  deleteServiceStatusById(serviceStatusId: string): Promise<void> {
    return this.commandExecutor.execute(new DeleteServiceStatus(serviceStatusId));
  }
}
