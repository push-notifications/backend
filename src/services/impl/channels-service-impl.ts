import { SetChannelOwner } from './channels/set-channel-owner';
import { ApiKeyService } from '../api-key-service';
import { VerifyApiKey } from './api-key/verify-api-key';
import { GetChannelStats } from './channels/get-channel-stats';
import { ChannelsService } from '../channels-service';
import { AbstractService } from './abstract-service';
import { Channel } from '../../models/channel';
import { FindAllChannels } from './channels/find-all-channels';
import { FindChannelById } from './channels/find-channel-by-id';
import { EditChannelById } from './channels/edit-channel-by-id';
import { CreateChannel } from './channels/create-channel';
import { AddGroupToChannel } from './channels/add-group-to-channel';
import { SubscribeToChannel } from './channels/subscribe-to-channel';
import { UnsubscribeFromChannel } from './channels/unsubscribe-from-channel';
import { UpdateChannel } from './channels/update-channel';
import { GetChannelMembers } from './channels/get-channel-members';
import { GetChannelGroups } from './channels/get-channel-groups';
import { FindPublicChannels } from './channels/find-public-channels';
import { AddMemberToChannel } from './channels/add-member-to-channel';
import { RemoveGroupFromChannel } from './channels/remove-group-from-channel';
import { RemoveUserFromChannel } from './channels/remove-user-from-channel';
import { DeleteChannel } from './channels/delete-channel';
import { UpdateChannelAdminGroup } from './channels/update-channel-admin-group';
import { RemoveChannelAdminGroup } from './channels/remove-channel-admin-group';
import { GenerateApiKey } from './channels/generate-api-key';
import { AuthorizationBag } from '../../models/authorization-bag';
import { Category } from '../../models/category';
import { SetCategory } from './channels/set-category';
import { SetTags } from './channels/set-tags';
import {
  ChannelResponse,
  ChannelsListResponse,
  CreateChannelRequest,
  UpdateChannelRequest,
  MembersListResponse,
  GroupsListResponse,
  ChannelsQuery,
  GroupResponse,
  GetChannelResponse,
  EditChannelResponse,
  setTagsRequest,
  ChannelStatsResponse,
  MemberResponse,
  Query,
  PublicChannelsListResponse,
  GetChannelPublicResponse,
  UpdateAdminGroupRequest,
} from '../../controllers/channels/dto';
import { GetChannelByIdPublic } from './channels/find-public-channel-by-id';

export class ChannelsServiceImpl extends AbstractService implements ChannelsService, ApiKeyService {
  getAllChannels(query: ChannelsQuery, authorizationBag: AuthorizationBag): Promise<ChannelsListResponse> {
    return this.commandExecutor.execute(new FindAllChannels(query, authorizationBag));
  }

  getAllPublicChannels(query: Query): Promise<PublicChannelsListResponse> {
    return this.commandExecutor.execute(new FindPublicChannels(query));
  }

  getChannelByIdPublic(channelId: string, authorizationBag: AuthorizationBag): Promise<GetChannelPublicResponse> {
    return this.commandExecutor.execute(new GetChannelByIdPublic(channelId, authorizationBag));
  }

  getChannelById(
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<GetChannelResponse | GetChannelPublicResponse> {
    return this.commandExecutor.execute(new FindChannelById(channelId, authorizationBag));
  }
  editChannelById(channelId: string, authorizationBag: AuthorizationBag): Promise<EditChannelResponse> {
    return this.commandExecutor.execute(new EditChannelById(channelId, authorizationBag));
  }

  createChannel(channel: CreateChannelRequest, authorizationBag: AuthorizationBag): Promise<ChannelResponse> {
    return this.commandExecutor.execute(new CreateChannel(channel, authorizationBag));
  }

  deleteChannel(channelId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new DeleteChannel(channelId, authorizationBag));
  }

  updateChannel(channel: UpdateChannelRequest, authorizationBag: AuthorizationBag): Promise<ChannelResponse> {
    return this.commandExecutor.execute(new UpdateChannel(channel, authorizationBag));
  }

  getChannelMembers(channelId: string, query: Query, authorizationBag: AuthorizationBag): Promise<MembersListResponse> {
    return this.commandExecutor.execute(new GetChannelMembers(channelId, query, authorizationBag));
  }

  getChannelGroups(channelId: string, query: Query, authorizationBag: AuthorizationBag): Promise<GroupsListResponse> {
    return this.commandExecutor.execute(new GetChannelGroups(channelId, query, authorizationBag));
  }

  addGroupToChannel(groupName: string, channelId: string, authorizationBag: AuthorizationBag): Promise<GroupResponse> {
    return this.commandExecutor.execute(new AddGroupToChannel(groupName, channelId, authorizationBag));
  }

  removeGroupFromChannel(groupId: string, channelId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new RemoveGroupFromChannel(groupId, channelId, authorizationBag));
  }

  updateChannelAdminGroup(
    newAdminGroup: UpdateAdminGroupRequest,
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<ChannelResponse> {
    return this.commandExecutor.execute(new UpdateChannelAdminGroup(newAdminGroup, channelId, authorizationBag));
  }

  removeChannelAdminGroup(channelId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new RemoveChannelAdminGroup(channelId, authorizationBag));
  }

  addMemberToChannel(
    membername: string,
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<MemberResponse> {
    return this.commandExecutor.execute(new AddMemberToChannel(membername, channelId, authorizationBag));
  }

  removeMemberFromChannel(memberId: string, channelId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(
      // missing username to check permission
      new RemoveUserFromChannel(memberId, channelId, authorizationBag),
    );
  }

  subscribeToChannel(channelId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new SubscribeToChannel(authorizationBag.userId, channelId, authorizationBag));
  }

  unsubscribeFromChannel(channelId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(
      new UnsubscribeFromChannel(authorizationBag.userId, channelId, authorizationBag),
    );
  }

  setCategory(channelId: string, category: Category, authorizationBag: AuthorizationBag): Promise<ChannelResponse> {
    return this.commandExecutor.execute(new SetCategory(channelId, category, authorizationBag));
  }

  setTags(channelId: string, tags: setTagsRequest, authorizationBag: AuthorizationBag): Promise<ChannelResponse> {
    return this.commandExecutor.execute(new SetTags(channelId, tags, authorizationBag));
  }

  setChannelOwner(username: string, channelId: string, authorizationBag: AuthorizationBag): Promise<ChannelResponse> {
    return this.commandExecutor.execute(new SetChannelOwner(username, channelId, authorizationBag));
  }

  generateApiKey(channelId: string, authorizationBag: AuthorizationBag): Promise<string> {
    return this.commandExecutor.execute(new GenerateApiKey(channelId, authorizationBag));
  }

  verifyAPIKey(id: string, key: string): Promise<boolean> {
    return this.commandExecutor.execute(new VerifyApiKey(id, key, Channel));
  }

  getChannelStats(channelId: string, authorizationBag: AuthorizationBag): Promise<ChannelStatsResponse> {
    return this.commandExecutor.execute(new GetChannelStats(channelId, authorizationBag));
  }
}
