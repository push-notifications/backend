import { Command } from './command';
import { EntityManager } from 'typeorm';
import { AppDataSource } from '../../app-data-source';

export class CommandExecutor {
  execute(command: Command): Promise<any> {
    return AppDataSource.manager.transaction(async (manager: EntityManager) => {
      return await command.execute(manager);
    });
  }
}
