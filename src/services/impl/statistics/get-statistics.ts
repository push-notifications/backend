import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Statistics } from '../../../models/statistics';
import { Channel } from '../../../models/channel';
import { Notification } from '../../../models/notification';
import { User } from '../../../models/user';
import { Device } from '../../../models/device';
import { Preference } from '../../../models/preference';
import { Mute } from '../../../models/mute';
import { Category } from '../../../models/category';
import { Tag } from '../../../models/tag';
import { CERNActiveDirectory } from '../../../models/cern-activedirectory';
import { AuditNotifications } from '../../../log/auditing';

export class GetStatistics implements Command {
  s = new Statistics();

  async execute(transactionManager: EntityManager): Promise<Statistics> {
    try {
      await this.buildStatistics(transactionManager);
      return this.s;
    } catch (ex) {
      console.error('Error GetStatistics', ex.message);
      return null as Statistics;
    }
  }

  async buildStatistics(transactionManager: EntityManager) {
    // Global numbers (total)
    //this.channels = await transactionManager.getRepository(Channel).count();
    this.s.channels = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .where("visibility != 'PRIVATE'")
      .getCount();
    this.s.channels_public = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .where("visibility = 'PUBLIC'")
      .getCount();
    this.s.channels_restricted = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .where("visibility = 'RESTRICTED'")
      .getCount();
    this.s.channels_internal = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .where("visibility = 'INTERNAL'")
      .getCount();
    this.s.notifications = await transactionManager.getRepository(Notification).count();
    this.s.users = await transactionManager.getRepository(User).count();
    this.s.devices = await transactionManager
      .getRepository(Device)
      .createQueryBuilder('device')
      .where("type != 'MAIL'")
      .getCount();
    this.s.preferences = await transactionManager
      .getRepository(Preference)
      .createQueryBuilder('preference')
      .where("name != 'Default Live' and name != 'Default Daily'")
      .getCount();
    this.s.mutes = await transactionManager.getRepository(Mute).count();
    this.s.categories = await transactionManager.getRepository(Category).count();
    this.s.tags = await transactionManager.getRepository(Tag).count();

    // Daily numbers (last 24h)
    this.s.last_day.channels_created = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .where('"creationDate" > NOW() - INTERVAL \'1 DAY\'')
      .getCount();
    this.s.last_day.notifications_sent = await transactionManager
      .getRepository(Notification)
      .createQueryBuilder('notification')
      .where('"sentAt" > NOW() - INTERVAL \'1 DAY\'')
      .getCount();
    this.s.last_day.users_active = await transactionManager
      .getRepository(User)
      .createQueryBuilder('user')
      .where('"lastLogin" > NOW() - INTERVAL \'1 DAY\'')
      .getCount();

    const usersWeekly = await transactionManager
      .getRepository(User)
      .createQueryBuilder('user')
      .where('"lastLogin" > NOW() - INTERVAL \'1 DAY\'')
      .select('user.email')
      .getRawMany();

    // channels per department
    this.s.last_day.users_active_per_department = {};
    for (const user of usersWeekly) {
      let dept = await CERNActiveDirectory.memoizedGetUserDepartment(user.user_email);
      if (!dept) {
        dept = '-';
      }
      if (dept in this.s.last_day.users_active_per_department)
        this.s.last_day.users_active_per_department[dept] = this.s.last_day.users_active_per_department[dept] + 1;
      else this.s.last_day.users_active_per_department[dept] = 1;
    }

    // Monthly numbers
    this.s.last_month.channels_created = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .where('"creationDate" > NOW() - INTERVAL \'1 month\'')
      .getCount();
    this.s.last_month.notifications_sent = await transactionManager
      .getRepository(Notification)
      .createQueryBuilder('notification')
      .where('"sentAt" > NOW() - INTERVAL \'1 month\'')
      .getCount();
    this.s.last_month.users_active = await transactionManager
      .getRepository(User)
      .createQueryBuilder('user')
      .where('"lastLogin" > NOW() - INTERVAL \'1 month\'')
      .getCount();

    const usersMonthly = await transactionManager
      .getRepository(User)
      .createQueryBuilder('user')
      .where('"lastLogin" > NOW() - INTERVAL \'1 month\'')
      .select('user.email')
      .getRawMany();

    // channels per department
    this.s.last_month.users_active_per_department = {};
    for (const user of usersMonthly) {
      let dept = await CERNActiveDirectory.memoizedGetUserDepartment(user.user_email);
      if (!dept) {
        dept = '-';
      }
      if (dept in this.s.last_month.users_active_per_department)
        this.s.last_month.users_active_per_department[dept] = this.s.last_month.users_active_per_department[dept] + 1;
      else this.s.last_month.users_active_per_department[dept] = 1;
    }

    await this.calculateDeviceStats(transactionManager);
    await this.calculateMuteStats(transactionManager);
    await this.calculatePreferenceStats(transactionManager);
    await this.calculateUserStats(transactionManager);
    await this.calculateNotificationStats(transactionManager);
    await this.calculateChannelStats(transactionManager);
  }

  async calculateChannelStats(transactionManager: EntityManager) {
    const owners = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .leftJoinAndSelect('channel.owner', 'owner')
      .distinctOn(['owner.email'])
      .select('owner.email')
      .getRawMany();

    // channels per department
    this.s.channels_per_department = {};
    for (const owner of owners) {
      let ownerDepartment = await CERNActiveDirectory.memoizedGetUserDepartment(owner.owner_email);
      if (!ownerDepartment) {
        ownerDepartment = '-';
        console.log(owner, ownerDepartment);
      }
      if (ownerDepartment in this.s.channels_per_department)
        this.s.channels_per_department[ownerDepartment] = this.s.channels_per_department[ownerDepartment] + 1;
      else this.s.channels_per_department[ownerDepartment] = 1;
    }

    const channels = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .select(['channel.name', 'channel.creationDate'])
      .orderBy('channel.creationDate', 'DESC')
      .limit(5)
      .getRawMany();

    this.s.top_latest_created_channels = {};
    channels.map(item => {
      this.s.top_latest_created_channels[item.channel_name] = item.channel_creationDate;
    });
  }

  async calculateDeviceStats(transactionManager: EntityManager) {
    const maxDevices = await transactionManager
      .getRepository(Device)
      .createQueryBuilder('device')
      .select('COUNT(device.id) AS device_count')
      .groupBy('device.userId')
      .orderBy('device_count', 'DESC')
      .limit(5)
      .getRawMany();

    this.s.top_max_devices = {};
    maxDevices.map((item, index) => {
      this.s.top_max_devices['***' + index] = item.device_count;
    });

    const device_avg = await transactionManager
      .createQueryBuilder()
      .select('AVG(device_count) AS average')
      .from(
        qb =>
          qb
            .select('COUNT(device.id) AS device_count')
            .from(Device, 'device')
            .groupBy('device.userId')
            .orderBy('device_count', 'DESC')
            .select('COUNT(device.id) AS device_count'),

        'device_counts',
      )
      .getRawOne();

    this.s.avg_devices_per_user = device_avg.average;
  }

  async calculatePreferenceStats(transactionManager: EntityManager) {
    const maxPreferences = await transactionManager
      .getRepository(Preference)
      .createQueryBuilder('preference')
      .select('COUNT(preference.id) AS preference_count')
      .groupBy('preference.userId')
      .orderBy('preference_count', 'DESC')
      .limit(5)
      .getRawMany();

    this.s.top_max_preferences = {};
    maxPreferences.map((item, index) => {
      this.s.top_max_preferences['***' + index] = item.preference_count;
    });

    const preference_avg = await transactionManager
      .createQueryBuilder()
      .select('AVG(preference_count) AS average')
      .from(
        qb =>
          qb
            .select('COUNT(preference.id) AS preference_count')
            .from(Device, 'preference')
            .groupBy('preference.userId')
            .orderBy('preference_count', 'DESC')
            .select('COUNT(preference.id) AS preference_count'),

        'preference_counts',
      )
      .getRawOne();

    this.s.avg_preferences_per_user = preference_avg.average;
  }

  async calculateMuteStats(transactionManager: EntityManager) {
    const max = await transactionManager
      .getRepository(Mute)
      .createQueryBuilder('mute')
      .select('COUNT(mute.id) AS mute_count')
      .groupBy('mute.userId')
      .orderBy('mute_count', 'DESC')
      .limit(5)
      .getRawMany();

    this.s.top_max_mutes = {};
    max.map((item, index) => {
      this.s.top_max_mutes['***' + index] = item.mute_count;
    });

    const avgs = await transactionManager
      .createQueryBuilder()
      .select('AVG(mute_count) AS average')
      .from(
        qb =>
          qb
            .select('COUNT(mute.id) AS mute_count')
            .from(Device, 'mute')
            .groupBy('mute.userId')
            .orderBy('mute_count', 'DESC')
            .select('COUNT(mute.id) AS mute_count'),

        'mute_counts',
      )
      .getRawOne();

    this.s.avg_mutes_per_user = avgs.average;
  }

  async calculateUserStats(transactionManager: EntityManager) {
    const maxMembers = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .leftJoinAndSelect('channel.members', 'member')
      .select(['COUNT(channel.id) AS users_count', 'channel.id', 'channel.name'])
      .groupBy('channel.id')
      .addGroupBy('channel.name')
      .orderBy('users_count', 'DESC')
      .limit(5)
      .getRawMany();

    this.s.top_channels_with_self_subscription = {};
    maxMembers.map((item, index) => {
      this.s.top_channels_with_self_subscription[item.channel_name] = item.users_count;
    });

    const arrayActiveUser = await transactionManager
      .getRepository(User)
      .createQueryBuilder('user')
      .select(['user.email', 'user.lastLogin'])
      .where('user.lastLogin IS NOT NULL')
      .getRawMany();

    this.s.users_active = arrayActiveUser.length;
    this.s.active_users_per_department = {};
    for (const user of arrayActiveUser) {
      let userDepartment = await CERNActiveDirectory.memoizedGetUserDepartment(user.user_email);
      if (!userDepartment) userDepartment = '-';
      if (userDepartment in this.s.active_users_per_department)
        this.s.active_users_per_department[userDepartment] = this.s.active_users_per_department[userDepartment] + 1;
      else this.s.active_users_per_department[userDepartment] = 1;
    }
  }

  async calculateNotificationStats(transactionManager: EntityManager) {
    const maxNotifications = await transactionManager
      .getRepository(Notification)
      .createQueryBuilder('notification')
      .select(['COUNT(notification.id) AS notification_count', 'channel.name'])
      .innerJoinAndSelect('notification.target', 'channel')
      .where('channel.deleteDate is NULL')
      .groupBy('notification.targetId')
      .addGroupBy('channel.name')
      .addGroupBy('channel.id')
      .orderBy('notification_count', 'DESC')
      .limit(5)
      .getRawMany();

    this.s.top_channels_with_notifications = {};
    maxNotifications.map((item, index) => {
      this.s.top_channels_with_notifications[item.channel_name] = item.notification_count;
    });

    const notification_avg = await transactionManager
      .createQueryBuilder()
      .select('AVG(notification_count) AS average')
      .from(
        qb =>
          qb
            .select('COUNT(notification.id) AS notification_count')
            .from(Notification, 'notification')
            .groupBy('notification.targetId')
            .orderBy('notification_count', 'DESC'),
        'notification_counts',
      )
      .getRawOne();

    this.s.avg_notifications_per_channel = notification_avg.average;

    const arrayNotification = await transactionManager
      .getRepository(Notification)
      .createQueryBuilder('notification')
      .where("notification.sentAt >= now() - interval '1 week'")
      .select(['notification.id'])
      .orderBy('notification.sentAt', 'DESC')
      .getRawMany();

    this.s.last_10_notifications_reach = {};
    let count = 0;
    let user_count = 0;
    this.s.avg_users_reached_per_notification_weekly = 0;
    this.s.avg_devices_reached_per_notification_weekly = 0;
    this.s.total_end_notifications_weekly = 0;
    this.s.total_end_notifications_weekly_across_devices = 0;
    this.s.last_100_user_reach_per_department = {};

    for (const notification of arrayNotification) {
      try {
        const oneAudit = (await AuditNotifications.getValuesAsJson(notification.notification_id)) as any;
        if (
          oneAudit &&
          notification.notification_id in oneAudit &&
          oneAudit[notification.notification_id].router &&
          oneAudit[notification.notification_id].router.target_users
        ) {
          const target_users = oneAudit[notification.notification_id].router.target_users;
          const total_users_reach = Object.keys(target_users).length;
          this.s.avg_users_reached_per_notification_weekly += total_users_reach;
          this.s.total_end_notifications_weekly += total_users_reach;

          if (count < 10) {
            const anon_id = '***-' + count; // anonymize notification id
            this.s.last_10_notifications_reach[anon_id] = {
              total_users_reach: total_users_reach,
              total_device_reach: 0,
            };
          }

          for (const user of Object.keys(target_users)) {
            user_count += 1;
            const total_device_reach = Object.keys(target_users[user]).length;
            this.s.avg_devices_reached_per_notification_weekly += total_device_reach;
            this.s.total_end_notifications_weekly_across_devices += total_device_reach;

            // reach per department
            if (user_count < 100) {
              let dept = await CERNActiveDirectory.memoizedGetUserDepartment(user);
              if (!dept) {
                dept = '-';
              }
              if (dept in this.s.last_100_user_reach_per_department)
                this.s.last_100_user_reach_per_department[dept] = this.s.last_100_user_reach_per_department[dept] + 1;
              else this.s.last_100_user_reach_per_department[dept] = 1;
            }

            if (count < 10) {
              const anon_id = '***-' + count; // anonymize notification id
              this.s.last_10_notifications_reach[anon_id]['total_device_reach'] += total_device_reach;
            }
          }
          count++;
        }
        // eslint-disable-next-line no-empty
      } catch (NotFoundError) {}
    }
    if (arrayNotification.length > 0) {
      this.s.avg_users_reached_per_notification_weekly =
        this.s.avg_users_reached_per_notification_weekly / arrayNotification.length;
      this.s.avg_devices_reached_per_notification_weekly =
        this.s.avg_devices_reached_per_notification_weekly / arrayNotification.length;
    }
  }
}
