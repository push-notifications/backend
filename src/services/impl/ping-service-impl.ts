import { AbstractService } from "./abstract-service";
import { PingService } from "../ping-service";
import { Ping } from "./ping/Ping";

export class PingServiceImpl extends AbstractService implements PingService {

  ping(): Promise<string> {
    return this.commandExecutor.execute(new Ping());
  }
}
