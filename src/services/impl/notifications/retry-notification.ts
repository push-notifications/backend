import * as Amqp from 'amqp-ts';
import * as stompit from 'stompit';
import { EntityManager } from 'typeorm';
import { NotFoundError } from 'routing-controllers';
import { Command } from '../command';
import { Notification } from '../../../models/notification';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { Configuration } from '../../../config/configuration';

export class RetryNotification implements Command {
  private connection: Amqp.Connection;
  private queue: Amqp.Queue;
  constructor(private notificationId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    const notification = await transactionManager.findOne(Notification, {
      relations: ['target', 'target.owner', 'target.adminGroup'],
      where: {
        id: this.notificationId,
      },
    });

    if (!notification) throw new NotFoundError('Notification does not exist');

    // Restricted to unauthenticated routes
    // if (!(await notification.target.hasAccess(this.authorizationBag)))
    //   throw new ForbiddenError("Access not authorized.");

    stompit.connect(Configuration.activeMQ, (error, client) => {
      if (error) {
        console.debug('connect error ' + error.message);
        return;
      }

      const sendHeaders = {
        destination: '/queue/np.routing',
        'content-type': 'text/plain',
        persistent: 'true',
      };

      const frame = client.send(sendHeaders);
      frame.write(JSON.stringify(notification));
      frame.end();

      client.disconnect();
    });
  }
}
