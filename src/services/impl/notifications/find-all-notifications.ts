import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Notification } from '../../../models/notification';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { NotificationsListResponse, Query } from '../../../controllers/channels/dto';

export class FindAllNotifications implements Command {
  constructor(private channelId: string, private query: Query, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<NotificationsListResponse> {
    const channel = await transactionManager.findOneBy(Channel, { id: this.channelId });
    if (!channel) {
      throw new NotFoundError('Channel does not exist');
    }

    // Get current user groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag?.userName);
    if (!(await channel.hasAccess(transactionManager, this.authorizationBag, userGroups))) {
      throw new ForbiddenError('Access to Channel not Authorized!');
    }

    const hasAdminAccess = this.authorizationBag.isAnonymous
      ? false
      : await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups);

    const qb = await transactionManager
      .getRepository(Notification)
      .createQueryBuilder('notification')
      .where('notification.target = :channelId', { channelId: this.channelId });

    if (this.query.searchText) qb.andWhere(Notification.qbSearchText(this.query.searchText));

    if (!hasAdminAccess && !this.authorizationBag.isSupporter) {
      qb.andWhere(Notification.filterNotifications(userGroups, this.authorizationBag.userId));
    }

    const count = await qb.clone().getCount();
    if (count == 0) {
      return new NotificationsListResponse();
    }

    qb.select(['notification.id', 'notification.sendAt', 'notification.sentAt'])
      .orderBy('notification.sentAt', 'DESC', 'NULLS FIRST')
      .addOrderBy('notification.sendAt', 'ASC')
      .distinct()
      .limit(this.query.take || 10)
      .offset(this.query.skip || 0);

    const notification_ids = await qb.getRawMany();

    const notifications = await transactionManager
      .getRepository(Notification)
      .createQueryBuilder('notification')
      .where('notification.id IN (:...notificationIds)', {
        notificationIds: notification_ids.map(n => n.notification_id),
      })
      .orderBy('notification.sentAt', 'DESC', 'NULLS FIRST')
      .addOrderBy('notification.sendAt', 'ASC')
      .getMany();

    return new NotificationsListResponse(notifications, count);
  }
}
