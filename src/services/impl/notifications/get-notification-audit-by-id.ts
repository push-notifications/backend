import { Command } from '../command';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditExternal } from '../../../log/auditing';
import { EntityManager } from 'typeorm';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { Notification } from '../../../models/notification';

export class GetNotificationAuditById implements Command {
  constructor(private notificationId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<JSON> {
    const notification = await transactionManager.findOne(Notification, {
      relations: ['target', 'target.owner', 'target.adminGroup'],
      where: {
        id: this.notificationId,
      },
    });

    if (!notification) throw new NotFoundError('Notification does not exist');

    if (!(await notification.target.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to edit this channel.");

    return AuditExternal.getValuesAsJson(this.notificationId);
  }
}
