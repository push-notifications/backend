import { Command } from '../command';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { EntityManager } from 'typeorm';
import { ForbiddenError, NotFoundError, InternalServerError } from 'routing-controllers';
import { Notification } from '../../../models/notification';
import { AuditNotifications } from '../../../log/auditing';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class DeleteNotificationById implements Command {
  constructor(private notificationId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    const notification = await transactionManager.findOne(Notification, {
      relations: ['target.owner', 'target.adminGroup'],
      where: {
        id: this.notificationId,
      },
    });

    if (!notification) throw new NotFoundError('Notification does not exist');

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    // user needs to be channel admin or service supporter/admin
    if (!(await notification.target.hasAdminAccess(transactionManager, this.authorizationBag, userGroups)))
      throw new ForbiddenError("You don't have the rights to manage this channel.");

    const result = await transactionManager.softDelete(Notification, notification.id);
    if (result.affected === 1) {
      await AuditNotifications.setValue(notification.id, {
        event: 'Delete',
        user: this.authorizationBag?.email,
        from: notification.source,
        ip: this.authorizationBag.ip,
      });
    } else throw new InternalServerError('An unexpected error occurred, please try again or open a ticket.');
  }
}
