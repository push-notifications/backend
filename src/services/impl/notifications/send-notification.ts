import * as stompit from 'stompit';
import { EntityManager } from 'typeorm';
import { BadRequestError, ForbiddenError, NotFoundError } from 'routing-controllers';
import { Command } from '../command';
import { Notification } from '../../../models/notification';
import { SendDateFormat, Times, Source, PriorityLevel } from '../../../models/notification-enums';
import { Channel } from '../../../models/channel';
import { User } from '../../../models/user';
import { Group } from '../../../models/group';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { Configuration } from '../../../config/configuration';
import { ServiceFactory } from '../../services-factory';

import * as moment from 'moment';
import { UsersServiceInterface } from '../../users-service';
import { GroupsServiceInterface } from '../../groups-service';
import * as memoize from 'memoizee';
import { AuditNotifications } from '../../../log/auditing';
import { GetNotificationResponse, SendNotificationRequest } from '../../../controllers/notifications/dto';
import { createShortURL } from '../channels/manage-short-url';

import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { SubmissionByForm } from '../../../models/channel-enums';

export class SendNotification implements Command {
  private usersService: UsersServiceInterface = ServiceFactory.getUserService();
  private groupsService: GroupsServiceInterface = ServiceFactory.getGroupService();

  constructor(private notification: SendNotificationRequest, private authorizationBag: AuthorizationBag) {}

  async hasAccess(transactionManager: EntityManager, channel: Channel, userGroups: any[]): Promise<boolean> {
    if (!this.authorizationBag) {
      // Call from the /unauthenticated
      // restricted by direct access in openshift
      return true;
    }

    const hasApiKeyAccess = channel.hasApiKeyAccess(this.authorizationBag);
    console.debug('hasApiKeyAccess', hasApiKeyAccess);
    if (hasApiKeyAccess) return true;

    if (channel.submissionByForm.includes(SubmissionByForm.administrators)) {
      const hasAdminAccess = await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups);
      console.debug('hasAdminAccess', hasAdminAccess);
      if (hasAdminAccess) return true;
    }

    const isTargetedNotification =
      this.notification.targetUsers?.length > 0 ||
      this.notification.targetGroups?.length > 0 ||
      this.notification.targetData?.length > 0;
    if (isTargetedNotification) {
      // Targeted notifications are not allowed for members
      return;
    }

    if (channel.submissionByForm.includes(SubmissionByForm.members)) {
      const hasMemberAccess = await channel.hasMemberAccess(
        transactionManager,
        this.authorizationBag.userId,
        userGroups,
      );
      console.debug('hasMemberAccess', hasMemberAccess);
      if (hasMemberAccess) return true;
    }

    return false;
  }

  async execute(transactionManager: EntityManager): Promise<GetNotificationResponse> {
    const targetChannel = await transactionManager.findOne(Channel, {
      relations: {
        owner: true,
        adminGroup: true,
        category: true,
      },
      where: { id: this.notification.target },
    });

    if (!targetChannel) {
      throw new NotFoundError('Channel does not exist');
    }

    // Get current user groups, to be used next in SQL to compare with channel groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag?.userName);

    if (!(await this.hasAccess(transactionManager, targetChannel, userGroups))) {
      throw new ForbiddenError('Sending to Channel not Authorized !');
    }

    if (this.notification.priority === PriorityLevel.CRITICAL) {
      if (!(await targetChannel.canSendCritical(this.authorizationBag)))
        throw new ForbiddenError('Sending Critical Notifications to Channel not Authorized !');
    }

    this.validateFields();

    // Update lastActivityDate from Channel
    await this.updateLastActivity(targetChannel.id, transactionManager);

    let targetUsers = [];
    let targetGroups = [];

    // Extract targetData values (strings comma separated)
    // and fill extracted values in targetUsers and targetGroups
    if (this.notification.targetData && this.notification.targetData.length > 0) {
      console.debug('Processing targetData', this.notification.targetData);
      if (!this.notification.targetUsers) this.notification.targetUsers = [];
      if (!this.notification.targetGroups) this.notification.targetGroups = [];

      this.notification.targetData.forEach(id => {
        if (!id) return;
        const identifier = id.toLowerCase();
        // no @ mean it's a group name (or a mistake that we'll ignore)
        if (!identifier.includes('@')) this.notification.targetGroups.push(new Group({ groupIdentifier: identifier }));
        // @domain is not @cern.ch then it's an external user
        else if (!identifier.includes('@cern.ch')) this.notification.targetUsers.push(new User({ email: identifier }));
        // email@cern.ch contains a - so it's a group
        else if (identifier.includes('-'))
          this.notification.targetGroups.push(new Group({ groupIdentifier: identifier.replace('@cern.ch', '') }));
        // And the rest is handled as user email
        else this.notification.targetUsers.push(new User({ email: identifier }));
      });
    }

    if (this.notification.targetUsers && this.notification.targetUsers.length > 0) {
      console.debug('Processing targetUsers', this.notification.targetUsers);
      this.notification.private = true;
      targetUsers = await this.getOrCreateTargetUsers(
        transactionManager,
        targetChannel,
        this.notification.intersection,
      );
    }

    if (this.notification.targetGroups && this.notification.targetGroups.length > 0) {
      console.debug('Processing targetGroups', this.notification.targetGroups);
      this.notification.private = true;
      targetGroups = await this.getOrCreateTargetGroups(
        transactionManager,
        targetChannel,
        this.notification.intersection,
        userGroups,
        targetChannel.id,
      );
    }

    const source = this.setSource(this.authorizationBag, this.notification.source);

    if (!targetChannel.shortUrl) {
      await transactionManager.update(
        Channel,
        { id: targetChannel.id },
        { shortUrl: await createShortURL(targetChannel.id) },
      );
    }

    const newNotification = await transactionManager.save(
      new Notification({
        ...this.notification,
        source,
        target: targetChannel,
        sender: this.authorizationBag?.email || this.notification.sender,
        targetUsers,
        targetGroups,
      }),
    );

    const result = await this.enrich(transactionManager, newNotification);

    if (!newNotification.sendAt) {
      await this.sendToQueue(newNotification);
    }

    // Warning!
    // Add no actions that could cause a rollback after successfully sending to queue
    try {
      await AuditNotifications.setValue(newNotification.id, {
        event: 'Sent',
        user: this.authorizationBag?.email || this.notification.sender,
        from: newNotification.source,
        ip: this.authorizationBag?.ip,
      });
      // eslint-disable-next-line no-empty
    } catch (any) {
      console.error(any);
    }

    // Get back object from DB, without full info about target Channel
    return new GetNotificationResponse(result);
  }

  setSource(authBag: AuthorizationBag, notificationSource: string): Source {
    if (notificationSource == Source.email) return Source.email;
    if (notificationSource == Source.web) return Source.web;
    if (authBag?.isApiKey) return Source.api;
    return Source.api;
  }

  async sendToQueue(notification: Notification): Promise<void> {
    await stompit.connect(Configuration.activeMQ, async (error, client) => {
      if (error) {
        console.error('connect error ' + error.message);
        throw new Error('Failed to create new notification: please open a ticket: ' + error);
      }

      const TTL = Number(process.env.TT ?? 172800);
      const sendHeaders = {
        destination: '/queue/np.routing',
        'content-type': 'text/plain',
        persistent: 'true',
        expires: Math.round(new Date().getTime()) + TTL * 1000,
      };

      const frame = client.send(sendHeaders, {
        onError: error => {
          console.error('stomp send error ' + error.message);
          throw new Error('Failed to create new notification: please open a ticket: ' + error);
        },
      });

      const message = {
        id: notification.id,
        priority: notification.priority,
        sentAt: notification.sentAt,
        body: notification.body,
        summary: notification.summary,
        link: notification.link,
        imgUrl: notification.imgUrl,
        private: notification.private,
        intersection: notification.intersection,
        target: {
          id: notification.target.id,
          name: notification.target.name,
          slug: notification.target.slug,
          category: notification.target.category,
          shortUrl: notification.target.shortUrl,
        },
      };

      frame.write(JSON.stringify(message));
      frame.end();

      client.disconnect(function (error) {
        if (error) {
          console.log('Error while disconnecting stomp: ' + error.message);
          return;
        }
      });
    });
  }

  async enrich(transactionManager: EntityManager, notification: Notification): Promise<Notification> {
    // Update link if empty to set the default click URI for push notifications
    if (!notification.link && process.env.BASE_WEBSITE_URL)
      notification.link = `${process.env.BASE_WEBSITE_URL}/channels/${encodeURIComponent(
        notification.target.id,
      )}/notifications/${encodeURIComponent(notification.id)}`;

    // Remove newlines from subject if any to prevent mail header and similar issues
    notification.summary = notification.summary.replace(/(\r\n|\n|\r)/gm, ' ');
    return await transactionManager.save(notification);
  }

  validateFields(): void {
    if (!this.notification.summary) throw new BadRequestError('Invalid Notification: field summary is required');

    if (!this.notification.body) throw new BadRequestError('Invalid Notification: field body is required');

    // Notification is scheduled
    if (this.notification.sendAt) {
      const sendMoment = moment(this.notification.sendAt, SendDateFormat, true);

      //is date well formatted
      if (!sendMoment.isValid())
        throw new BadRequestError('Invalid Scheduled Date: scheduled date is not correctly formatted');

      //scheduled to the past
      if (sendMoment.isBefore())
        throw new BadRequestError('Invalid Scheduled Date: scheduled date has to be later than the current one');

      //scheduled time not valid
      if (!(sendMoment.hour() in Times)) throw new BadRequestError('Invalid Scheduled Time: scheduled time is invalid');
    }
  }

  // Get users objects, create and subscribe if needed
  // Ignore subscribe and intersect status, it's handled later at routing time
  async getOrCreateTargetUsers(
    transactionManager: EntityManager,
    targetChannel: Channel,
    intersection: boolean,
  ): Promise<User[]> {
    if (!targetChannel.sendPrivate) throw new ForbiddenError('This Channel does not allow direct notifications');

    const usersToSubscribe = [];
    const targetUsers = [];
    await Promise.all(
      this.notification.targetUsers.map(async (_targetUser: User) => {
        const userIdentifier = _targetUser.email.toLowerCase().trim();
        let user;
        try {
          user = await this.usersService.GetOrCreateUserFromUsernameOrEmail(userIdentifier);
        } catch (ex) {
          console.warn('Direct notification to new member: ', userIdentifier, ex);
          throw new BadRequestError('Error adding target user: ' + userIdentifier);
        }

        targetUsers.push(user);

        if (intersection) return;
        if (await targetChannel.isUserSubscribed(transactionManager, user)) return;

        usersToSubscribe.push(user);
      }),
    );

    if (intersection) {
      return targetUsers;
    }

    console.debug('subscribing target users', targetUsers);
    // optimized save performance
    await transactionManager.createQueryBuilder().relation(Channel, 'members').of(targetChannel).add(usersToSubscribe);

    return targetUsers;
  }

  // Get groups objects, create and subscribe if needed
  // Ignore subscribe and intersect status, it's handled later at routing time
  async getOrCreateTargetGroups(
    transactionManager: EntityManager,
    channel: Channel,
    intersection: boolean,
    userGroups: any[],
    channel_id: string,
  ): Promise<Group[]> {
    if (!channel.sendPrivate) throw new ForbiddenError('This Channel does not allow direct notifications');

    const groupsToSubscribe = [];
    const targetGroups = [];
    await Promise.all(
      this.notification.targetGroups.map(async (_targetGroup: Group) => {
        const groupIdentifier = _targetGroup.groupIdentifier.trim();
        let group;
        try {
          group = await this.groupsService.GetOrCreateGroup(groupIdentifier);
        } catch (ex) {
          console.warn('Direct notification to new group failed: ', groupIdentifier, ex);
          throw new BadRequestError('Grappa group not found: ' + groupIdentifier);
        }

        // Throws error if there's an un-matched EGroup posting restriction
        /*
        await group.validateEgroupPostingRestrictionsOrThrowError(
          this.authorizationBag.userName,
          userGroups,
          channel_id,
        );
        */

        targetGroups.push(group);

        if (intersection) return;
        if (await channel.isGroupSubscribed(transactionManager, group)) return;

        groupsToSubscribe.push(group);
      }),
    );

    if (intersection || !groupsToSubscribe) {
      return targetGroups;
    }

    console.debug('subscribing target groups', targetGroups);

    // optimized save performance
    await transactionManager.createQueryBuilder().relation(Channel, 'groups').of(channel).add(groupsToSubscribe);

    return targetGroups;
  }

  // Update lastActivityDate from the Channel with the current Date
  static async _updateLastActivityNoCache(channel_id: string, transactionManager: EntityManager) {
    await transactionManager
      .createQueryBuilder()
      .update(Channel)
      .set({ lastActivityDate: new Date() })
      .where('id = :id', { id: channel_id })
      .execute();
  }

  // Only update once every 15 minutes
  static _memoizedUpdateLastActivity = memoize(SendNotification._updateLastActivityNoCache, {
    //promise: true,
    maxAge: 5 * 60 * 1000, // Update field once every 5 minutes only
    length: 1, // use only first parameter for cache (channel_id)
  });

  // Using id only for the cache function
  async updateLastActivity(channel_id: string, transactionManager: EntityManager) {
    return SendNotification._memoizedUpdateLastActivity(channel_id, transactionManager);
  }
}
