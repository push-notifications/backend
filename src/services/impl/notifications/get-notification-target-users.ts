import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Notification } from '../../../models/notification';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { NotificationTargetUsersResponse } from '../../../controllers/notifications/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { User } from '../../../models/user';

const ITEMS_LIMIT = 200;

export class GetNotificationTargetUsers implements Command {
  constructor(private notificationId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<NotificationTargetUsersResponse> {
    const notification = await transactionManager.findOne(Notification, {
      relations: ['target'],
      where: {
        id: this.notificationId,
      },
    });
    if (!notification) throw new NotFoundError('Notification does not exist');

    const userGroups = (await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag?.userName)) || [];

    if (
      !(await notification.target.hasAdminAccess(transactionManager, this.authorizationBag, userGroups)) &&
      !this.authorizationBag.isSupporter
    ) {
      console.debug('Unauthorized user access to notification groups targets.', notification.id, this.authorizationBag);
      throw new ForbiddenError('Access to notification target groups not authorized.');
    }

    const [users, count] = await transactionManager.findAndCount(User, {
      order: {
        email: 'ASC',
        username: 'ASC',
      },
      take: ITEMS_LIMIT,
      where: {
        notificationsTargeting: {
          id: this.notificationId,
        },
      },
    });

    return new NotificationTargetUsersResponse(count, users);
  }
}
