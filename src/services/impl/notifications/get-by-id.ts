import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Notification } from '../../../models/notification';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { GetNotificationResponse } from '../../../controllers/notifications/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class GetById implements Command {
  constructor(private notificationId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<GetNotificationResponse> {
    const notification = await transactionManager.findOne(Notification, {
      relations: ['target', 'target.owner', 'target.adminGroup', 'targetUsers', 'targetGroups'],
      where: {
        id: this.notificationId,
      },
    });

    if (!notification) throw new NotFoundError('Notification does not exist');

    const userGroups = (await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag?.userName)) || [];

    if (!(await notification.target.hasAccess(transactionManager, this.authorizationBag, userGroups))) {
      console.debug(
        'Unauthorized user access to a notification in a forbidden channel',
        notification.id,
        this.authorizationBag,
      );
      throw new ForbiddenError('Access to channel not authorized.');
    }

    // Check if targeted notification is authorized for current user
    if (!(await notification.isUserTargeted(this.authorizationBag))) {
      console.debug('Unauthorized user access to targeted notification', notification.id, this.authorizationBag);
      throw new ForbiddenError('Access to notification not authorized.');
    }

    return new GetNotificationResponse(notification);
  }
}
