import * as jwt from 'jsonwebtoken';

import { Command } from '../../command';
import { CernOauth } from '../../../../models/cern-oauth';
import { ServiceFactory } from '../../../services-factory';
import { AuthService } from '../../../auth-service';
import { JwtPayload } from 'jsonwebtoken';

export class Login implements Command {
  private cernOauth: CernOauth;

  authService: AuthService = ServiceFactory.getAuthenticationService();

  constructor(private code: string) {
    this.cernOauth = new CernOauth();
  }

  async execute(): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }> {
    try {
      const tokens = await this.cernOauth.getToken(this.code);
      const payload = jwt.decode(tokens.access_token) as JwtPayload;
      await this.authService.upsertUser(payload.cern_upn, payload.email);

      return tokens;
    } catch (e) {
      throw new Error('Error when trying to authenticate the user: ' + e);
    }
  }
}
