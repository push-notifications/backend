import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../../models/user';
import { Device } from '../../../../models/device';
import { Preference } from '../../../../models/preference';

export class CreateUserWithDefaults implements Command {
  constructor(private user: User) {}

  async execute(transactionManager: EntityManager): Promise<User> {
    try {
      // Create User
      const savedUser = await transactionManager.save(this.user);

      // Create default Device with user mail address
      const mailDevice = await transactionManager.save(
        new Device({
          name: this.user.email,
          user: savedUser,
          info: 'Default',
          type: 'MAIL',
          token: this.user.email,
        }),
      );

      // Create Default Live Preference sending to default mail device
      // Default Live: Normal and Important
      await transactionManager.save(
        new Preference({
          name: 'Default Live',
          user: savedUser,
          type: 'LIVE',
          notificationPriority: process.env.DEFAULT_LIVE_NOTIFICATION_PRIORITIES.split(','),
          devices: [mailDevice],
        }),
      );
      // Create Default Daily Preference sending to default mail device
      // Default Daily: Low
      await transactionManager.save(
        new Preference({
          name: 'Default Daily',
          user: savedUser,
          type: 'DAILY',
          notificationPriority: process.env.DEFAULT_DAILY_NOTIFICATION_PRIORITIES.split(','),
          devices: [mailDevice],
        }),
      );

      return savedUser;
    } catch (ex) {
      console.debug(ex);
      throw new Error('Failed to create new user with default preference and device.');
    }
  }
}
