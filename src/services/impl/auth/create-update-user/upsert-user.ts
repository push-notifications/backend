import { Command } from '../../command';
import { User } from '../../../../models/user';
import { EntityManager } from 'typeorm';
import { AuthService } from '../../../auth-service';
import { ServiceFactory } from '../../../services-factory';

export class UpsertUser implements Command {
  constructor(private upn: string, private email: string) {}
  authService: AuthService = ServiceFactory.getAuthenticationService();

  async execute(transactionManager: EntityManager): Promise<void> {
    // Refresh user
    const user = new User({
      username: this.upn,
      email: this.email,
    });

    // Search user on username OR email
    let foundUser = await transactionManager
      .getRepository(User)
      .createQueryBuilder()
      .where('username = :username OR email = :email', {
        username: user.username,
        email: user.email,
      })
      .getOne();

    if (!foundUser) {
      // Create User, with default device and preference
      foundUser = await this.authService.createUserWithDefaults(user);
    } else {
      // username is set, update mail if different
      if (foundUser.username && user.email && user.email !== foundUser.email) {
        await this.authService.updateUserEmail(foundUser, user.email);
      }
      // no username, means external email that never logged in
      // update the username
      if (!foundUser.username && !foundUser.lastLogin) {
        await this.authService.updateUserUsername(foundUser, user.username);
      }
    }

    await Promise.all([
      this.authService.updateUserLastLogin(foundUser),
      this.authService.upsertMattermostDevice(foundUser),
      this.authService.upsertPhoneDevice(foundUser),
    ]);

    console.debug('Updated user %s', foundUser.email);
  }
}
