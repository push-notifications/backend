import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../../models/user';
import { Device } from '../../../../models/device';

export class UpsertMattermostDevice implements Command {
  constructor(private user: User) {}

  async execute(transactionManager: EntityManager): Promise<Device> {
    try {
      // Get Mattermost device if exists
      let mmdevice = await transactionManager.findOne(Device, {
        relations: ['user'],
        where: {
          user: {
            id: this.user.id,
          },
          type: 'APP',
          subType: 'MATTERMOST',
        },
      });
      if (!mmdevice) {
        // Create Mattermost Device with user mail address
        mmdevice = await transactionManager.save(
          new Device({
            name: 'Mattermost',
            user: this.user,
            info: this.user.email,
            type: 'APP',
            subType: 'MATTERMOST',
            token: this.user.email,
          }),
        );
      }

      if (mmdevice.info !== this.user.email) {
        await transactionManager.update(
          Device,
          {
            id: mmdevice.id,
          },
          {
            name: mmdevice.name,
            info: this.user.email,
            token: this.user.email,
          },
        );
      }

      console.debug('Updated user mm device %s', this.user.email);

      return mmdevice;
    } catch (ex) {
      console.error(ex);
      throw new Error('Failed to create Mattermost device.');
    }
  }
}
