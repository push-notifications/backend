import { Command } from "../../command";
import { EntityManager } from "typeorm";
import { User } from "../../../../models/user";

export class UpdateUserUsername implements Command {

    constructor(private user: User, private username: string) { }

    async execute(transactionManager: EntityManager): Promise<User> {
        try {
            // Update User
            this.user.username = this.username;
            transactionManager.save(this.user);

            return this.user;
        } catch (ex) {
            console.error(ex, this.username);
            throw new Error("Failed to update user with new username.");
        }
    }
}
