import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../../models/user';

export class UpdateUserLastLogin implements Command {
  constructor(private user: User) {}

  async execute(transactionManager: EntityManager): Promise<User> {
    try {
      // Update User
      this.user.lastLogin = new Date();
      await transactionManager.save(this.user);

      console.debug('Updated user last login %s', this.user.email);

      return this.user;
    } catch (ex) {
      console.error(ex);
      throw new Error('Failed to update user last login date.');
    }
  }
}
