import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../../models/user';
import { Device, DeviceSubType, DeviceType } from '../../../../models/device';
import { CERNActiveDirectory } from '../../../../models/cern-activedirectory';

export class UpsertPhoneDevice implements Command {
  constructor(private user: User) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    let userADentry = undefined;
    try {
      userADentry = await CERNActiveDirectory.getUser(this.user.email);
    } catch (ex) {
      console.error('Failed to get user AD entry', ex);
      return;
    }

    console.debug(userADentry);
    const token = userADentry?.phone ? `${userADentry.phone}@mail2sms.cern.ch` : '';
    try {
      const phoneDevice = await transactionManager
        .getRepository(Device)
        .createQueryBuilder('device')
        .leftJoin('device.user', 'user')
        .where('user.id = :userid', {
          userid: this.user.id,
        })
        .andWhere('device.type = :deviceType', {
          deviceType: DeviceType.PHONE,
        })
        .andWhere('device.subType = :deviceSubtype', {
          deviceSubtype: DeviceSubType.PHONE_MAIL2SMS,
        })
        .getOne();
      if (!phoneDevice) {
        await transactionManager.save(
          new Device({
            name: 'CERN Phone',
            user: this.user,
            info: userADentry?.phone || null,
            type: DeviceType.PHONE,
            subType: DeviceSubType.PHONE_MAIL2SMS,
            token: token,
          }),
        );

        return;
      }

      if (phoneDevice.token !== token) {
        await transactionManager.update(
          Device,
          {
            id: phoneDevice.id,
          },
          {
            name: phoneDevice.name,
            info: userADentry?.phone || null,
            token: token,
          },
        );
        console.debug('Updated user phone device %s', this.user.email);
      }
    } catch (ex) {
      console.error(ex);
      throw new Error('Failed to create CERN phone device.');
    }
  }
}
