import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../../models/user';
import { Device } from '../../../../models/device';

export class UpdateUserEmail implements Command {
  constructor(private user: User, private email: string) {}

  async execute(transactionManager: EntityManager): Promise<User> {
    try {
      // Update default device with new mail
      const device = await transactionManager.findOne(Device, {
        relations: ['user'],
        where: {
          user: {
            id: this.user.id,
          },
          name: this.user.email,
          type: 'MAIL',
          token: this.user.email,
        },
      });
      if (device) {
        device.name = this.email;
        device.token = this.email;
        transactionManager.save(device);
      }

      // Update Mattermost device with new mail
      const mmdevice = await transactionManager.findOne(Device, {
        relations: ['user'],
        where: {
          user: {
            id: this.user.id,
          },
          info: this.user.email,
          type: 'APP',
          subType: 'MATTERMOST',
          token: this.user.email,
        },
      });
      if (mmdevice) {
        mmdevice.info = this.email;
        mmdevice.token = this.email;
        transactionManager.save(mmdevice);
      }

      // Update User
      this.user.email = this.email;
      transactionManager.save(this.user);

      return this.user;
    } catch (ex) {
      console.error(ex, this.email);
      throw new Error('Failed to update user or device with new email.');
    }
  }
}
