import { Command } from '../../command';
import { CernOauth } from '../../../../models/cern-oauth';
import { AuthService } from '../../../auth-service';
import { ServiceFactory } from '../../../services-factory';
import * as jwt from 'jsonwebtoken';
import { JwtPayload } from 'jsonwebtoken';

export class RefreshTokens implements Command {
  private cernOauth: CernOauth;
  authService: AuthService = ServiceFactory.getAuthenticationService();

  constructor(private refreshToken: string) {
    this.cernOauth = new CernOauth();
  }

  async execute(): Promise<{
    randomString: string;
    access_token: string;
    refresh_token: string;
  }> {
    try {
      const tokens = await this.cernOauth.refreshTokens(this.refreshToken);
      const payload = jwt.decode(tokens.access_token) as JwtPayload;
      await this.authService.upsertUser(payload.cern_upn, payload.email);

      return tokens;
    } catch (e) {
      throw new Error('Error when trying to refresh the user authentication: ' + e);
    }
  }
}
