import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../../models/user';

export class GetAuthenticatedUser implements Command {
  constructor(private username: string) {}

  async execute(transactionManager: EntityManager): Promise<User> {
    return await transactionManager.findOneBy(User, { username: this.username });
  }
}
