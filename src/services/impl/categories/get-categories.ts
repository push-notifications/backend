import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Category } from '../../../models/category';
import { AuthorizationBag } from '../../../models/authorization-bag';

export class GetCategories implements Command {
  constructor(private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<Category[]> {
    return transactionManager.getTreeRepository(Category).findTrees();
  }
}
