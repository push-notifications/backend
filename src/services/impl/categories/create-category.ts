import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Category } from '../../../models/category';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { BadRequestError } from 'routing-controllers';

export class CreateCategory implements Command {
  constructor(private category, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<Category> {
    // Check category length
    if (this.category.name.length < 2 || this.category.name.length > 70) {
      throw new BadRequestError('Category should be between 2 and 70 characters.');
    }
    // Check if category already exists
    if (
      await transactionManager.findOneBy(Category, {
        name: this.category.name,
      })
    ) {
      throw new BadRequestError('Category already exists');
    }

    const newCategory = new Category({ ...this.category });

    if (this.category.parent) {
      const parentCategory = await transactionManager.findOneBy(Category, {
        name: this.category.parent,
      });
      if (!parentCategory) {
        throw new BadRequestError('Parent Category does not exist.');
      }
      newCategory.parent = parentCategory;
    }
    return transactionManager.save(newCategory);
  }
}
