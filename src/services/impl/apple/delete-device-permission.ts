import { Command } from "../command";
import { EntityManager } from "typeorm";
import { Device } from "../../../models/device";

export class DeleteDevicePermissionPolicy implements Command {

  constructor(private authorization: string, private deviceToken: string) { }

  async execute(transactionManager: EntityManager) {
    // Get the anonid from the authorization header
    if (!this.authorization || this.authorization.indexOf("ApplePushNotifications ") < 0)
      throw new Error("Invalid authorization header in Apple RegisterOrUpdateDevicePermissionPolicy.");

    // TODO: Decode encrypted uuid
    const uuid = this.authorization.substring("ApplePushNotifications ".length);
    if (!uuid)
      throw new Error("The anon identifier was not found, RegisterOrUpdateDevicePermissionPolicy failed.");

    // Delete device from the DB, using UUID and DeviceToken
    let result = await transactionManager.delete(Device, {
      uuid: uuid,
      token: this.deviceToken,
    });

    // For now, return true for all cases. 
    // If DB delete failed, user will have to delete from DeviceList which is not an issue.
    if (result.affected !== 1) 
      console.error("DeleteDevicePermissionPolicy: The Safari device does not exist, identifier or token not found.");
    
    return true;
  }
}
