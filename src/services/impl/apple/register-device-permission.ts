import { Command } from "../command";
import { EntityManager } from "typeorm";
import { Device } from "../../../models/device";

export class RegisterOrUpdateDevicePermissionPolicy implements Command {

  constructor(private authorization: string, private deviceToken: string) { }

  async execute(transactionManager: EntityManager) {
    // Get the anonid from the authorization header
    if (!this.authorization || this.authorization.indexOf("ApplePushNotifications ") < 0)
      throw new Error("Invalid authorization header in Apple RegisterOrUpdateDevicePermissionPolicy.");

    // TODO: Decode encrypted uuid
    const uuid = this.authorization.substring("ApplePushNotifications ".length);
    if (!uuid)
      throw new Error("The anon identifier was not found, RegisterOrUpdateDevicePermissionPolicy failed.");

    // Nothing for create, the device was already saved in the DB by the frontend, the device token as well.

    // But Update record token for the specified UUID, in case the cert was renewed for example
    let result = await transactionManager.update(
      Device,
      { uuid: uuid },
      { token: this.deviceToken });

    // return true in all cases. 
    // Update will fail for first registrations, as the web-portal AddDevice form did not yet save the device   
    return true;
  }
}
