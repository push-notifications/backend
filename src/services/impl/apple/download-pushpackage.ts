import { Command } from "../command";
import { EntityManager } from "typeorm";
import { createPushPackage } from "../../../push-browser/apple-pushpackage-generator"

export class DownloadPushPackage implements Command {

  constructor(private userInfo) {}

  async execute(transactionManager: EntityManager) {
    // Get the anonid from the PostBody json
    if (!this.userInfo)
      throw new Error("The anon indentifier was not found, pushPackage creation failed.");

    // Rebuild a pushPackage for this user with the anonid and Send it back to Safari
    return createPushPackage(this.userInfo);
  }
}
