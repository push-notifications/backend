import { AbstractService } from "./abstract-service";
import { PreferencesServiceInterface } from "../preferences-service";
import { Preference } from "../../models/preference";
import { CreateUserPreference } from "./preferences/create-user-preference";
import { GetUserPreferences } from "./preferences/get-user-preferences";
import { DeleteUserPreference } from "./preferences/delete-user-preference";
import { SetEnabledGlobalPreference } from "./preferences/set-enabled-global-preferences";
import { UpdateUserPreference } from "./preferences/update-user-preference";
import { AuthorizationBag } from "../../models/authorization-bag";

export class PreferencesService
  extends AbstractService
  implements PreferencesServiceInterface {
  createUserPreference(
    preference: Preference,
    authorizationBag: AuthorizationBag
  ): Promise<Preference> {
    return this.commandExecutor.execute(
      new CreateUserPreference(preference, authorizationBag)
    );
  }

  getUserPreferences(
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<Preference[]> {
    return this.commandExecutor.execute(
      new GetUserPreferences(channelId, authorizationBag)
    );
  }

  deleteUserPreferenceById(
    preferenceId: string,
    authorizationBag: AuthorizationBag
  ): Promise<Preference> {
    return this.commandExecutor.execute(
      new DeleteUserPreference(preferenceId, authorizationBag)
    );
  }

  setEnabledGlobalPreference(
    authorizationBag: AuthorizationBag,
    channelId: string,
    preferenceId: string,
    isEnabled: boolean
  ): Promise<Preference> {
    return this.commandExecutor.execute(
      new SetEnabledGlobalPreference(
        authorizationBag,
        channelId,
        preferenceId,
        isEnabled
      )
    );
  }

  updateUserPreference(
    preferenceId: string,
    preference: Preference,
    authorizationBag: AuthorizationBag
  ): Promise<Preference> {
    return this.commandExecutor.execute(
      new UpdateUserPreference(preferenceId, preference, authorizationBag)
    );
  }

}
