import { AbstractService } from './abstract-service';
import { UsersServiceInterface } from '../users-service';
import { User } from '../../models/user';
import { GetUserFromExternal } from './users/get-user-from-external';
import { GetOrCreateUserFromUsernameOrEmail } from './users/get-or-create-user';

export class UsersServiceImpl extends AbstractService implements UsersServiceInterface {
  GetOrCreateUserFromUsernameOrEmail(user: string): Promise<User> {
    return this.commandExecutor.execute(new GetOrCreateUserFromUsernameOrEmail(user));
  }

  GetUserFromExternal(user: string): Promise<User> {
    return this.commandExecutor.execute(new GetUserFromExternal(user));
  }
}
