import { Command } from '../command';
import { User } from '../../../models/user';
import { NotFoundError } from 'routing-controllers';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { usernameFromEmailIfValid } from '../../../utils/username-from-email';

export class GetUserFromExternal implements Command {
  constructor(private userIdentifier: string) {}

  async execute(): Promise<User> {
    let acUser = await CernAuthorizationService.getUser(this.userIdentifier);
    // If not found, let's verify if email is like login@cern.ch to re-initiate a search on login
    // To prevent creating unconfirmed entries on login@cern.ch
    if (!acUser) {
      const username = usernameFromEmailIfValid(this.userIdentifier);
      if (username) acUser = await CernAuthorizationService.getUser(username);
    }

    // User not found, and lookup is not an email so we can't create an email only user
    if (!acUser && !this.userIdentifier.includes('@')) throw new NotFoundError('The user does not exist');

    if (acUser) {
      console.debug('Return known user', acUser);
      return new User({ username: acUser.upn, email: acUser.mail });
    }

    console.debug('Return email only user', this.userIdentifier);
    return new User({ email: this.userIdentifier });
  }
}
