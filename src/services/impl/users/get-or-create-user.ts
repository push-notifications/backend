import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../models/user';
import { ServiceFactory } from '../../services-factory';
import { AuthService } from '../../auth-service';
import { UsersServiceInterface } from '../../users-service';
import { usernameFromEmailIfValid } from '../../../utils/username-from-email';
import { CERNActiveDirectory } from '../../../models/cern-activedirectory';

export class GetOrCreateUserFromUsernameOrEmail implements Command {
  constructor(private userIdentifier: string) {}

  authService: AuthService = ServiceFactory.getAuthenticationService();
  usersService: UsersServiceInterface = ServiceFactory.getUserService();

  async execute(transactionManager: EntityManager): Promise<User> {
    let userToAdd = await transactionManager.findOne(User, {
      where: [{ username: this.userIdentifier }, { email: this.userIdentifier }],
    });

    // If not found, let's verify if email is like login@cern.ch to reinitiate a search on login
    if (!userToAdd && this.userIdentifier && this.userIdentifier.endsWith('@cern.ch')) {
      const userFromCernAlias = await CERNActiveDirectory.getUser(this.userIdentifier);
      if (userFromCernAlias) {
        // email alias resolved to primary, let's use that one now
        this.userIdentifier = userFromCernAlias.email;
        userToAdd = await transactionManager.findOne(User, {
          where: this.userIdentifier ? { username: this.userIdentifier } : { email: this.userIdentifier },
        });
      } else {
        const username = usernameFromEmailIfValid(this.userIdentifier);
        if (username) {
          userToAdd = await transactionManager.findOne(User, {
            where: [{ username: username }],
          });
        }
      }
    }

    if (!userToAdd) {
      const user = await this.usersService.GetUserFromExternal(this.userIdentifier);
      // Check again in DB if user already there in case it's a mapped user in Auth systems
      userToAdd = await transactionManager.findOne(User, {
        where: user.username ? { username: user.username } : { email: user.email },
      });
      if (!userToAdd) userToAdd = await this.authService.createUserWithDefaults(user);
    }
    return userToAdd;
  }
}
