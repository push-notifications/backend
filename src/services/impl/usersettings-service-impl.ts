import { AbstractService } from "./abstract-service";
import { UserSettingsServiceInterface } from "../usersettings-service";
import { SaveDraft } from "./usersettings/save-draft";
import { GetDraft } from "./usersettings/get-draft";
import { DeleteDraft } from "./usersettings/delete-draft";
import { AuthorizationBag } from "../../models/authorization-bag";
import { UserSettings } from "../../models/user-settings";
import { GetUserSettings } from "./usersettings/get-user-settings";

export class UserSettingsService
  extends AbstractService
  implements UserSettingsServiceInterface {

    getUserSettings(
      authorizationBag: AuthorizationBag
    ): Promise<UserSettings> {
      return this.commandExecutor.execute(
        new GetUserSettings(authorizationBag)
      );
    }

  saveDraft(
    draft: string,
    authorizationBag: AuthorizationBag
  ): Promise<string> {
    return this.commandExecutor.execute(
      new SaveDraft(draft, authorizationBag)
    );
  }

  getDraft(
    authorizationBag: AuthorizationBag,
  ): Promise<string> {
    return this.commandExecutor.execute(
      new GetDraft(authorizationBag)
    );
  }

  deleteDraft(
    authorizationBag: AuthorizationBag,
  ): Promise<boolean> {
    return this.commandExecutor.execute(
      new DeleteDraft(authorizationBag)
    );
  }
}
