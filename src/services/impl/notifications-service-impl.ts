import { NotificationsService } from '../notifications-service';
import { SendNotification } from './notifications/send-notification';
import { FindAllNotifications } from './notifications/find-all-notifications';
import { GetById } from './notifications/get-by-id';
import { GetNotificationAuditById } from './notifications/get-notification-audit-by-id';
import { AbstractService } from './abstract-service';
import { AuthorizationBag } from '../../models/authorization-bag';
import { RetryNotification } from './notifications/retry-notification';
import {
  GetNotificationResponse,
  NotificationTargetUsersResponse,
  NotificationTargetGroupsResponse,
  SendNotificationRequest,
} from '../../controllers/notifications/dto';
import { NotificationsListResponse, Query } from '../../controllers/channels/dto';
import { DeleteNotificationById } from './notifications/delete-notification-by-id';
import { GetNotificationTargetGroups } from './notifications/get-notification-target-groups';
import { GetNotificationTargetUsers } from './notifications/get-notification-target-users';

export class NotificationsServiceImpl extends AbstractService implements NotificationsService {
  sendNotification(
    notification: SendNotificationRequest,
    authorizationBag: AuthorizationBag,
  ): Promise<GetNotificationResponse> {
    return this.commandExecutor.execute(new SendNotification(notification, authorizationBag));
  }

  retryNotification(notificationId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new RetryNotification(notificationId, authorizationBag));
  }

  findAllNotifications(
    channelId: string,
    query: Query,
    authorizationBag: AuthorizationBag,
  ): Promise<NotificationsListResponse> {
    return this.commandExecutor.execute(new FindAllNotifications(channelId, query, authorizationBag));
  }

  getNotificationTargetGroups(
    notificationId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<NotificationTargetGroupsResponse> {
    return this.commandExecutor.execute(new GetNotificationTargetGroups(notificationId, authorizationBag));
  }

  getNotificationTargetUsers(
    notificationId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<NotificationTargetUsersResponse> {
    return this.commandExecutor.execute(new GetNotificationTargetUsers(notificationId, authorizationBag));
  }

  getById(notificationId: string, authorizationBag: AuthorizationBag): Promise<GetNotificationResponse> {
    return this.commandExecutor.execute(new GetById(notificationId, authorizationBag));
  }

  getNotificationAuditById(notificationId: string, authorizationBag: AuthorizationBag): Promise<JSON> {
    return this.commandExecutor.execute(new GetNotificationAuditById(notificationId, authorizationBag));
  }

  deleteNotificationById(notificationId: string, authorizationBag: AuthorizationBag): Promise<void> {
    return this.commandExecutor.execute(new DeleteNotificationById(notificationId, authorizationBag));
  }
}
