import { Command } from '../command';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditIPAddress } from '../../../log/auditing';
import { EntityManager } from 'typeorm';
import { ForbiddenError } from 'routing-controllers';

export class GetAuditIPAdress implements Command {
  constructor(private IPAddress: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    if (!this.authorizationBag.isSupporter) throw new ForbiddenError('Access forbidden');

    return AuditIPAddress.getValuesAsJson(this.IPAddress);
  }
}
