import { Command } from '../command';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { EntityManager } from 'typeorm';
import { ForbiddenError } from 'routing-controllers';

export class GetAuditChannels implements Command {
  constructor(private id: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    if (!this.authorizationBag.isSupporter)
      throw new ForbiddenError('Access forbidden');

    return AuditChannels.getValuesAsJson(this.id);
  }
}
