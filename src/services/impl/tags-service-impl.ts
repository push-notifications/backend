import { AbstractService } from "./abstract-service";
import { TagsServiceInterface } from "../tags-service";
import { Tag } from "../../models/tag";
import { CreateTag } from "./tags/create-tag";
import { GetTagsByPrefix } from "./tags/get-tags-by-prefix";
import { GetTags } from "./tags/get-tags";
import { AuthorizationBag } from "../../models/authorization-bag";

export class TagsService
  extends AbstractService
  implements TagsServiceInterface {
  createTag(
    tag: Tag,
    authorizationBag: AuthorizationBag
  ): Promise<Tag> {
    return this.commandExecutor.execute(
      new CreateTag(tag, authorizationBag)
    );
  }

  getTags(
    authorizationBag: AuthorizationBag,
  ): Promise<Tag[]> {
    return this.commandExecutor.execute(
      new GetTags(authorizationBag)
    );
  }

  getTagsByPrefix(
    prefix: string,
    authorizationBag: AuthorizationBag,
  ): Promise<Tag[]> {
    return this.commandExecutor.execute(
      new GetTagsByPrefix(prefix, authorizationBag)
    );
  }
}
