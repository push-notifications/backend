import { AbstractService } from './abstract-service';
import { AuditServiceInterface } from '../audit-service';
import { GetAuditNotifications } from './audit/get-audit-notifications';
import { GetAuditChannels } from './audit/get-audit-channels';
import { GetAuditIPAdress } from './audit/get-audit-ip-address';
import { AuthorizationBag } from '../../models/authorization-bag';

export class AuditService extends AbstractService implements AuditServiceInterface {
  getAuditNotifications(id: string, authorizationBag: AuthorizationBag): Promise<any> {
    return this.commandExecutor.execute(new GetAuditNotifications(id, authorizationBag));
  }

  getAuditChannels(id: string, authorizationBag: AuthorizationBag): Promise<any> {
    return this.commandExecutor.execute(new GetAuditChannels(id, authorizationBag));
  }

  getAuditIPAdress(IPAddress: string, authorizationBag: AuthorizationBag): Promise<any> {
    return this.commandExecutor.execute(new GetAuditIPAdress(IPAddress, authorizationBag));
  }
}
