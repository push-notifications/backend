import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { ServiceStatus } from '../../../models/service-status';

export class SetServiceStatus implements Command {
  constructor(private message: string, private validity?: number) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    await transactionManager.save(new ServiceStatus({ message: this.message, validity: this.validity }));
  }
}
