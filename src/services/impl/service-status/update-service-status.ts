import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { ServiceStatus } from '../../../models/service-status';
import { ServiceStatusMessage } from '../../../controllers/service-status/dto';

export class UpdateServiceStatus implements Command {
  constructor(private serviceStatusMessage: ServiceStatusMessage) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    await transactionManager.update(
      ServiceStatus,
      {
        id: this.serviceStatusMessage.id,
      },
      {
        message: this.serviceStatusMessage.message,
        validity: this.serviceStatusMessage.validity,
      },
    );
  }
}
