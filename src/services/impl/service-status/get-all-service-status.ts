import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { ServiceStatus } from '../../../models/service-status';
import { Query, ServiceStatusListResponse } from '../../../controllers/service-status/dto';

export class GetAllServiceStatus implements Command {
  constructor(private query: Query) {}
  async execute(transactionManager: EntityManager): Promise<ServiceStatusListResponse> {
    const messages = await transactionManager
      .getRepository(ServiceStatus)
      .createQueryBuilder('servicestatus')
      .orderBy('servicestatus.created', 'DESC')
      .limit(this.query.take || 10)
      .offset(this.query.skip || 0)
      .getMany();

    return new ServiceStatusListResponse(
      messages,
      await transactionManager.getRepository(ServiceStatus).createQueryBuilder('servicestatus').getCount(),
    );
  }
}
