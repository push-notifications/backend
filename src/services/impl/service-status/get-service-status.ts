import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { ServiceStatus } from '../../../models/service-status';
import { ServiceStatusResponse } from '../../../controllers/service-status/dto';

export class GetServiceStatus implements Command {
  async execute(transactionManager: EntityManager): Promise<ServiceStatusResponse> {
    try {
      return await transactionManager
        .getRepository(ServiceStatus)
        .createQueryBuilder('servicestatus')
        .where("created + validity * INTERVAL '1 day' > CURRENT_TIMESTAMP")
        .orderBy('created', 'DESC') // get only the latest if multiple rows found
        .getOneOrFail();
    } catch {
      return { message: null } as ServiceStatus;
    }
  }
}
