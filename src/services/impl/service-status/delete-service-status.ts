import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { ServiceStatus } from '../../../models/service-status';
import { BadRequestError, NotFoundError } from 'routing-controllers';

export class DeleteServiceStatus implements Command {
  constructor(private serviceStatusId: string) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    try {
      const result = await transactionManager.delete(ServiceStatus, {
        id: this.serviceStatusId,
      });
      if (result.affected === 1) return;
    } catch (ex) {
      throw new BadRequestError(ex.message || 'internal error');
    }

    throw new NotFoundError('The Service Status does not exist');
  }
}
