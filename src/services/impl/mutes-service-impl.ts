import { AbstractService } from "./abstract-service";
import { MutesServiceInterface } from "../mutes-service";
import { Mute } from "../../models/mute";
import { CreateUserMute } from "./mutes/create-user-mute";
import { GetUserMutes } from "./mutes/get-user-mutes";
import { DeleteUserMute } from "./mutes/delete-user-mute";
import { UpdateUserMute } from "./mutes/update-user-mute";
import { CreateUnSubscribeMute } from "./mutes/create-unsubscribe-mute";
import { AuthorizationBag } from "../../models/authorization-bag";

export class MutesService
  extends AbstractService
  implements MutesServiceInterface {
  createUserMute(
    mute: Mute,
    authorizationBag: AuthorizationBag
  ): Promise<Mute> {
    return this.commandExecutor.execute(
      new CreateUserMute(mute, authorizationBag)
    );
  }

  getUserMutes(
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<Mute[]> {
    return this.commandExecutor.execute(
      new GetUserMutes(channelId, authorizationBag)
    );
  }

  deleteUserMuteById(
    muteId: string,
    authorizationBag: AuthorizationBag
  ): Promise<Mute> {
    return this.commandExecutor.execute(
      new DeleteUserMute(muteId, authorizationBag)
    );
  }

  updateUserMute(
    muteId: string,
    mute: Mute,
    authorizationBag: AuthorizationBag
  ): Promise<Mute> {
    return this.commandExecutor.execute(
      new UpdateUserMute(muteId, mute, authorizationBag)
    );
  }

  createUnSubscribe(
    mute: Mute,
    blob: string,
    email: string,
  ): Promise<Mute> {
    return this.commandExecutor.execute(
      new CreateUnSubscribeMute(mute, blob, email)
    );
  }

}
