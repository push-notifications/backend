import { Command } from '../command';
import { EntityManager, IsNull } from 'typeorm';
import { decodeEmail, Mute } from '../../../models/mute';
import { User } from '../../../models/user';
import { BadRequestError } from 'routing-controllers';
import { AuthService } from '../../../services/auth-service';
import { ServiceFactory } from '../../../services/services-factory';

export class CreateUnSubscribeMute implements Command {
  constructor(private mute: Mute, private blob: string, private email: string) {}

  authService: AuthService = ServiceFactory.getAuthenticationService();

  async execute(transactionManager: EntityManager) {
    try {
      if (!this.email || !this.blob) throw new Error('Error: missing email or blob in request');
      if (decodeEmail(this.blob) !== this.email) throw new Error('Error: tempered unsubscribe request');
    } catch (e) {
      console.error(e);
      // Return a generic error, no need to explain why it fails
      throw new BadRequestError('Error validating unsubscribe request');
    }

    // Retrieve existing user if any for this email
    let user = await transactionManager.findOneBy(User, { email: this.email });

    // Check if a global permanent unsubscribe record already exists for this user
    if (
      user &&
      (await transactionManager.findOne(Mute, {
        relations: ['user', 'target'],
        where: {
          user: user,
          target: IsNull(),
          type: 'PERMANENT',
        },
      }))
    ) {
      throw new BadRequestError('This email is already unsubscribed');
    }

    const mute = new Mute({
      type: this.mute.type,
      target: null,
      user: user,
    });

    // Check dates validity for example
    mute.validateMute(transactionManager);

    // If no user for this email, create one
    if (!user) {
      // TODO: retrieve user details in Authz Service (in MR https://gitlab.cern.ch/push-notifications/backend/-/merge_requests/93)
      const newUser = new User({ email: this.email });
      // Create User, with default device and preference
      user = await this.authService.createUserWithDefaults(newUser);
      if (!user) {
        console.error('Error creating user object for unsubscribe', this.email);
        throw new Error('Error creating User object');
      }

      // Assign mute to the newly created user
      mute.user = user;
    }

    const savedMute = await transactionManager.save(mute);
    if (savedMute) {
      return await transactionManager.findOne(Mute, {
        where: {
          id: savedMute.id,
        },
      });
    }
  }
}
