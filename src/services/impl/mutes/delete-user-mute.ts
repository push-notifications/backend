import { Command } from "../command";
import { EntityManager } from "typeorm";
import { Mute } from "../../../models/mute";
import { AuthorizationBag } from "../../../models/authorization-bag";
import { ForbiddenError } from "routing-controllers";

export class DeleteUserMute implements Command {
  constructor(private muteId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    let result = await transactionManager.delete(Mute, {
      id: this.muteId,
      user: this.authorizationBag.userId,
    });

    if (result.affected === 1) return this.muteId;
    else
      throw new ForbiddenError("The mute object does not exist or you are not the owner");
  }
}
