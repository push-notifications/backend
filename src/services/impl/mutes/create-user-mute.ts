import { Command } from "../command";
import { EntityManager } from "typeorm";
import { Mute } from "../../../models/mute";
import { Channel } from "../../../models/channel";
import { AuthorizationBag } from "../../../models/authorization-bag";
import { BadRequestError } from "routing-controllers";

export class CreateUserMute implements Command {
  constructor(private mute: Mute, private authorizationBag: AuthorizationBag) { }

  async execute(transactionManager: EntityManager) {
    const mute = new Mute({ ...this.mute, user: this.authorizationBag.userId })
    let target = null;

    if (mute.target) {
      try {
        target = await transactionManager.findOne(Channel, {
          where: {
            id: mute.target,
          },
        });
      } catch (err) {
        throw new BadRequestError("Invalid Channel: channel's ID does not exist");
      }
    }

    // Check dates validity for example
    await mute.validateMute(transactionManager);

    const savedMute = await transactionManager.save(
      new Mute({ ...mute, target })
    );
    if (savedMute) {
      return await transactionManager.findOne(Mute, {
        relations: ['target'],
        where: {
          id: savedMute.id,
        },
      });
    }
  }
}
