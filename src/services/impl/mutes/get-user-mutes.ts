import { Command } from '../command';
import { EntityManager, IsNull } from 'typeorm';
import { Mute } from '../../../models/mute';
import { AuthorizationBag } from '../../../models/authorization-bag';

export class GetUserMutes implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  private async fetchGlobalMutes(transactionManager: EntityManager) {
    return await transactionManager.find(Mute, {
      relations: ['target'],
      where: {
        user: {
          id: this.authorizationBag.userId,
        },
        target: IsNull(),
      },
    });
  }

  private async fetchChannelMutes(transactionManager: EntityManager) {
    return await transactionManager
      .getRepository(Mute)
      .createQueryBuilder('mute')
      .leftJoin('mute.user', 'user')
      .leftJoinAndSelect('mute.target', 'target')
      .where('user.id = :userId AND target.id = :channelId', {
        userId: this.authorizationBag.userId,
        channelId: this.channelId,
      })
      .getMany();
  }

  private async fetchAllChannelsMutes(transactionManager: EntityManager) {
    return await transactionManager
      .getRepository(Mute)
      .createQueryBuilder('mute')
      .leftJoin('mute.user', 'user')
      .leftJoinAndSelect('mute.target', 'target')
      .where('user.id = :userId AND target.id IS NOT NULL', {
        userId: this.authorizationBag.userId,
      })
      .getMany();
  }

  async execute(transactionManager: EntityManager) {
    const result = {
      globalMutes: await this.fetchGlobalMutes(transactionManager),
    };

    if (this.channelId) {
      result['channelMutes'] = await this.fetchChannelMutes(transactionManager);
    } else {
      result['channelMutes'] = await this.fetchAllChannelsMutes(transactionManager);
    }

    return result;
  }
}
