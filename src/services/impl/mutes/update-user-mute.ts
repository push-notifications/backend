import { Command } from "../command";
import { EntityManager } from "typeorm";
import { BadRequestError } from "routing-controllers";
import { Mute } from "../../../models/mute";
import { AuthorizationBag } from "../../../models/authorization-bag";
import { Channel } from "../../../models/channel";

export class UpdateUserMute implements Command {
  constructor(
    private muteId: string,
    private mute: Mute,
    private authorizationBag: AuthorizationBag
  ) { }

  async execute(transactionManager: EntityManager) {
    if (this.mute.id !== this.muteId)
      throw new BadRequestError("Invalid Mute update information");

    const mute = new Mute({
      ...this.mute,
      user: this.authorizationBag.userId
    });

    if (mute.target) {
      try {
        const target = await transactionManager.findOne(Channel, {
          where: {
            id: mute.target,
          },
        });
      } catch (err) {
        throw new BadRequestError("Invalid Channel: channel's ID does not exist");
      }
    }

    await mute.validateMute(transactionManager);

    // Save will also update existing objects
    const savedMute = await transactionManager.save(mute);
    if (savedMute) {
      return await transactionManager.findOne(Mute, {
        relations: ["target"],
        where: {
          id: savedMute.id,
        },
      });
    }

  }
}
