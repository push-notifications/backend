import { Command } from '../command';

export class Ping implements Command {
  async execute(): Promise<string> {
    return 'pong';
  }
}
