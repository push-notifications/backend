import { AbstractService } from "./abstract-service";
import { StatisticsServiceInterface } from "../statistics-service";
import { Statistics } from "../../models/statistics";
import { GetStatistics } from "./statistics/get-statistics";

export class StatisticsService
  extends AbstractService
  implements StatisticsServiceInterface {
  getStatistics(): Promise<Statistics> {
    return this.commandExecutor.execute(new GetStatistics());
  }
}
