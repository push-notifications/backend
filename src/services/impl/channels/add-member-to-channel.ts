import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { ServiceFactory } from '../../services-factory';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { UsersServiceInterface } from '../../users-service';
import { AuditChannels } from '../../../log/auditing';
import { MemberResponse } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class AddMemberToChannel implements Command {
  constructor(private membername: string, private channelId: string, private authorizationBag: AuthorizationBag) {}

  usersService: UsersServiceInterface = ServiceFactory.getUserService();

  async hasAccess(transactionManager: EntityManager, channel: Channel, userGroups: any[]): Promise<boolean> {
    const hasApiKeyAccess = channel.hasApiKeyAccess(this.authorizationBag);
    console.debug('hasApiKeyAccess', hasApiKeyAccess);
    if (hasApiKeyAccess) return true;

    const hasAdminAccess = await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups);
    console.debug('hasAdminAccess', hasAdminAccess);
    return !!hasAdminAccess;
  }

  async execute(transactionManager: EntityManager): Promise<MemberResponse> {
    const channel = await transactionManager.findOne(Channel, {
      relations: {
        owner: true,
        adminGroup: true,
      },
      where: { id: this.channelId },
    });

    if (!channel) {
      throw new NotFoundError('Channel does not exist');
    }

    // Get current user groups, to be used next in SQL to compare with channel groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    if (!(await this.hasAccess(transactionManager, channel, userGroups))) {
      throw new ForbiddenError("You don't have the rights to manage this channel.");
    }

    const userToAdd = await this.usersService.GetOrCreateUserFromUsernameOrEmail(this.membername.toLowerCase().trim());

    const isMemberViaUser = await channel.isMemberViaUser(transactionManager, userToAdd.id);
    console.debug('userToAdd.id', userToAdd.id);
    console.debug('isMemberViaUser', isMemberViaUser);
    if (isMemberViaUser) {
      throw new ForbiddenError(
        `User ${this.membername} / ${userToAdd.email} is already a member or in a member group.`,
      );
    }

    let memberGroups = [];
    if (userToAdd.username) {
      // Get new member groups, to be used next in SQL to compare with channel groups
      memberGroups = await CernAuthorizationService.getCurrentUserGroups(userToAdd.username);
    }

    const isMemberViaGroup = await channel.isMemberViaGroup(transactionManager, memberGroups);
    console.debug('isMemberViaGroup', isMemberViaGroup);
    if (isMemberViaGroup) {
      throw new ForbiddenError(
        `User ${this.membername} / ${userToAdd.email} is already a member or in a member group.`,
      );
    }
    // optimized save performance
    await channel.addUser(transactionManager, userToAdd);

    await AuditChannels.setValue(channel.id, {
      event: 'AddMember',
      user: this.authorizationBag.email,
      membername: this.membername,
      ip: this.authorizationBag.ip,
    });

    return new MemberResponse(userToAdd);
  }
}
