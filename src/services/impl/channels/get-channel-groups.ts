import { Command } from '../command';
import { Channel } from '../../../models/channel';
import { EntityManager, ILike } from 'typeorm';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { GroupsListResponse, Query } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { Group } from '../../../models/group';

export class GetChannelGroups implements Command {
  constructor(private channelId: string, private query: Query, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<GroupsListResponse> {
    const targetChannel = await transactionManager.findOne(Channel, {
      relations: {
        owner: true,
        adminGroup: true,
      },
      where: { id: this.channelId },
    });
    if (!targetChannel) throw new NotFoundError('Channel does not exist.');

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    if (
      !(await targetChannel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups)) &&
      !this.authorizationBag.isSupporter
    ) {
      throw new ForbiddenError('Access to channel groups not authorized.');
    }

    const options = {
      skip: this.query.skip || 0,
      take: this.query.take || 10,
      order: { groupIdentifier: 'ASC' },
    };

    if (this.query.searchText) {
      options['where'] = {
        channelGroups: {
          id: this.channelId,
        },
        groupIdentifier: ILike(`%${this.query.searchText}%`),
      };
    } else {
      options['where'] = { channelGroups: { id: this.channelId } };
    }

    const [groups, count] = await transactionManager.findAndCount(Group, options);

    return new GroupsListResponse(groups, count);
  }
}
