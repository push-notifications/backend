import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../models/user';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { ChannelFlags } from '../../../models/channel-enums';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class UnsubscribeFromChannel implements Command {
  constructor(private memberId: string, private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    const channel = await transactionManager.findOne(Channel, {
      where: { id: this.channelId },
      relations: ['owner', 'adminGroup'],
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // If removing someone else, you need to be channel admin (NOT IMPLEMENTED on web-portal for now)
    // If adding yourself then all ok
    if (this.memberId !== this.authorizationBag.userId) throw new ForbiddenError('Access to Channel not Authorized !');

    if (channel.channelFlags.includes(ChannelFlags.mandatory))
      throw new ForbiddenError(
        'This channel is mandatory, unsubscribe is not possible. You can however mute it if needed.',
      );

    // Get current user groups, to be used next in SQL to compare with channel groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    const user = await transactionManager.findOneBy(User, { id: this.memberId });
    if (!user) throw new NotFoundError('User not found');

    await channel.unsubscribeSelf(transactionManager, user, userGroups);

    await AuditChannels.setValue(channel.id, {
      event: 'UnSubscribed',
      user: this.authorizationBag.email,
      memberId: this.memberId,
    });
  }
}
