import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { GetChannelPublicResponse, GetChannelResponse } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class FindChannelById implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<GetChannelResponse | GetChannelPublicResponse> {
    const channel = await transactionManager.findOne(Channel, {
      // Specify needed joinColumns targets here, or use eager=true in model column def.
      relations: ['owner', 'adminGroup', 'category', 'tags'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // Get current user groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag?.userName);
    if (!(await channel.hasAccess(transactionManager, this.authorizationBag, userGroups))) {
      throw new ForbiddenError('Access to Channel not Authorized!');
    }

    if (!this.authorizationBag || this.authorizationBag.isAnonymous) {
      return new GetChannelPublicResponse(channel);
    }

    return new GetChannelResponse(
      channel,
      await channel.isSubscribed(transactionManager, this.authorizationBag, userGroups),
      await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups),
      await channel.canSendByForm(transactionManager, this.authorizationBag, userGroups),
    );
  }
}
