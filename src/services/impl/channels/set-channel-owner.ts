import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { User } from '../../../models/user';
import { BadRequestError, ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuthService } from '../../../services/auth-service';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { ServiceFactory } from '../../../services/services-factory';
import { AuditChannels } from '../../../log/auditing';

export class SetChannelOwner implements Command {
  constructor(private username: string, private channelId: string, private authorizationBag: AuthorizationBag) {}

  authService: AuthService = ServiceFactory.getAuthenticationService();

  async execute(transactionManager: EntityManager): Promise<Channel> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');
    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to edit this channel.");
    if (!this.username) throw new BadRequestError('Username not specified');

    let newOwner = await transactionManager.findOne(User, {
      where: [
        {
          username: this.username,
        },
        { email: this.username },
      ],
    });

    // Not found in DB, try to create from Authorization Service
    if (!newOwner) {
      const acUser = await CernAuthorizationService.getUser(this.username);
      if (acUser) {
        const user = new User({ username: acUser.upn, email: acUser.mail });
        console.debug('SetChannelOwner: Creating new known', user);
        // Create User, with default device and preference
        newOwner = await this.authService.createUserWithDefaults(user);
      }
    }

    if (!newOwner) throw new NotFoundError('User does not exist');

    channel.owner = newOwner;

    await transactionManager.save(channel);
    await AuditChannels.setValue(channel.id, {
      event: 'SetOwner',
      user: this.authorizationBag.email,
      owner: this.username,
      ip: this.authorizationBag.ip,
    });

    return channel;
  }
}
