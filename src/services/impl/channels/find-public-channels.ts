import { Command } from '../command';
import { EntityManager, Like } from 'typeorm';
import { Channel } from '../../../models/channel';
import { Visibility } from '../../../models/channel-enums';
import { GetChannelPublicResponse, PublicChannelsListResponse } from '../../../controllers/channels/dto';

export class FindPublicChannels implements Command {
  constructor(private query) {}

  async execute(transactionManager: EntityManager): Promise<PublicChannelsListResponse> {
    const [channels, count] = await transactionManager.findAndCount(Channel, {
      relations: ['owner', 'category', 'tags'],
      where: [
        {
          visibility: Visibility.public,
          description: Like(`%${this.query.searchText || ''}%`),
        },
        {
          visibility: Visibility.public,
          name: Like(`%${this.query.searchText || ''}%`),
        },
        {
          visibility: Visibility.public,
          owner: { username: Like(`%${this.query.searchText || ''}%`) },
        },
      ],
      skip: parseInt(this.query.skip) || 0,
      take: parseInt(this.query.take) || 10,
      order: {
        name: 'ASC',
      },
    });
    return new PublicChannelsListResponse(
      channels.map(channel => {
        return new GetChannelPublicResponse(channel);
      }),
      count,
    );
  }
}
