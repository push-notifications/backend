import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { User } from '../../../models/user';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class RemoveUserFromChannel implements Command {
  constructor(private memberId: string, private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    const channel = await transactionManager.findOne(Channel, {
      where: { id: this.channelId },
      relations: ['owner', 'adminGroup'],
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // Get current user groups, to be used next in SQL to compare with channel groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    // If removing someone else, you need to be channel admin (NOT IMPLEMENTED on web-portal for now)
    // If adding yourself then all ok
    if (
      this.memberId !== this.authorizationBag.userId &&
      !(await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups))
    )
      throw new ForbiddenError('Access to Channel not Authorized !');

    const user = await transactionManager.findOneBy(User, { id: this.memberId });
    if (!user) throw new NotFoundError('User not found');

    await channel.removeUser(transactionManager, user);

    await AuditChannels.setValue(channel.id, {
      event: 'RemoveMember',
      user: this.authorizationBag.email,
      memberId: this.memberId,
      ip: this.authorizationBag.ip,
    });
  }
}
