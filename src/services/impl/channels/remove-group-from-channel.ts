import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { Group } from '../../../models/group';
import { BadRequestError, ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { isUUID } from 'class-validator';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class RemoveGroupFromChannel implements Command {
  constructor(private groupId: string, private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    if (!isUUID(this.channelId))
      throw new BadRequestError('Provided channel id: "' + this.channelId + '" is not a UUID.');

    if (!isUUID(this.groupId)) throw new BadRequestError('Provided group id: "' + this.groupId + '" is not a UUID.');

    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    // you need to be channel admin
    if (!(await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups)))
      throw new ForbiddenError("You don't have the rights to manage this channel.");

    const group = await transactionManager.findOne(Group, {
      where: { id: this.groupId },
    });

    if (!group) throw new NotFoundError('Group not found');

    await channel.removeGroup(transactionManager, group);
    await AuditChannels.setValue(channel.id, {
      event: 'RemoveGroup',
      user: this.authorizationBag.email,
      groupId: this.groupId,
      ip: this.authorizationBag.ip,
    });
  }
}
