import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ChannelStatsResponse } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class GetChannelStats implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<ChannelStatsResponse> {
    const channel = await transactionManager.findOne(Channel, {
      // Specify needed joinColumns targets here, or use eager=true in model column def.
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    const userGroups = (await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag?.userName)) || [];

    if (!(await channel.hasAccess(transactionManager, this.authorizationBag, userGroups)))
      throw new ForbiddenError('Access to Channel not Authorized !');

    return new ChannelStatsResponse(channel);
  }
}
