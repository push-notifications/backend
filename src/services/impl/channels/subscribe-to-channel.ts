import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { User } from '../../../models/user';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class SubscribeToChannel implements Command {
  constructor(private memberId: string, private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    const channel = await transactionManager.findOne(Channel, {
      where: { id: this.channelId },
      relations: ['owner', 'adminGroup'],
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    if (this.memberId !== this.authorizationBag.userId) throw new ForbiddenError('Access to Channel not Authorized !');

    // Get current user groups, to be used next in SQL to compare with channel groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    if (!(await channel.hasAccess(transactionManager, this.authorizationBag, userGroups)))
      throw new ForbiddenError('Access to Channel not Authorized !');

    const user = await transactionManager.findOneBy(User, { id: this.memberId });
    if (!user) throw new NotFoundError('User not found');
    await channel.subscribeSelf(transactionManager, user, userGroups);

    await AuditChannels.setValue(channel.id, {
      event: 'Subscribed',
      user: this.authorizationBag.email,
      memberId: this.memberId,
    });
  }
}
