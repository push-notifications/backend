import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { EditChannelResponse } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

export class EditChannelById implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<EditChannelResponse> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup', 'category', 'tags'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) {
      throw new NotFoundError('Channel does not exist');
    }

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    const hasAdminAccess = async (): Promise<boolean> => {
      return await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups);
    };

    if (this.authorizationBag.isSupporter || (await hasAdminAccess())) {
      return new EditChannelResponse(channel);
    }

    throw new ForbiddenError('Access to Channel not Authorized !');
  }
}
