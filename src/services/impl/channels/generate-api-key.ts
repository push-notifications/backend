import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';

export class GenerateApiKey implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<string> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to generate an API Key for this channel.");

    const key = channel.generateAPIkey();
    transactionManager.save(channel);
    await AuditChannels.setValue(channel.id, {
      event: 'SetApiKey',
      user: this.authorizationBag.email,
      ip: this.authorizationBag.ip,
    });

    return 'ck_' + this.channelId + '_' + key;
  }
}
