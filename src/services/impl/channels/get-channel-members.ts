import { Command } from '../command';
import { Channel } from '../../../models/channel';
import { EntityManager, ILike, Like } from 'typeorm';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { MembersListResponse, Query } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { User } from '../../../models/user';

export class GetChannelMembers implements Command {
  constructor(private channelId: string, private query: Query, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<MembersListResponse> {
    const targetChannel = await transactionManager.findOne(Channel, {
      relations: {
        owner: true,
        adminGroup: true,
      },
      where: { id: this.channelId },
    });
    if (!targetChannel) {
      throw new NotFoundError('Channel does not exist.');
    }

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    if (
      !(await targetChannel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups)) &&
      !this.authorizationBag.isSupporter
    ) {
      throw new ForbiddenError('Access to channel members not authorized.');
    }

    const options = {
      skip: this.query.skip || 0,
      take: this.query.take || 10,
      order: { username: `ASC`, email: `ASC` },
    };

    if (this.query.searchText) {
      options['where'] = [
        {
          subscriptions: {
            id: this.channelId,
          },
          email: ILike(`%${this.query.searchText}%`),
        },
        {
          subscriptions: {
            id: this.channelId,
          },
          username: ILike(`%${this.query.searchText}%`),
        },
      ];
    } else {
      options['where'] = { subscriptions: { id: this.channelId } };
    }

    const [users, count] = await transactionManager.findAndCount(User, options);

    return new MembersListResponse(users, count);
  }
}
