import { validate } from 'class-validator';
import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { BadRequestError, NotFoundError } from 'routing-controllers';
import { Channel } from '../../../models/channel';
import { Group } from '../../../models/group';
import { User } from '../../../models/user';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { prepareValidationErrorList } from './validation-utils';
import { AuditChannels } from '../../../log/auditing';
import { ChannelResponse, CreateChannelRequest } from '../../../controllers/channels/dto';
import { SubscriptionPolicy } from '../../../models/channel-enums';
import { createShortURL } from './manage-short-url';

export class CreateChannel implements Command {
  constructor(private channel: CreateChannelRequest, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<ChannelResponse> {
    // whitelist: strip all properties that don't have any decorators
    await validate(this.channel, { whitelist: true }).then(errors => {
      if (errors.length > 0) throw new BadRequestError('Form validation failed: ' + prepareValidationErrorList(errors));
    });

    // Get owner (user who have created the channel) to set as user by default
    const user = await transactionManager.findOneBy(User, {
      id: this.authorizationBag.userId,
    });

    const channel = new Channel({
      ...this.channel,
      owner: this.authorizationBag.userId,
      members: [user],
      subscriptionPolicy: SubscriptionPolicy.dynamic, //TODO TO be removed once feature is added
    });
    channel.name = channel.name.trim();

    if (channel.name.length < Channel.MIN_NAME_SIZE || channel.name.length > Channel.MAX_NAME_SIZE) {
      throw new BadRequestError("Channel's name should be between 4 and 128 characters.");
    }
    if (channel.description.length > Channel.MAX_DESCRIPTION_SIZE) {
      throw new BadRequestError("Channel's description should be less than 256 characters.");
    }

    if (
      await transactionManager.findOneBy(Channel, {
        name: channel.name,
      })
    ) {
      throw new BadRequestError('Channel name already exists');
    }
    if (
      await transactionManager.findOneBy(Channel, {
        slug: channel.slug,
      })
    ) {
      throw new BadRequestError('Channel slug already exists');
    }

    if (this.channel.adminGroup) {
      const adminGroup = await transactionManager.findOneBy(Group, { groupIdentifier: this.channel.adminGroup });
      if (!adminGroup) throw new NotFoundError('Admin group does not exist');
      if (adminGroup.groupIdentifier != this.channel.adminGroup)
        throw new Error('Admin group name does not match provided id.');
      channel.adminGroup = adminGroup;
    }

    const validationErrors = await validate(channel);
    if (validationErrors.length > 0)
      throw new BadRequestError('Form validation failed: ' + prepareValidationErrorList(validationErrors));

    const createdChannel = await transactionManager.save(channel);

    createdChannel.shortUrl = await createShortURL(createdChannel.id);
    await transactionManager.save(createdChannel);

    await AuditChannels.setValue(createdChannel.id, {
      event: 'Create',
      user: this.authorizationBag.email,
      ip: this.authorizationBag.ip,
    });

    //TODO could go wrong if transaction manager fails. test this
    return new ChannelResponse(createdChannel);
  }
}
