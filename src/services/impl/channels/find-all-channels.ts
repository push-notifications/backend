import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { UnauthorizedError } from 'routing-controllers';
import { Category } from '../../../models/category';
import { ChannelsListResponse, GetChannelResponse, ChannelsQuery } from '../../../controllers/channels/dto';
import { SubmissionByForm } from '../../../models/channel-enums';

export class FindAllChannels implements Command {
  constructor(private query: ChannelsQuery, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<ChannelsListResponse> {
    if (!this.authorizationBag)
      throw new UnauthorizedError('Access unauthorized. Please authenticate or use /public/channels instead.');

    // Get current user groups, to be used next in SQL to compare with channel groups
    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    // Initial query with left joins but no select to avoid slow typeorm aggregation
    const qb = transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .leftJoin('channel.owner', 'owner')
      .leftJoin('channel.adminGroup', 'adminGroup');

    if (this.query.searchText) qb.andWhere(Channel.qbSearchText(this.query.searchText));

    if (this.query.category) {
      const categoryIds = await Category.getDescendantIds(transactionManager, this.query.category);
      qb.leftJoin('channel.category', 'category');
      qb.andWhere(Channel.qbMatchCategory(categoryIds));
    }

    if (this.query.tags) {
      qb.leftJoin('channel.tags', 'tags');
      qb.andWhere(Channel.qbMatchTags(this.query.tags));
    }

    qb.andWhere(
      Channel.filterChannels(
        userGroups,
        this.authorizationBag,
        this.query.ownerFilter,
        this.query.subscribedFilter,
        this.query.favoritesFilter,
      ),
    );

    // query count
    const count = await qb.clone().getCount();

    // Nothing found, no match, we return an empty list
    if (count === 0) {
      return new ChannelsListResponse();
    }

    // query channel_ids and filter, skip and take only what we need for this page
    qb.select(['channel.id', 'channel.name']) // select channel_id
      .orderBy('channel.name', 'ASC')
      .distinct()
      .limit(this.query.take || 10)
      .offset(this.query.skip || 0);

    const channel_ids = await qb.getRawMany();

    // select channels and relations for this list of channel_ids only
    const channels = await transactionManager
      .getRepository(Channel)
      .createQueryBuilder('channel')
      .leftJoinAndSelect('channel.unsubscribed', 'unsubscribed')
      .leftJoinAndSelect('channel.owner', 'owner')
      .leftJoinAndSelect('channel.adminGroup', 'adminGroup')
      .leftJoinAndSelect('channel.category', 'category')
      .leftJoinAndSelect('channel.tags', 'tags')
      .where('channel.id IN (:...ids)', {
        ids: channel_ids.map(c => c.channel_id),
      })
      .loadRelationCountAndMap('channel.notificationCount', 'channel.notifications')
      .orderBy('channel.name', 'ASC')
      .getMany();

    const returnChannels: GetChannelResponse[] = await Promise.all(
      channels.map(async channel => {
        const isSubscribed = await channel.isSubscribed(transactionManager, this.authorizationBag, userGroups);
        const hasAdminAccess = await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups);
        const canSendByForm =
          (channel.submissionByForm.includes(SubmissionByForm.members) && isSubscribed) ||
          (channel.submissionByForm.includes(SubmissionByForm.administrators) && hasAdminAccess);
        return new GetChannelResponse(
          channel,
          isSubscribed,
          hasAdminAccess || this.authorizationBag.isSupporter,
          canSendByForm,
        );
      }),
    );

    return new ChannelsListResponse(returnChannels, count);
  }
}
