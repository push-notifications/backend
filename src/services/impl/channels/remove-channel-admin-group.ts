import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ChannelResponse } from '../../../controllers/channels/dto';
import { AuditChannels } from '../../../log/auditing';

export class RemoveChannelAdminGroup implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to manage this channel.");

    channel.adminGroup = null;

    const updatedChannel = await transactionManager.save(channel);

    await AuditChannels.setValue(updatedChannel.id, {
      event: 'SetAdminGroup',
      user: this.authorizationBag.email,
      groupIdentifier: null,
      ip: this.authorizationBag.ip,
    });
  }
}
