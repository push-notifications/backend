import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { Category } from '../../../models/category';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { ChannelResponse } from '../../../controllers/channels/dto';

export class SetCategory implements Command {
  constructor(private channelId: string, private category: Category, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<ChannelResponse> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup', 'category'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to edit this channel.");

    if (this.category) {
      const category = await transactionManager.findOne(Category, {
        where: {
          id: this.category.id,
        },
      });
      if (!category) throw new NotFoundError('Category does not exist');
      channel.category = category;
    } else channel.category = null;

    await transactionManager.save(channel);
    await AuditChannels.setValue(channel.id, {
      event: 'SetCategory',
      user: this.authorizationBag.email,
      category: this.category,
      ip: this.authorizationBag.ip,
    });

    return new ChannelResponse(channel);
  }
}
