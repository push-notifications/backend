import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ServiceFactory } from '../../services-factory';
import { GroupsServiceInterface } from '../../groups-service';
import { AuditChannels } from '../../../log/auditing';
import { GroupResponse } from '../../../controllers/channels/dto';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';
import { Group } from '../../../models/group';

export class AddGroupToChannel implements Command {
  constructor(private groupName: string, private channelId: string, private authorizationBag: AuthorizationBag) {}
  groupsService: GroupsServiceInterface = ServiceFactory.getGroupService();

  async execute(transactionManager: EntityManager): Promise<GroupResponse> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);
    // you need to be channel admin
    if (!(await channel.hasAdminAccess(transactionManager, this.authorizationBag, userGroups)))
      throw new ForbiddenError("You don't have the rights to manage this channel.");

    const groupToAdd = await this.groupsService.GetOrCreateGroup(this.groupName);
    if (await channel.isGroupSubscribed(transactionManager, groupToAdd)) {
      throw new ForbiddenError(`Group ${this.groupName} is already a member.`);
    }

    // Throws error if there's an un-matched EGroup posting restriction
    await groupToAdd.validateEgroupPostingRestrictionsOrThrowError(
      this.authorizationBag.userName,
      userGroups,
      this.channelId,
    );

    await channel.addGroup(transactionManager, groupToAdd);
    await AuditChannels.setValue(channel.id, {
      event: 'AddGroup',
      user: this.authorizationBag.email,
      groupIdentifier: this.groupName,
      ip: this.authorizationBag.ip,
    });

    return new GroupResponse(groupToAdd);
  }
}
