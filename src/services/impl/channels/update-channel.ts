import { Command } from '../command';
import { EntityManager, Not } from 'typeorm';
import { Channel } from '../../../models/channel';
import { BadRequestError, ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { prepareValidationErrorList } from './validation-utils';
import { AuditChannels } from '../../../log/auditing';
import { UpdateChannelRequest, ChannelResponse } from '../../../controllers/channels/dto';
import { validate } from 'class-validator';
import { createShortURL } from './manage-short-url';

export class UpdateChannel implements Command {
  constructor(private channel: UpdateChannelRequest, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<ChannelResponse> {
    // whitelist: strip all properties that don't have any decorators
    await validate(this.channel, { whitelist: true }).then(errors => {
      if (errors.length > 0) throw new BadRequestError('Form validation failed: ' + prepareValidationErrorList(errors));
    });

    const channel = await transactionManager.findOne(Channel, {
      relations: ['adminGroup', 'groups', 'owner'],
      where: {
        id: this.channel.id,
      },
    });

    if (!channel.shortUrl) {
      channel.shortUrl = await createShortURL(this.channel.id);
    }

    validate(channel).then(errors => {
      console.log(errors);
    });

    if (!channel) throw new NotFoundError('The channel does not exist.');

    this.channel.name = this.channel.name.trim();

    if (this.channel.name.length < Channel.MIN_NAME_SIZE || this.channel.name.length > Channel.MAX_NAME_SIZE) {
      throw new BadRequestError("Channel's name length should be between 4 and 126 characters.");
    }
    if (this.channel.description.length > Channel.MAX_DESCRIPTION_SIZE) {
      throw new BadRequestError("Channel's description should be less than 256 characters.");
    }

    if (
      await transactionManager.findOne(Channel, {
        where: {
          id: Not(channel.id),
          name: this.channel.name,
        },
        withDeleted: true,
      })
    ) {
      throw new BadRequestError('Channel name already exists');
    }

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to edit this channel.");

    // TODO Loop should one day keep only updated properties
    // but it impacts the returned object. To link with DTOs
    for (const key in this.channel) {
      if (key != 'tags') channel[key] = this.channel[key];
    }
    /*
    if (this.channel.tags) {
      const tagCleaned = this.channel.tags.map(data => {
        const tagid = data.id || data;
        if (isUUID(tagid as string)) return tagid;
      });

      channel.tags = await transactionManager.find(Tag, {
        where: {
          id: In(tagCleaned),
        },
      });
    }*/

    if (!this.authorizationBag.isSupporter) channel.channelFlags = undefined;

    const validationErrors = await validate(channel);
    if (validationErrors.length > 0)
      throw new BadRequestError('Form validation failed: ' + prepareValidationErrorList(validationErrors));

    const updatedChannel = await transactionManager.save(channel);
    await AuditChannels.setValue(updatedChannel.id, {
      event: 'Update',
      user: this.authorizationBag.email,
      ip: this.authorizationBag.ip,
    });

    return new ChannelResponse(updatedChannel);
  }
}
