import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { deleteShortURL } from './manage-short-url';

export class DeleteChannel implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<void> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['members', 'groups', 'owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to manage this channel.");

    // Update name + slug before doing softDelete
    channel.name = `${channel.name}-DELETED-${Date.now()}`;
    channel.slug = `${channel.slug}-DELETED-${Date.now()}`;
    await transactionManager.save(channel);

    try {
      await deleteShortURL(channel.shortUrl);
    } catch (e) {
      console.error('Error deleting the short URL for', channel.name, 'channel:', e);
    }

    const result = await transactionManager.softDelete(Channel, channel.id);
    if (result.affected === 1) {
      await AuditChannels.setValue(channel.id, {
        event: 'Delete',
        user: this.authorizationBag.email,
        ip: this.authorizationBag.ip,
      });
    } else throw new ForbiddenError('The channel does not exist or you are not allowed to delete it.');
  }
}
