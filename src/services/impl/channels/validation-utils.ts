import { ValidationError } from "class-validator";

export function prepareValidationErrorList(validationErrors: ValidationError[]) {
    let err = '';
    validationErrors.forEach((validationError) => {
      if (validationError.constraints)
        for (var i in validationError.constraints)
          err += JSON.stringify(validationError.constraints[i]) + '\n';
      else
        err += validationError.property + '\n';
    });

    return err;
}
