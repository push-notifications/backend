import { Command } from '../command';
import { EntityManager, In } from 'typeorm';
import { Channel } from '../../../models/channel';
import { Tag } from '../../../models/tag';
import { BadRequestError, ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { AuditChannels } from '../../../log/auditing';
import { ChannelResponse, setTagsRequest } from '../../../controllers/channels/dto';
import { isUUID } from 'class-validator';

export class SetTags implements Command {
  constructor(private channelId: string, private tags: setTagsRequest, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<ChannelResponse> {
    if (!isUUID(this.channelId))
      throw new BadRequestError('Provided channel id: "' + this.channelId + '" is not a UUID.');

    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup', 'tags'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to edit this channel.");

    this.tags
      ? (channel.tags = await transactionManager.find(Tag, {
          where: { id: In(this.tags.tagIds) },
        }))
      : (channel.tags = null);

    await transactionManager.save(channel);
    await AuditChannels.setValue(channel.id, {
      event: 'AddTags',
      user: this.authorizationBag.email,
      tags: this.tags,
      ip: this.authorizationBag.ip,
    });

    return new ChannelResponse(channel);
  }
}
