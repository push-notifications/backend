import { deleteWithTimeout, getWithTimeout, postWithTimeout } from '../../../utils/fetch';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

function generateRandomSlug(): string {
  let slug = '';
  const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
  let counter = 0;
  while (counter < 3) {
    slug += characters.charAt(Math.floor(Math.random() * characters.length));
    counter += 1;
  }
  return slug;
}

export async function _createShortURL(channelId: string): Promise<string> {
  const shortURLToken = await CernAuthorizationService.getShortURLAuthToken();

  try {
    const shortURLs = await postWithTimeout(`${process.env.SHORT_URL_BASE_URL}`, {
      headers: shortURLToken,
      body: JSON.stringify({
        slug: `n-${generateRandomSlug()}`,
        targetUrl: `${process.env.BASE_WEBSITE_URL}/channels/${channelId}/notifications`,
        description: `Notifications: Short URL for channel ${channelId}`,
        appendQuery: true,
      }),
    });

    return `${new URL(shortURLs.url).host}/${shortURLs.slug}`;
  } catch (e) {
    if (e.message.includes('Entry already exists.')) {
      console.error(`Channel Short URL: The generated slug already exists.`);
    }
    return null;
  }
}

export async function createShortURL(channelId: string): Promise<string> {
  let url = await _createShortURL(channelId);
  if (url) {
    return url;
  }

  console.error(`Channel Short URL: Failed creating. Attempting again...`);
  url = await _createShortURL(channelId);
  if (url) {
    return url;
  }

  console.error(`Channel Short URL: Failed creating.`);
  return null;
}

export async function deleteShortURL(shortUrl: string): Promise<void> {
  const shortURLToken = await CernAuthorizationService.getShortURLAuthToken();
  const slug = shortUrl.split('/')[1];
  const shortUrls = await getWithTimeout(`${process.env.SHORT_URL_BASE_URL}?slug=${slug}`, {
    headers: shortURLToken,
  });
  const shortUrlId = shortUrls.find(url => {
    return url.slug === slug;
  })?.id;
  if (!shortUrlId || shortUrls.length == 0) {
    console.error(`Channel Short URL: Failed deleting. Could not find shortUrl: ${shortUrl}`);
  }

  await deleteWithTimeout(`${process.env.SHORT_URL_BASE_URL}/${shortUrlId}`, {
    headers: shortURLToken,
  });
}
