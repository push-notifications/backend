import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ChannelResponse, GetChannelPublicResponse, GetChannelResponse } from '../../../controllers/channels/dto';
import { Visibility } from '../../../models/channel-enums';

export class GetChannelByIdPublic implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<GetChannelPublicResponse> {
    const channel = await transactionManager.findOne(Channel, {
      // Specify needed joinColumns targets here, or use eager=true in model column def.
      relations: ['category', 'tags'],
      where: {
        id: this.channelId,
        visibility: Visibility.public,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    return new GetChannelPublicResponse(channel);
  }
}
