import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Channel } from '../../../models/channel';
import { Group } from '../../../models/group';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ChannelResponse, UpdateAdminGroupRequest } from '../../../controllers/channels/dto';
import { AuditChannels } from '../../../log/auditing';

export class UpdateChannelAdminGroup implements Command {
  constructor(
    private newAdminGroup: UpdateAdminGroupRequest,
    private channelId: string,
    private authorizationBag: AuthorizationBag,
  ) {}

  async execute(transactionManager: EntityManager): Promise<ChannelResponse> {
    const channel = await transactionManager.findOne(Channel, {
      relations: ['owner', 'adminGroup'],
      where: {
        id: this.channelId,
      },
    });

    if (!channel) throw new NotFoundError('Channel does not exist');

    // you need to be channel admin
    if (!(await channel.isAdmin(this.authorizationBag)))
      throw new ForbiddenError("You don't have the rights to manage this channel.");

    const newGroup = new Group({ groupIdentifier: this.newAdminGroup.newAdminGroup });

    let groupToAdd = await transactionManager.findOneBy(Group, {
      groupIdentifier: this.newAdminGroup.newAdminGroup,
    });
    if (!groupToAdd && (await newGroup.exists())) {
      groupToAdd = newGroup;
      groupToAdd = await transactionManager.save(groupToAdd);
    }
    if (!groupToAdd) throw new NotFoundError('The group does not exist');

    channel.adminGroup = groupToAdd;

    const updatedChannel = await transactionManager.save(channel);

    await AuditChannels.setValue(updatedChannel.id, {
      event: 'SetAdminGroup',
      user: this.authorizationBag.email,
      groupIdentifier: this.newAdminGroup.newAdminGroup,
      ip: this.authorizationBag.ip,
    });

    return new ChannelResponse(updatedChannel);
  }
}
