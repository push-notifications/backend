import { AbstractService } from "./abstract-service";
import { AppleServiceInterface } from "../apple-service";
import { DownloadPushPackage } from "./apple/download-pushpackage";
import { RegisterOrUpdateDevicePermissionPolicy } from "./apple/register-device-permission";
import { DeleteDevicePermissionPolicy } from "./apple/delete-device-permission";


export class AppleService
  extends AbstractService
  implements AppleServiceInterface {

  downloadPushPackage(userInfo): Promise<any> {
    return this.commandExecutor.execute(new DownloadPushPackage(userInfo));
  }

  registerOrUpdateDevicePermissionPolicy(authorization: string, deviceToken: string): Promise<boolean> {
    return this.commandExecutor.execute(new RegisterOrUpdateDevicePermissionPolicy(authorization, deviceToken));
  }

  deleteDevicePermissionPolicy(authorization: string, deviceToken: string): Promise<boolean> {
    return this.commandExecutor.execute(new DeleteDevicePermissionPolicy(authorization, deviceToken));
  }
}
