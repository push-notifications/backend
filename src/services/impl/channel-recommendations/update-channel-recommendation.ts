import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { AuthorizationBag } from '../../../models/authorization-bag';
import {
  ChannelRecommendationResponse,
  ChannelRecommendationAttributesRequest,
} from '../../../controllers/channel-recommendations/dto';
import { ForbiddenError, NotFoundError, UnauthorizedError } from 'routing-controllers';
import { ChannelRecommendation } from '../../../models/channel-recommendation';

export class UpdateChannelRecommendation implements Command {
  constructor(
    private channelRecommendationId: string,
    private channelRecommendationAttributes: ChannelRecommendationAttributesRequest,
    private authorizationBag: AuthorizationBag,
  ) {}

  async execute(transactionManager: EntityManager): Promise<ChannelRecommendationResponse> {
    if (!this.channelRecommendationAttributes.ignoredAt && !this.channelRecommendationAttributes.subscribedAt) {
      throw new ForbiddenError(
        `SubscribedAt or IgnoredAt attributes have to be dates (${this.channelRecommendationId})`,
      );
    }

    if (this.channelRecommendationAttributes.ignoredAt && this.channelRecommendationAttributes.subscribedAt) {
      throw new ForbiddenError(
        `It is not possible to change both the SubscribedAt and IgnoredAt attributes (${this.channelRecommendationId})`,
      );
    }

    const channelRecommendation = await transactionManager.findOne(ChannelRecommendation, {
      relations: ['user', 'channel'],
      where: {
        id: this.channelRecommendationId,
      },
    });

    if (!channelRecommendation) {
      throw new NotFoundError(`Channel Recommendation (${this.channelRecommendationId}) does not exist`);
    }

    if (channelRecommendation.user.id !== this.authorizationBag.userId) {
      throw new UnauthorizedError(
        `Channel Recommendation (${this.channelRecommendationId}) does not belong to current user`,
      );
    }

    if (channelRecommendation.ignoredAt || channelRecommendation.subscribedAt) {
      throw new ForbiddenError(`Channel Recommendation (${this.channelRecommendationId}) has already been updated.`);
    }

    channelRecommendation.ignoredAt = this.channelRecommendationAttributes.ignoredAt;
    channelRecommendation.subscribedAt = this.channelRecommendationAttributes.subscribedAt;

    const createdChannelRecommendation = await transactionManager.save(channelRecommendation);
    return new ChannelRecommendationResponse(createdChannelRecommendation);
  }
}
