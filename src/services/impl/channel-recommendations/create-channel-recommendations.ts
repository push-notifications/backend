import { Command } from '../command';
import { EntityManager } from 'typeorm';
import {
  ChannelRecommendationRequest,
  ChannelRecommendationResponse,
} from '../../../controllers/channel-recommendations/dto';
import { ChannelRecommendation } from '../../../models/channel-recommendation';
import { User } from '../../../models/user';
import { Channel } from '../../../models/channel';
import { NotFoundError, BadRequestError } from 'routing-controllers';

export class CreateChannelRecommendations implements Command {
  constructor(private channelRecommendation: ChannelRecommendationRequest) {}

  async execute(transactionManager: EntityManager): Promise<ChannelRecommendationResponse> {
    const channelRecommendation = new ChannelRecommendation(this.channelRecommendation);

    const existingChannelRecommendation = await transactionManager.findOne(ChannelRecommendation, {
      relations: ['user', 'channel'],
      where: {
        channel: {
          id: this.channelRecommendation.channelId,
        },
        user: {
          id: this.channelRecommendation.userId,
        },
        source: this.channelRecommendation.source,
      },
    });

    if (existingChannelRecommendation) {
      throw new BadRequestError('Channel Recommendation Already Exists');
    }

    const user = await transactionManager.findOneBy(User, {
      id: this.channelRecommendation.userId,
    });
    if (!user) {
      throw new NotFoundError(`User (${this.channelRecommendation.userId}) does not exist`);
    }
    channelRecommendation.user = user;

    const channel = await transactionManager.findOneBy(Channel, {
      id: this.channelRecommendation.channelId,
    });
    if (!channel) {
      throw new NotFoundError(`Channel (${this.channelRecommendation.channelId}) does not exist`);
    }
    channelRecommendation.channel = channel;

    const createdchannelRecommendation = await transactionManager.save(channelRecommendation);
    return new ChannelRecommendationResponse(createdchannelRecommendation);
  }
}
