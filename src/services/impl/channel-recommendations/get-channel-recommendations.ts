import { Command } from '../command';
import { EntityManager, Brackets } from 'typeorm';
import { AuthorizationBag } from '../../../models/authorization-bag';
import {
  ChannelRecommendationResponse,
  GetChannelRecommendationsResponse,
} from '../../../controllers/channel-recommendations/dto';
import { ChannelRecommendation } from '../../../models/channel-recommendation';
import { Channel } from '../../../models/channel';
import { CernAuthorizationService } from '../../../models/cern-authorization-service';

const DEFAULT_LIMIT = 5;

export class GetChannelRecommendations implements Command {
  constructor(private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager): Promise<GetChannelRecommendationsResponse> {
    const userId = this.authorizationBag.userId;

    const userGroups = await CernAuthorizationService.getCurrentUserGroups(this.authorizationBag.userName);

    const queryChannelRecommendations = transactionManager
      .getRepository(ChannelRecommendation)
      .createQueryBuilder('channelRecommendations')
      .innerJoinAndSelect('channelRecommendations.channel', 'channel')
      .leftJoin('channel.owner', 'owner')
      .leftJoin('channel.adminGroup', 'adminGroup')
      .innerJoinAndSelect('channelRecommendations.user', 'user')
      .where('user.id = :id', { id: userId })
      .andWhere('channel.deleteDate IS NULL')
      .andWhere('channelRecommendations.ignoredAt IS NULL')
      .andWhere('channelRecommendations.subscribedAt IS NULL');

    const qbUserSubscriptionFilter = new Brackets(qb => {
      qb.where(Channel.qbRestrictedNotSubscribedUserChannels(userId, userGroups)).orWhere(
        Channel.qbPublicInternalNotSubscribedUserChannels(userId, userGroups),
      );
    });

    queryChannelRecommendations
      .andWhere(qbUserSubscriptionFilter)
      .orderBy(Channel.name)
      .distinctOn([Channel.name])
      .limit(parseInt(process.env.MAX_RECOMMENDATIONS, 10) || DEFAULT_LIMIT);

    const channelRecommendations = await queryChannelRecommendations.getMany();

    return new GetChannelRecommendationsResponse(
      channelRecommendations.map(channelRecommendation => new ChannelRecommendationResponse(channelRecommendation)),
    );
  }
}
