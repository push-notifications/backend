import { AuthService } from '../auth-service';
import { Login } from './auth/login/Login';
import { RefreshTokens } from './auth/refreshtokens/RefreshTokens';
import { AbstractService } from './abstract-service';
import { User } from '../../models/user';
import { Device } from '../../models/device';
import { GetAuthenticatedUser } from './auth/get-authenticated-user/GetAuthenticatedUser';
import { CreateUserWithDefaults } from './auth/create-update-user/create-user-with-defaults';
import { UpdateUserEmail } from './auth/create-update-user/update-user-email';
import { UpdateUserUsername } from './auth/create-update-user/update-user-username';
import { UpdateUserLastLogin } from './auth/create-update-user/update-user-last-login';
import { UpsertPhoneDevice } from './auth/create-update-user/upsert-phone-device';
import { UpsertUser } from './auth/create-update-user/upsert-user';
import { UpsertMattermostDevice } from './auth/create-update-user/upsert-mattermost-device';

export class AuthServiceImpl extends AbstractService implements AuthService {
  authenticateUser(code: string): Promise<{
    randomString: string;
    access_token: string;
    refresh_token: string;
  }> {
    return this.commandExecutor.execute(new Login(code));
  }

  refreshTokens(refreshToken: string): Promise<{
    randomString: string;
    access_token: string;
    refresh_token: string;
  }> {
    return this.commandExecutor.execute(new RefreshTokens(refreshToken));
  }

  getAuthenticatedUser(username: string): Promise<User> {
    return this.commandExecutor.execute(new GetAuthenticatedUser(username));
  }

  createUserWithDefaults(user: User): Promise<User> {
    return this.commandExecutor.execute(new CreateUserWithDefaults(user));
  }

  updateUserEmail(user: User, email: string): Promise<User> {
    return this.commandExecutor.execute(new UpdateUserEmail(user, email));
  }

  updateUserUsername(user: User, username: string): Promise<User> {
    return this.commandExecutor.execute(new UpdateUserUsername(user, username));
  }

  updateUserLastLogin(user: User): Promise<User> {
    return this.commandExecutor.execute(new UpdateUserLastLogin(user));
  }

  upsertMattermostDevice(user: User): Promise<Device> {
    return this.commandExecutor.execute(new UpsertMattermostDevice(user));
  }

  upsertPhoneDevice(user: User): Promise<Device> {
    return this.commandExecutor.execute(new UpsertPhoneDevice(user));
  }

  upsertUser(upn: string, email: string): Promise<void> {
    return this.commandExecutor.execute(new UpsertUser(upn, email));
  }
}
