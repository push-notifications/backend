import { Command } from "../command";
import { EntityManager } from "typeorm";
import { Tag } from "../../../models/tag";
import { AuthorizationBag } from "../../../models/authorization-bag";

export class GetTags implements Command {
  constructor(
    private authorizationBag: AuthorizationBag
  ) { }

  async execute(transactionManager: EntityManager) {
    return {
      tags: await transactionManager.find(Tag, {
        order: {
          name: "ASC"
        }
      })
    };

  }
}
