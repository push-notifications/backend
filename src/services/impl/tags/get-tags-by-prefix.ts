import { Command } from "../command";
import { EntityManager, Like } from "typeorm";
import { Tag } from "../../../models/tag";
import { AuthorizationBag } from "../../../models/authorization-bag";

export class GetTagsByPrefix implements Command {
  constructor(
    private prefix: string,
    private authorizationBag: AuthorizationBag
  ) { }

  async execute(transactionManager: EntityManager) {
    return {
      tags: await transactionManager.find(Tag, {
        where: [
          {
            name: Like(`${this.prefix || ""}%`),
          },
        ],
        order: {
          name: "ASC"
        }
      })
    };
  }
}
