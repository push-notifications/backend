import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Tag } from '../../../models/tag';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { BadRequestError } from 'routing-controllers';

export class CreateTag implements Command {
  constructor(private tag: Tag, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    // Check tag length
    if (this.tag.name.length < 2 || this.tag.name.length > 32) {
      throw new BadRequestError('Tag should be between 2 and 32 characters.');
    }
    // Check if tag already exists
    if (
      await transactionManager.findOneBy(Tag, {
        name: this.tag.name,
      })
    ) {
      throw new BadRequestError('Tag already exists');
    }

    const tag = new Tag({ ...this.tag });

    const savedTag = await transactionManager.save(tag);
    if (savedTag) {
      return await transactionManager.findOneBy(Tag, {
        id: savedTag.id,
      });
    }
  }
}
