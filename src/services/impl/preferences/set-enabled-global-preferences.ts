import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Preference } from '../../../models/preference';
import { Channel } from '../../../models/channel';
import { User } from '../../../models/user';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ForbiddenError } from 'routing-controllers';

export class SetEnabledGlobalPreference implements Command {
  constructor(
    private authorizationBag: AuthorizationBag,
    private channelId: string,
    private preferenceId: string,
    private isEnabled: boolean,
  ) {}

  async execute(transactionManager: EntityManager) {
    const preference = await transactionManager.findOne(Preference, {
      where: {
        id: this.preferenceId,
        user: { id: this.authorizationBag.userId },
      },
      relations: ['target', 'disabledChannels', 'devices'],
    });

    const channel = await transactionManager.findOne(Channel, {
      where: { id: this.channelId },
    });

    if (preference.target)
      throw new ForbiddenError('Enable/disable preference is only allowed for global preferences.');

    if (!preference) throw new ForbiddenError('The preference does not exist or you are not the owner.');

    if (this.isEnabled) {
      preference.disabledChannels = preference.disabledChannels.filter(channel => channel.id !== this.channelId);
    } else if (!preference.disabledChannels.some(c => c.id === channel.id)) {
      preference.disabledChannels.push(channel);
    }

    return await transactionManager.save(preference);
  }
}
