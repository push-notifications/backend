import { Command } from "../command";
import { EntityManager } from "typeorm";
import { Preference } from "../../../models/preference";
import { AuthorizationBag } from "../../../models/authorization-bag";
import { ForbiddenError } from "routing-controllers";

export class DeleteUserPreference implements Command {
  constructor(private preferenceId: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    let result = await transactionManager.delete(Preference, {
      id: this.preferenceId,
      user: this.authorizationBag.userId,
    });

    if (result.affected === 1) return this.preferenceId;
    else
      throw new ForbiddenError("The preference does not exist or you are not the owner");
  }
}
