import { Command } from "../command";
import { EntityManager, Like } from "typeorm";
import { Preference } from "../../../models/preference";
import { Channel } from "../../../models/channel";
import { AuthorizationBag } from "../../../models/authorization-bag";
import { BadRequestError } from "routing-controllers";

export class CreateUserPreference implements Command {
  constructor(private preference: Preference, private authorizationBag: AuthorizationBag) { }

  async execute(transactionManager: EntityManager) {
    const preference = new Preference({ ...this.preference, user: this.authorizationBag.userId })
    let target = null;

    if (preference.target) {
      try {
        target = await transactionManager.findOne(Channel, {
          where: {
            id: preference.target,
          },
        });
      } catch (err) {
        throw new BadRequestError("Invalid Channel: channel's ID does not exist");
      }
    }

    preference.validatePreference();

    const savedPreference = await transactionManager.save(
      new Preference({ ...preference, target })
    );
    if (savedPreference) {
      return await transactionManager.findOne(Preference, {
        relations: ["target", "disabledChannels", "devices"],
        where: {
          id: savedPreference.id,
        },
      });
    }
  }
}
