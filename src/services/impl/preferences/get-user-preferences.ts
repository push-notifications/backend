import { Command } from '../command';
import { EntityManager, IsNull } from 'typeorm';
import { Preference } from '../../../models/preference';
import { AuthorizationBag } from '../../../models/authorization-bag';

export class GetUserPreferences implements Command {
  constructor(private channelId: string, private authorizationBag: AuthorizationBag) {}

  private async fetchGlobalPreferences(transactionManager: EntityManager) {
    return await transactionManager.find(Preference, {
      relations: {
        target: true,
        disabledChannels: true,
        devices: true,
      },
      where: {
        user: { id: this.authorizationBag.userId },
        target: IsNull(),
      },
    });
  }

  private async fetchChannelPreferences(transactionManager: EntityManager) {
    return await transactionManager
      .getRepository(Preference)
      .createQueryBuilder('preference')
      .leftJoin('preference.user', 'user')
      .leftJoinAndSelect('preference.target', 'target')
      .leftJoinAndSelect('preference.devices', 'device')
      .where('user.id = :userId AND target.id = :channelId', {
        userId: this.authorizationBag.userId,
        channelId: this.channelId,
      })
      .getMany();
  }

  async execute(transactionManager: EntityManager) {
    const result = {
      globalPreferences: await this.fetchGlobalPreferences(transactionManager),
    };

    if (this.channelId) {
      result['channelPreferences'] = await this.fetchChannelPreferences(transactionManager);
    }

    return result;
  }
}
