import { Command } from "../command";
import { EntityManager } from "typeorm";
import { BadRequestError } from "routing-controllers";
import { Preference } from "../../../models/preference";
import { AuthorizationBag } from "../../../models/authorization-bag";
import { Channel } from "../../../models/channel";

export class UpdateUserPreference implements Command {
  constructor(
    private preferenceId: string,
    private preference: Preference,
    private authorizationBag: AuthorizationBag
  ) { }

  async execute(transactionManager: EntityManager) {
    if (this.preference.id !== this.preferenceId)
      throw new BadRequestError("Invalid Preference update information");

    const preference = new Preference({
      ...this.preference,
      user: this.authorizationBag.userId
    });

    if (preference.target) {
      try {
        const target = await transactionManager.findOne(Channel, {
          where: {
            id: preference.target,
          },
        });
      } catch (err) {
        throw new BadRequestError("Invalid Channel: channel's ID does not exist");
      }
    }

    preference.validatePreference();

    // Save will also update existing objects
    const savedPreference = await transactionManager.save(preference);
    if (savedPreference) {
      return await transactionManager.findOne(Preference, {
        relations: ["target", "disabledChannels", "devices"],
        where: {
          id: savedPreference.id,
        },
      });
    }

  }
}
