import { AbstractService } from "./abstract-service";
import { ToggleChannel } from "./usersettings/toggle-channel-in-collection";
import { AuthorizationBag } from "../../models/authorization-bag";
import { UserSettings } from "../../models/user-settings";
import { UserChannelCollectionType } from "../../models/user-channel-collection";
import { UserChannelCollectionServiceInterface } from "../user-channel-collection-service";

export class UserChannelCollectionService
    extends AbstractService
    implements UserChannelCollectionServiceInterface {

    toggleChannel(
        channelId: string,
        collectionType: UserChannelCollectionType,
        authorizationBag: AuthorizationBag
    ): Promise<String> {
        return this.commandExecutor.execute(
            new ToggleChannel(channelId, collectionType, authorizationBag)
        );
    }
}