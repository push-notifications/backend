import { AbstractService } from './abstract-service';
import { GetChannelRecommendations } from './channel-recommendations/get-channel-recommendations';
import { CreateChannelRecommendations } from './channel-recommendations/create-channel-recommendations';
import { AuthorizationBag } from '../../models/authorization-bag';
import { ChannelRecommendationServiceInterface } from '../channel-recommendation-service';
import {
  ChannelRecommendationAttributesRequest,
  ChannelRecommendationRequest,
  ChannelRecommendationResponse,
  GetChannelRecommendationsResponse,
} from '../../controllers/channel-recommendations/dto';
import { UpdateChannelRecommendation } from './channel-recommendations/update-channel-recommendation';

export class ChannelRecommendationService extends AbstractService implements ChannelRecommendationServiceInterface {
  updateChannelRecommendation(
    channelRecommendationId: string,
    channelRecommendationAttributes: ChannelRecommendationAttributesRequest,
    authorizationBag: AuthorizationBag,
  ): Promise<ChannelRecommendationResponse> {
    return this.commandExecutor.execute(
      new UpdateChannelRecommendation(channelRecommendationId, channelRecommendationAttributes, authorizationBag),
    );
  }

  getChannelRecommendations(authorizationBag: AuthorizationBag): Promise<GetChannelRecommendationsResponse> {
    return this.commandExecutor.execute(new GetChannelRecommendations(authorizationBag));
  }

  createChannelRecommendation(
    channelRecommendation: ChannelRecommendationRequest,
  ): Promise<ChannelRecommendationResponse> {
    return this.commandExecutor.execute(new CreateChannelRecommendations(channelRecommendation));
  }
}
