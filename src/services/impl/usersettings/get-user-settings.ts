import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { UserSettings } from '../../../models/user-settings';
import { User } from '../../../models/user';
import { AuthorizationBag } from '../../../models/authorization-bag';

export class GetUserSettings implements Command {
  constructor(private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    let userSettings = await transactionManager.findOne(UserSettings, {
      where: {
        user: {
          id: this.authorizationBag.userId,
        },
      },
    });

    if (!userSettings) {
      const user = await transactionManager.findOneBy(User, { id: this.authorizationBag.userId });

      userSettings = new UserSettings({
        user: user,
        draft: '',
        favoriteList: [],
      });
      return await transactionManager.save(userSettings);
    }
    return userSettings;
  }
}
