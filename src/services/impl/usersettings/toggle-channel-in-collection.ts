import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ForbiddenError, NotFoundError } from 'routing-controllers';
import { UserChannelCollection, UserChannelCollectionType } from '../../../models/user-channel-collection';
import { User } from '../../../models/user';
import { Channel } from '../../../models/channel';

export class ToggleChannel implements Command {
  constructor(
    private channelId: string,
    private collectionType: UserChannelCollectionType,
    private authorizationBag: AuthorizationBag,
  ) {}

  async execute(transactionManager: EntityManager) {
    const channel = await transactionManager.findOne(Channel, {
      where: {
        id: this.channelId,
      },
    });
    if (!channel) {
      throw new NotFoundError('Channel does not exist');
    }

    const user = await transactionManager.findOne(User, {
      where: {
        id: this.authorizationBag.userId,
      },
    });
    if (!user) {
      throw new NotFoundError('User does not exist');
    }

    const collection = await transactionManager
      .getRepository(UserChannelCollection)
      .createQueryBuilder('collection')
      .leftJoin('collection.user', 'user')
      .leftJoinAndSelect('collection.channel', 'channel')
      .where('type = :colType', { colType: this.collectionType })
      .andWhere('channel.id = :chanId', { chanId: this.channelId })
      .andWhere('user.id = :userId', { userId: this.authorizationBag.userId })
      .getOne();

    if (!collection) {
      //toggle: insert
      const newCollection = new UserChannelCollection({
        channel: channel,
        userId: this.authorizationBag.userId,
        channelId: this.channelId,
        user: user,
        type: this.collectionType,
      });
      transactionManager.save(newCollection);
      return this.channelId;
    }

    //toggle: remove
    const deleteResult = await transactionManager.delete(UserChannelCollection, {
      channel: channel,
      user: user,
      type: this.collectionType,
    });

    if (deleteResult.affected !== 1) {
      throw new ForbiddenError('The preference does not exist or you are not the owner');
    }
    return this.channelId;
  }
}
