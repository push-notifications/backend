import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { UserSettings } from '../../../models/user-settings';
import { AuthorizationBag } from '../../../models/authorization-bag';

export class GetDraft implements Command {
  constructor(private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    const userSetting = await transactionManager.findOne(UserSettings, {
      where: {
        user: { id: this.authorizationBag.userId },
      },
    });

    if (userSetting) {
      return userSetting.draft;
    }
  }
}
