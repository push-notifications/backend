import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { UserSettings } from '../../../models/user-settings';
import { AuthorizationBag } from '../../../models/authorization-bag';

export class SaveDraft implements Command {
  constructor(private draft: string, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    let userSetting = await transactionManager.findOne(UserSettings, {
      where: {
        user: { id: this.authorizationBag.userId },
      },
    });

    if (!userSetting) {
      userSetting = new UserSettings({
        user: this.authorizationBag.userId,
      });
    }

    userSetting.draft = this.draft;
    await transactionManager.save(userSetting);
  }
}
