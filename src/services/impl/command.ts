import { EntityManager } from 'typeorm';

export interface Command {
  execute(transactionManager: EntityManager): Promise<any>;
}
