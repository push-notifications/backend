import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { NotFoundError } from 'routing-controllers';
import { Group } from '../../../models/group';

export class GetOrCreateGroup implements Command {
  constructor(private groupIdentifier: string) {}

  async execute(transactionManager: EntityManager): Promise<Group> {
    let group = await transactionManager.findOneBy(Group, {
      groupIdentifier: this.groupIdentifier,
    });
    if (group) return group;

    group = new Group({ groupIdentifier: this.groupIdentifier });
    if (await group.exists()) {
      group = await transactionManager.save(group);
      return group;
    }

    throw new NotFoundError('The group does not exist');
  }
}
