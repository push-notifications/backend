import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Device, DeviceStatus } from '../../../models/device';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ForbiddenError } from 'routing-controllers';
import { DeviceResponse, DeviceValuesRequest } from '../../../controllers/devices/dto';

export class UpdateUserDevice implements Command {
  constructor(
    private deviceId: string,
    private authorizationBag: AuthorizationBag,
    private newDeviceValues: DeviceValuesRequest,
  ) {}

  async execute(transactionManager: EntityManager): Promise<DeviceResponse> {
    console.log(this.newDeviceValues);
    const result = await transactionManager.update(
      Device,
      {
        id: this.deviceId,
        user: this.authorizationBag.userId,
      },
      {
        ...(this.newDeviceValues.name && { name: this.newDeviceValues.name }),
        ...(this.newDeviceValues.info && { info: this.newDeviceValues.info }),
        ...(this.newDeviceValues.token && { token: this.newDeviceValues.token }),
        ...(this.newDeviceValues.token && { status: DeviceStatus.OK }), // Token is updated, reset status.
      },
    );
    if (result.affected !== 1) throw new ForbiddenError('The device does not exist or you are not the owner');

    const response = await transactionManager.findOneBy(Device, { id: this.deviceId });

    return new DeviceResponse(response);
  }
}
