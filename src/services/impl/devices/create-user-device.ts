import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Device } from '../../../models/device';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { DeviceRequest, DeviceResponse } from '../../../controllers/devices/dto';

export class CreateUserDevice implements Command {
  constructor(private device: DeviceRequest, private authorizationBag: AuthorizationBag) {}

  async execute(transactionManager: EntityManager) {
    //TODO check if device isnt illegal (dont allow users to add their own numbers for MAIL2SMS)
    const result = await transactionManager.save(new Device({ ...this.device, user: this.authorizationBag.userId }));

    return new DeviceResponse(result);
  }
}
