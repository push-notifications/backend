import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { DefaultDevices, Device } from '../../../models/device';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { Preference } from '../../../models/preference';
import { ForbiddenError, NotFoundError } from 'routing-controllers';

export class DeleteUserDevice implements Command {
  constructor(private deviceId: string, private authorizationBag: AuthorizationBag) {}

  private async deviceHasPreferencesAssociated(transactionManager: EntityManager) {
    const firstPreference = await transactionManager.findOneBy(Preference, {
      user: { id: this.authorizationBag.userId },
      devices: { id: this.deviceId },
    });

    return !!firstPreference;
  }

  async execute(transactionManager: EntityManager) {
    const deviceToDelete = await transactionManager.findOne(Device, {
      relations: ['user'],
      where: {
        id: this.deviceId,
        user: {
          id: this.authorizationBag.userId,
        },
      },
    });

    if (!deviceToDelete) {
      throw new NotFoundError('Provided device id does not match any device.');
    }
    if (DefaultDevices.includes(deviceToDelete.type)) {
      throw new ForbiddenError('This device cannot be deleted.');
    }
    if (await this.deviceHasPreferencesAssociated(transactionManager)) {
      throw new ForbiddenError(
        'The provided device has associated preferences. Please change the devices associated with these preferences or remove the preferences first to proceed.',
      );
    } else {
      const deleteResult = await transactionManager.delete(Device, {
        id: this.deviceId,
        user: this.authorizationBag.userId,
      });
      if (!deleteResult.affected) {
        throw new NotFoundError('Unable to delete device.');
      }
    }
  }
}
