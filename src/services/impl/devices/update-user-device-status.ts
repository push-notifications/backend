import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Device, DeviceStatus } from '../../../models/device';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { ForbiddenError } from 'routing-controllers';
import { DeviceResponse, DeviceValuesRequest } from '../../../controllers/devices/dto';

export class UpdateUserDeviceStatus implements Command {
  constructor(private deviceId: string, private authorizationBag: AuthorizationBag, private status: string) {}

  async execute(transactionManager: EntityManager): Promise<DeviceResponse> {
    if (!Object.keys(DeviceStatus).includes(this.status))
      throw new ForbiddenError('This device status value is not allowed');

    const result = await transactionManager.update(
      Device,
      {
        id: this.deviceId,
      },
      {
        status: DeviceStatus[this.status],
      },
    );

    if (result.affected !== 1) throw new ForbiddenError('The device does not exist or you are not allowed');

    const response = await transactionManager.findOneBy(Device, { id: this.deviceId });

    return new DeviceResponse(response);
  }
}
