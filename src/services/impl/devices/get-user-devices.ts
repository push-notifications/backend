import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Device } from '../../../models/device';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { DeviceResponse, GetDevicesResponse } from '../../../controllers/devices/dto';

export class GetUserDevices implements Command {
  constructor(private authorizationBag: AuthorizationBag) {}

  private async fetchUserDevices(transactionManager: EntityManager) {
    return await transactionManager
      .getRepository(Device)
      .createQueryBuilder('device')
      .leftJoin('device.user', 'user')
      .where('user.id = :userid', {
        userid: this.authorizationBag.userId,
      })
      .getMany();
  }

  async execute(transactionManager: EntityManager): Promise<GetDevicesResponse> {
    const devices = await this.fetchUserDevices(transactionManager);
    return new GetDevicesResponse(devices.map(device => new DeviceResponse(device)));
  }
}
