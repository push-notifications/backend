import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { Device, DeviceStatus, DeviceSubType, DeviceType } from '../../../models/device';
import { testBrowserPush } from '../../../push-browser/push-browser-sender';
import { testSafariPush } from '../../../push-browser/push-safari-sender';
import { testMattermost } from '../../../push-browser/push-mattermost-sender';
import { AuthorizationBag } from '../../../models/authorization-bag';
import { BadRequestError, ForbiddenError } from 'routing-controllers';
import { DevicesServiceInterface } from '../../devices-service';
import { ServiceFactory } from '../../services-factory';

export class TestUserDevice implements Command {
  constructor(private deviceId: string, private authorizationBag: AuthorizationBag) {}
  devicesService: DevicesServiceInterface = ServiceFactory.getDevicesService();

  async execute(transactionManager: EntityManager): Promise<void> {
    const selectedDevice = await transactionManager
      .getRepository(Device)
      .createQueryBuilder('device')
      .leftJoin('device.user', 'user')
      .where('user.id = :userid AND device.id = :deviceId', {
        userid: this.authorizationBag.userId,
        deviceId: this.deviceId,
      })
      .getOne(); // should be only one

    if (!selectedDevice || !selectedDevice.token)
      throw new ForbiddenError('The device does not exist, you are not the owner or no endpoint is registered.');

    console.debug('Sending test notification to device');
    // Temporary sending test push directly from backend
    // until a dedicated service is built, or maybe it will stay in backend, TBD.
    if (selectedDevice.type === DeviceType.BROWSER && selectedDevice.subType === DeviceSubType.OTHER) {
      const testRet = await testBrowserPush(selectedDevice.token);
      console.debug('Device testBrowserPush returned', testRet);

      if (testRet === 'EXPIRED' && selectedDevice.status !== 'EXPIRED') {
        // Device is expired, update the device status accordingly
        console.debug('Device is expired, updating DB');
        this.devicesService.updateUserDeviceStatusById(selectedDevice.id, null, DeviceStatus.EXPIRED);
      }
      if (testRet === 'OK' && selectedDevice.status === 'EXPIRED') {
        // Device is not expired anymore, update the device status accordingly
        console.debug('Device is not expired anymore, updating DB');
        this.devicesService.updateUserDeviceStatusById(selectedDevice.id, null, DeviceStatus.OK);
      }
      return;
    }

    if (selectedDevice.type === DeviceType.BROWSER && selectedDevice.subType === DeviceSubType.SAFARI) {
      testSafariPush(selectedDevice.token);
      return;
    }

    if (selectedDevice.type === DeviceType.APP && selectedDevice.subType === DeviceSubType.WINDOWS) {
      await testBrowserPush(selectedDevice.token, 'aesgcm'); // encryption aesgcm only for current windows app
      return;
    }

    if (selectedDevice.type === DeviceType.APP && selectedDevice.subType === DeviceSubType.MATTERMOST) {
      await testMattermost(selectedDevice.token);
      return;
    }

    throw new BadRequestError('The device type is not supported for TestMe.');
  }
}
