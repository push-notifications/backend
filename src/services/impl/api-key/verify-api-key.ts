import { Command } from '../command';
import { EntityManager } from 'typeorm';
import { EntityTarget } from 'typeorm/common/EntityTarget';
import { ApiKeyObject } from '../../../models/api-key-object';

export class VerifyApiKey implements Command {
  constructor(private id: string, private key: string, private entity: EntityTarget<ApiKeyObject>) {}

  async execute(transactionManager: EntityManager): Promise<boolean> {
    const obj = await transactionManager.findOneBy(this.entity, { id: this.id });

    if (!obj) return false;

    return obj.verifyKey(this.id, this.key);
  }
}
