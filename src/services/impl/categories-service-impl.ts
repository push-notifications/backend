import { AbstractService } from "./abstract-service";
import { CategoriesServiceInterface } from "../categories-service";
import { Category } from "../../models/category";
import { CreateCategory } from "./categories/create-category";
import { GetCategories } from "./categories/get-categories";
import { AuthorizationBag } from "../../models/authorization-bag";

export class CategoriesService
  extends AbstractService
  implements CategoriesServiceInterface {
  createCategory(
    category: Category,
    authorizationBag: AuthorizationBag
  ): Promise<Category> {
    return this.commandExecutor.execute(
      new CreateCategory(category, authorizationBag)
    );
  }

  getCategories(
    authorizationBag: AuthorizationBag,
  ): Promise<Category[]> {
    return this.commandExecutor.execute(
      new GetCategories(authorizationBag)
    );
  }
}
