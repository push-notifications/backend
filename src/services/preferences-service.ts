import { AuthorizationBag } from "../models/authorization-bag";
import { Preference } from "../models/preference";

export interface PreferencesServiceInterface {
  createUserPreference(
    preference: Preference, 
    authorizationBag: AuthorizationBag
  ): Promise<Preference>;

  getUserPreferences(
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<Preference[]>;

  deleteUserPreferenceById(
    preferenceId: string,
    authorizationBag: AuthorizationBag
  ): Promise<Preference>;

  setEnabledGlobalPreference(
    authorizationBag: AuthorizationBag,
    channelId: string,
    preferenceId: string,
    isEnabled: boolean
  ): Promise<Preference>;

  updateUserPreference(
    preferenceId: string,
    preference: Preference, 
    authorizationBag: AuthorizationBag
  ): Promise<Preference>;

}
