import { AuthorizationBag } from '../models/authorization-bag';
import {
  GetNotificationResponse,
  NotificationTargetUsersResponse,
  NotificationTargetGroupsResponse,
  SendNotificationRequest,
} from '../controllers/notifications/dto';
import { NotificationsListResponse, Query } from '../controllers/channels/dto';

export interface NotificationsService {
  sendNotification(
    notification: SendNotificationRequest,
    authorizationBag: AuthorizationBag,
  ): Promise<GetNotificationResponse>;

  retryNotification(notificationId: string, authorizationBag: AuthorizationBag): Promise<void>;

  findAllNotifications(
    channelId: string,
    query: Query,
    authorizationBag: AuthorizationBag,
  ): Promise<NotificationsListResponse>;

  getNotificationTargetGroups(
    notificationId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<NotificationTargetGroupsResponse>;

  getNotificationTargetUsers(
    notificationId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<NotificationTargetUsersResponse>;

  getById(notificationId: string, authorizationBag: AuthorizationBag): Promise<GetNotificationResponse>;

  getNotificationAuditById(notificationId: string, authorizationBag: AuthorizationBag): Promise<JSON>;

  deleteNotificationById(notificationId: string, authorizationBag: AuthorizationBag): void;
}
