import { User } from '../models/user';

export interface UsersServiceInterface {
  GetOrCreateUserFromUsernameOrEmail(user: string): Promise<User>;

  GetUserFromExternal(user: string): Promise<User>;
}
