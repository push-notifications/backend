import { AuthServiceImpl } from './impl/auth-service-impl';
import { AuthService } from './auth-service';
import { ChannelsService } from './channels-service';
import { ChannelsServiceImpl } from './impl/channels-service-impl';
import { NotificationsService } from './notifications-service';
import { NotificationsServiceImpl } from './impl/notifications-service-impl';
import { PreferencesServiceInterface } from './preferences-service';
import { PreferencesService } from './impl/preferences-service-impl';
import { PingServiceImpl } from './impl/ping-service-impl';
import { PingService } from './ping-service';
import { DevicesServiceInterface } from './devices-service';
import { DevicesService } from './impl/devices-service-impl';
import { AppleService } from './impl/apple-service-impl';
import { AppleServiceInterface } from './apple-service';
import { ServiceStatusServiceInterface } from './service-status-service';
import { ServiceStatusService } from './impl/service-status-service-impl';
import { MutesServiceInterface } from './mutes-service';
import { MutesService } from './impl/mutes-service-impl';
import { CategoriesServiceInterface } from './categories-service';
import { CategoriesService } from './impl/categories-service-impl';
import { TagsServiceInterface } from './tags-service';
import { TagsService } from './impl/tags-service-impl';
import { UserSettingsServiceInterface } from './usersettings-service';
import { UserSettingsService } from './impl/usersettings-service-impl';
import { StatisticsServiceInterface } from './statistics-service';
import { StatisticsService } from './impl/statistics-service-impl';
import { UserChannelCollectionServiceInterface } from './user-channel-collection-service';
import { UserChannelCollectionService } from './impl/user-channel-collection-service-impl';
import { UsersServiceImpl } from './impl/users-service-impl';
import { UsersServiceInterface } from './users-service';
import { GroupsServiceInterface } from './groups-service';
import { GroupsServiceImpl } from './impl/groups-service-impl';
import { AuditServiceInterface } from './audit-service';
import { AuditService } from './impl/audit-service-impl';
import { ChannelRecommendationServiceInterface } from './channel-recommendation-service';
import { ChannelRecommendationService } from './impl/channel-recommendation-service-impl';

export class ServiceFactory {
  static getNotificationsService(): NotificationsService {
    return new NotificationsServiceImpl();
  }

  static getAuthenticationService(): AuthService {
    return new AuthServiceImpl();
  }

  static getChannelsService(): ChannelsService {
    return new ChannelsServiceImpl();
  }

  static getPreferencesService(): PreferencesServiceInterface {
    return new PreferencesService();
  }

  static getMutesService(): MutesServiceInterface {
    return new MutesService();
  }

  static getPingService(): PingService {
    return new PingServiceImpl();
  }

  static getDevicesService(): DevicesServiceInterface {
    return new DevicesService();
  }

  static getAppleService(): AppleServiceInterface {
    return new AppleService();
  }

  static getServiceStatusService(): ServiceStatusServiceInterface {
    return new ServiceStatusService();
  }

  static getCategoriesService(): CategoriesServiceInterface {
    return new CategoriesService();
  }

  static getTagsService(): TagsServiceInterface {
    return new TagsService();
  }

  static getUserSettingsService(): UserSettingsServiceInterface {
    return new UserSettingsService();
  }

  static getUserChannelCollectionService(): UserChannelCollectionServiceInterface {
    return new UserChannelCollectionService();
  }

  static getStatisticsService(): StatisticsServiceInterface {
    return new StatisticsService();
  }

  static getUserService(): UsersServiceInterface {
    return new UsersServiceImpl();
  }

  static getGroupService(): GroupsServiceInterface {
    return new GroupsServiceImpl();
  }

  static getAuditService(): AuditServiceInterface {
    return new AuditService();
  }

  static getChannelRecommendationService(): ChannelRecommendationServiceInterface {
    return new ChannelRecommendationService();
  }
}
