import { AuthorizationBag } from "../models/authorization-bag";
import { Category } from "../models/category";

export interface CategoriesServiceInterface {
  createCategory(
    category: Category, 
    authorizationBag: AuthorizationBag
  ): Promise<Category>;

  getCategories(
    authorizationBag: AuthorizationBag,
  ): Promise<Category[]>;
}
