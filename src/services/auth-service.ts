import { User } from '../models/user';
import { Device } from '../models/device';

export interface AuthService {
  authenticateUser(code: string): Promise<{
    randomString: string;
    access_token: string;
    refresh_token: string;
  }>;

  refreshTokens(refreshToken: string): Promise<{
    randomString: string;
    access_token: string;
    refresh_token: string;
  }>;

  getAuthenticatedUser(username: string): Promise<User>;

  createUserWithDefaults(user: User): Promise<User>;

  updateUserEmail(user: User, email: string): Promise<User>;

  updateUserUsername(user: User, username: string): Promise<User>;

  updateUserLastLogin(user: User): Promise<User>;

  upsertMattermostDevice(user: User): Promise<Device>;

  upsertPhoneDevice(user: User): Promise<Device>;

  upsertUser(upn: string, email: string): Promise<void>;
}
