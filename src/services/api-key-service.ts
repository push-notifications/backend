import { AuthorizationBag } from '../models/authorization-bag';

export interface ApiKeyService {
  generateApiKey(id: string, authorizationBag: AuthorizationBag): Promise<string>;

  verifyAPIKey(id: string, key: string): Promise<boolean>;
}
