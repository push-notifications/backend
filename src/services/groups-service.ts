import { Group } from '../models/group';

export interface GroupsServiceInterface {
  GetOrCreateGroup(group: string): Promise<Group>;
}
