import { Statistics } from "../models/statistics";

export interface StatisticsServiceInterface {
  getStatistics(): Promise<Statistics>;
}
