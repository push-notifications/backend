import { Category } from '../models/category';
import { AuthorizationBag } from '../models/authorization-bag';
import { ApiKeyService } from './api-key-service';
import {
  ChannelResponse,
  ChannelsListResponse,
  CreateChannelRequest,
  UpdateChannelRequest,
  ChannelsQuery,
  Query,
  MembersListResponse,
  GroupsListResponse,
  GroupResponse,
  GetChannelResponse,
  EditChannelResponse,
  setTagsRequest,
  ChannelStatsResponse,
  MemberResponse,
  PublicChannelsListResponse,
  GetChannelPublicResponse,
  UpdateAdminGroupRequest,
} from '../controllers/channels/dto';

export interface ChannelsService extends ApiKeyService {
  getAllChannels(query: ChannelsQuery, authorizationBag: AuthorizationBag): Promise<ChannelsListResponse>;

  getAllPublicChannels(query): Promise<PublicChannelsListResponse>; //public controller

  getChannelByIdPublic(channelId: string, authorizationBag: AuthorizationBag): Promise<GetChannelPublicResponse>;

  getChannelById(
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<GetChannelResponse | GetChannelPublicResponse>;

  editChannelById(channelId: string, authorizationBag: AuthorizationBag): Promise<EditChannelResponse>;

  createChannel(channel: CreateChannelRequest, authorizationBag: AuthorizationBag): Promise<ChannelResponse>;

  deleteChannel(channelId: string, authorizationBag: AuthorizationBag): void;

  updateChannel(channel: UpdateChannelRequest, authorizationBag: AuthorizationBag): Promise<ChannelResponse>;

  getChannelMembers(channelId: string, query: Query, authorizationBag: AuthorizationBag): Promise<MembersListResponse>;

  getChannelGroups(channelId: string, query: Query, authorizationBag: AuthorizationBag): Promise<GroupsListResponse>;

  addGroupToChannel(groupName: string, channelId: string, authorizationBag: AuthorizationBag): Promise<GroupResponse>;

  removeGroupFromChannel(groupId: string, channelId: string, authorizationBag: AuthorizationBag): Promise<void>;

  updateChannelAdminGroup(
    newAdminGroup: UpdateAdminGroupRequest,
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<ChannelResponse>;

  removeChannelAdminGroup(channelId: string, authorizationBag: AuthorizationBag): Promise<void>;

  addMemberToChannel(
    membername: string,
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<MemberResponse>;

  removeMemberFromChannel(memberId: string, channelId: string, authorizationBag: AuthorizationBag): Promise<void>;

  subscribeToChannel(channelId: string, authorizationBag: AuthorizationBag): Promise<void>;

  unsubscribeFromChannel(channelId: string, authorizationBag: AuthorizationBag): Promise<void>;

  generateApiKey(channelId: string, authorizationBag: AuthorizationBag): Promise<string>;

  setCategory(channelId: string, category: Category, authorizationBag: AuthorizationBag): Promise<ChannelResponse>;

  setTags(channelId: string, tags: setTagsRequest, authorizationBag: AuthorizationBag): Promise<ChannelResponse>;

  setChannelOwner(username: string, channelId: string, authorizationBag: AuthorizationBag): Promise<ChannelResponse>;

  getChannelStats(channelId: string, authorizationBag: AuthorizationBag): Promise<ChannelStatsResponse>;
}
