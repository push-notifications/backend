import { AuthorizationBag } from '../models/authorization-bag';
import {
  ChannelRecommendationAttributesRequest,
  ChannelRecommendationRequest,
  ChannelRecommendationResponse,
  GetChannelRecommendationsResponse,
} from '../controllers/channel-recommendations/dto';

export interface ChannelRecommendationServiceInterface {
  getChannelRecommendations(authorizationBag: AuthorizationBag): Promise<GetChannelRecommendationsResponse>;

  createChannelRecommendation(
    channelRecommendation: ChannelRecommendationRequest,
  ): Promise<ChannelRecommendationResponse>;

  updateChannelRecommendation(
    channelRecommendationId: string,
    channelRecommendationUpdate: ChannelRecommendationAttributesRequest,
    authorizationBag: AuthorizationBag,
  ): Promise<ChannelRecommendationResponse>;
}
