export interface AppleServiceInterface {

  downloadPushPackage(userInfo): Promise<any>;

  registerOrUpdateDevicePermissionPolicy(authorization: string, deviceToken: string): Promise<boolean>;

  deleteDevicePermissionPolicy(authorization: string, deviceToken: string): Promise<boolean>;
}
