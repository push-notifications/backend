import { AuthorizationBag } from '../models/authorization-bag';

export interface AuditServiceInterface {
  getAuditNotifications(id: string, authorizationBag: AuthorizationBag): Promise<any>;
  getAuditChannels(id: string, authorizationBag: AuthorizationBag): Promise<any>;
  getAuditIPAdress(IPAddress: string, authorizationBag: AuthorizationBag): Promise<any>;
}
