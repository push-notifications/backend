import { AuthorizationBag } from "../models/authorization-bag";
import { UserChannelCollectionType } from "../models/user-channel-collection";
import { UserSettings } from "../models/user-settings";

export interface UserChannelCollectionServiceInterface {

toggleChannel(
    channelId: string,
    collectionType: UserChannelCollectionType,
    authorizationBag: AuthorizationBag
  ): Promise<String>;
}