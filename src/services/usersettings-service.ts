import { AuthorizationBag } from "../models/authorization-bag";
import { UserSettings } from "../models/user-settings";

export interface UserSettingsServiceInterface {

  getUserSettings(
    authorizationBag: AuthorizationBag
  ): Promise<UserSettings>;

  saveDraft(
    draft: string, 
    authorizationBag: AuthorizationBag
  ): Promise<string>;

  getDraft(
    authorizationBag: AuthorizationBag,
  ): Promise<string>;

  deleteDraft(
    authorizationBag: AuthorizationBag,
  ): Promise<boolean>;
}
