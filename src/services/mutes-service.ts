import { AuthorizationBag } from "../models/authorization-bag";
import { Mute } from "../models/mute";

export interface MutesServiceInterface {
  createUserMute(
    mute: Mute, 
    authorizationBag: AuthorizationBag
  ): Promise<Mute>;

  getUserMutes(
    channelId: string,
    authorizationBag: AuthorizationBag,
  ): Promise<Mute[]>;

  deleteUserMuteById(
    muteId: string,
    authorizationBag: AuthorizationBag
  ): Promise<Mute>;

  updateUserMute(
    muteId: string,
    mute: Mute, 
    authorizationBag: AuthorizationBag
  ): Promise<Mute>;

  createUnSubscribe(
    mute: Mute,
    blob: string,
    email: string,
  ): Promise<Mute>;
}
