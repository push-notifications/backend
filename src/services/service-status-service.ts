import {
  ServiceStatusResponse,
  ServiceStatusMessage,
  ServiceStatusListResponse,
} from '../controllers/service-status/dto';
import { Query } from '../controllers/channels/dto';

export interface ServiceStatusServiceInterface {
  getServiceStatus(): Promise<ServiceStatusResponse>;
  getAllServiceStatus(query: Query): Promise<ServiceStatusListResponse>;
  setServiceStatus(message: string, validity?: number): Promise<void>;
  updateServiceStatus(serviceStatusMessage: ServiceStatusMessage): Promise<void>;
  deleteServiceStatusById(serviceStatusId: string): Promise<void>;
}
