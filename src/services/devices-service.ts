import { DeviceRequest, DeviceResponse, DeviceValuesRequest, GetDevicesResponse } from '../controllers/devices/dto';
import { AuthorizationBag } from '../models/authorization-bag';
import { DeviceStatus } from '../models/device';

export interface DevicesServiceInterface {
  createUserDevice(device: DeviceRequest, authorizationBag: AuthorizationBag): Promise<DeviceResponse>;

  getUserDevices(authorizationBag: AuthorizationBag): Promise<GetDevicesResponse>;

  deleteUserDeviceById(deviceId: string, authorizationBag: AuthorizationBag): Promise<void>;

  tryBrowserPushNotification(deviceId: string, authorizationBag: AuthorizationBag): Promise<void>;

  updateUserDeviceById(
    deviceId: string,
    authorizationBag: AuthorizationBag,
    newDeviceValues: DeviceValuesRequest,
  ): Promise<DeviceResponse>;

  updateUserDeviceStatusById(
    deviceId: string,
    authorizationBag: AuthorizationBag,
    status: DeviceStatus,
  ): Promise<DeviceResponse>;
}
