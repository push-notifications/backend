import {
  Controller,
  Req,
  Delete,
  Post,
  Param,
  BodyParam,
  Body,
  HeaderParam,
  UseBefore,
  ContentType,
} from 'routing-controllers';
import { json } from 'body-parser';
import { ServiceFactory } from '../services/services-factory';
import { AppleServiceInterface } from '../services/apple-service';

@Controller('/apple/safari')
export class AppleController {
  appleService: AppleServiceInterface = ServiceFactory.getAppleService();

  // Safari Specifics endpoints
  // https://developer.apple.com/library/archive/documentation/NetworkingInternet/Conceptual/NotificationProgrammingGuideForWebsites/PushNotifications/PushNotifications.html#//apple_ref/doc/uid/TP40013225-CH3-SW9

  // Downloading Your Website Package
  @Post('/:version/pushPackages/:websitePushID')
  @ContentType('application/zip')
  @UseBefore(json()) // Needed for the json body to be handled properly, as we can't use JSONController.
  async postDownloadPushPackage(
    @Req() req,
    @Param('version') version: string,
    @Param('websitePushID') websitePushID: string,
    @BodyParam('anonid') anonid: string, // from the json passed at subscription
  ) {
    console.debug('DownloadPushPackage version %s, websitePushID %s, anonid %s', version, websitePushID, anonid);

    if (websitePushID !== process.env.APPLE_SAFARI_PUSH_WEBSITEPUSHID) throw new Error('Invalid websitePushID');

    // Build and return the pushPackage.zip for this user
    return this.appleService.downloadPushPackage(anonid);
  }

  // Registering or Updating Device Permission Policy
  @Post('/:version/devices/:deviceToken/registrations/:websitePushID')
  @ContentType('application/json')
  async postRegisterOrUpdateDevicePermissionPolicy(
    @Req() req,
    @Param('version') version: string,
    @Param('deviceToken') deviceToken: string,
    @Param('websitePushID') websitePushID: string,
    @HeaderParam('authorization') authorization: string,
  ) {
    console.debug(
      'RegisterOrUpdateDevicePermissionPolicy version %s, websitePushID %s, deviceToken %s, authorization %s',
      version,
      websitePushID,
      deviceToken,
      authorization,
    );

    if (websitePushID !== process.env.APPLE_SAFARI_PUSH_WEBSITEPUSHID) throw new Error('Invalid websitePushID');

    // Get back token from the header, formatted ApplePushNotifications token
    return this.appleService.registerOrUpdateDevicePermissionPolicy(authorization, deviceToken);
  }

  // Forgetting Device Permission Policy
  @Delete('/:version/devices/:deviceToken/registrations/:websitePushID')
  @ContentType('application/json')
  async deleteDevicePermissionPolicy(
    @Req() req,
    @Param('version') version: string,
    @Param('deviceToken') deviceToken: string,
    @Param('websitePushID') websitePushID: string,
    @HeaderParam('authorization') authorization: string,
  ) {
    console.debug(
      'deleteDevicePermissionPolicy version %s, websitePushID %s, deviceToken %s, authorization %s',
      version,
      websitePushID,
      deviceToken,
      authorization,
    );

    if (websitePushID !== process.env.APPLE_SAFARI_PUSH_WEBSITEPUSHID) throw new Error('Invalid websitePushID');

    // Get back token from the header, formatted ApplePushNotifications token
    return this.appleService.deleteDevicePermissionPolicy(authorization, deviceToken);
  }

  // Logging Errors
  @Post('/:version/log')
  @ContentType('application/json')
  async postLog(
    @Req() req,
    @Param('version') version: string,
    @Body() log: string, // Contains the log
  ) {
    console.debug('Logging Errors version %s', version);
    console.debug('Safari registration Error: ' + JSON.stringify(log));
    return true;
  }
}
