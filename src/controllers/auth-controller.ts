import { JsonController, Post, BodyParam, HeaderParam, HttpCode } from 'routing-controllers';
import { ServiceFactory } from '../services/services-factory';
import { AuthService } from '../services/auth-service';

@JsonController()
export class AuthController {
  authService: AuthService = ServiceFactory.getAuthenticationService();

  @HttpCode(201)
  @Post('/login')
  async login(@BodyParam('code') code: string): Promise<{ accessToken: string; refreshToken: string }> {
    const tokens = await this.authService.authenticateUser(code);
    const accessToken = tokens.access_token;
    const refreshToken = tokens.refresh_token;

    return { accessToken, refreshToken };
  }

  @HttpCode(201)
  @Post('/refresh')
  async refreshTokens(
    @HeaderParam('authorization') authorization: string,
  ): Promise<{ accessToken: string; refreshToken: string }> {
    const token = authorization.replace('Bearer ', '');
    const tokens = await this.authService.refreshTokens(token);
    const accessToken = tokens.access_token;
    const refreshToken = tokens.refresh_token;

    return { accessToken, refreshToken };
  }
}
