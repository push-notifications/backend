import { IsString, IsNumber, IsDateString, IsUUID, IsOptional, ValidateNested } from 'class-validator';
import { v4 } from 'uuid';
import { JSONSchema } from 'class-validator-jsonschema';
import { Type } from 'class-transformer';
import { Notification } from '../../models/notification';

export class ServiceStatusListResponse {
  @ValidateNested({ each: true })
  @Type(() => ServiceStatusResponse)
  @JSONSchema({
    description: 'List of service status message.',
    example: [{}],
  })
  items: ServiceStatusResponse[];

  @JSONSchema({
    description: 'Number of message returned.',
    example: 2,
  })
  @IsNumber()
  count: number;

  constructor(list: ServiceStatusResponse[] = [], count = 0) {
    this.items = list;
    this.count = count;
  }
}

export class Query {
  @JSONSchema({
    description: 'For pagination, number of items to skip in result. Default: 10.',
  })
  @IsOptional()
  @IsNumber()
  take: number;

  @JSONSchema({
    description: 'For pagination, number of items to skip in result. Default: 0.',
  })
  @IsOptional()
  @IsNumber()
  skip: number;
}

export class ServiceStatusResponse {
  @IsString()
  @IsUUID('4')
  @JSONSchema({
    description: 'Service Status Message id.',
    example: v4(),
  })
  id: string;

  @IsString()
  @JSONSchema({
    description: 'Service Status Message text.',
    example: 'We are experiencing some problems',
  })
  message: string;

  @IsDateString()
  @JSONSchema({
    description: 'The date the Service Status Message was created.',
    example: new Date(),
  })
  created: Date;

  @IsNumber()
  @JSONSchema({
    description: 'Number of days that the Service Status Message is valid',
    example: '3',
  })
  validity: number;
}

export class ServiceStatusMessage {
  @IsString()
  @IsUUID('4')
  @JSONSchema({
    description: 'Service Status Message id.',
    example: v4(),
  })
  id: string;

  @IsString()
  @IsOptional()
  @JSONSchema({
    description: 'Service Status Message text.',
    example: 'We are experiencing some problems',
  })
  message: string;

  @IsNumber()
  @IsOptional()
  @JSONSchema({
    description: 'Number of days that the Service Status Message is valid',
    example: '3',
  })
  validity: number;
}
