import {
  JsonController,
  Get,
  Post,
  Put,
  Delete,
  BodyParam,
  Param,
  UnauthorizedError,
  Authorized,
  OnUndefined,
  QueryParam,
  QueryParams,
} from 'routing-controllers';
import { ServiceStatusResponse, ServiceStatusMessage, ServiceStatusListResponse } from './dto';
import { ServiceFactory } from '../../services/services-factory';
import { ServiceStatusServiceInterface } from '../../services/service-status-service';
import { ResponseSchema } from 'routing-controllers-openapi';
import { Query } from '../channels/dto';

@JsonController('/servicestatus')
export class ServiceStatusController {
  serviceStatusService: ServiceStatusServiceInterface = ServiceFactory.getServiceStatusService();

  @Get()
  async getServiceStatus(): Promise<ServiceStatusResponse> {
    return this.serviceStatusService.getServiceStatus();
  }

  @Authorized([process.env.SUPPORTER_ROLE])
  @Get('/all')
  @ResponseSchema(ServiceStatusListResponse, { description: 'List of service status message and their count.' })
  async getAllServiceStatus(
    @QueryParam('pagination') pagination: number,
    @QueryParams() query: Query,
  ): Promise<ServiceStatusListResponse> {
    if (process.env.EXPOSE_UNAUTHENTICATED_ROUTES) {
      return this.serviceStatusService.getAllServiceStatus(query);
    }
    throw new Error('Unauthenticated route is not enabled');
  }

  @Authorized([process.env.SUPPORTER_ROLE])
  @Post()
  @OnUndefined(204)
  async setServiceStatus(
    @BodyParam('message') message: string,
    @BodyParam('validity') validity: number,
  ): Promise<void> {
    if (process.env.EXPOSE_UNAUTHENTICATED_ROUTES) {
      await this.serviceStatusService.setServiceStatus(message, validity);
    } else {
      throw new Error('Unauthenticated route is not enabled');
    }
  }

  @Authorized([process.env.SUPPORTER_ROLE])
  @Put()
  @OnUndefined(204)
  async updateServiceStatus(
    @BodyParam('serviceStatusMessage') serviceStatusMessage: ServiceStatusMessage,
  ): Promise<void> {
    if (process.env.EXPOSE_UNAUTHENTICATED_ROUTES) {
      await this.serviceStatusService.updateServiceStatus(serviceStatusMessage);
    } else {
      throw new Error('Unauthenticated route is not enabled');
    }
  }

  @Authorized([process.env.SUPPORTER_ROLE])
  @Delete('/:serviceStatusId')
  @OnUndefined(204)
  async deleteServiceStatusById(@Param('serviceStatusId') serviceStatusId: string): Promise<void> {
    if (process.env.EXPOSE_UNAUTHENTICATED_ROUTES) {
      await this.serviceStatusService.deleteServiceStatusById(serviceStatusId);
    } else {
      throw new UnauthorizedError('Unauthenticated route is not enabled');
    }
  }
}
