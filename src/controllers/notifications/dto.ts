import {
  IsBoolean,
  IsDateString,
  IsEmail,
  IsEnum,
  IsNotEmpty,
  IsOptional,
  IsString,
  IsUUID,
  MaxDate,
  MinDate,
  ValidateNested,
  isUUID,
} from 'class-validator';
import { Type } from 'class-transformer';
import { JSONSchema } from 'class-validator-jsonschema';
import { Notification } from '../../models/notification';
import { PriorityLevel, Source } from '../../models/notification-enums';
import { v4 } from 'uuid';
import { Group } from '../../models/group';
import { User } from '../../models/user';

const swaggerChannelId = process.env.SWAGGER_CHANNEL_ID;
@JSONSchema({
  description: 'Notification json return',
  example: {
    id: v4(),
    body: '<p>My body</p>',
    description: 'Notification description.',
    summary: 'My notification',
    target: swaggerChannelId,
    sendAt: new Date(),
    sentAt: new Date(),
    priority: PriorityLevel.NORMAL,
    link: 'https://notifications.web.cern.ch/',
    imgUrl: 'https://home.cern/sites/default/files/logo/cern-logo.png',
    source: Source.api,
    private: false,
    intersection: false,
  },
})
export class GetNotificationResponse {
  @IsString()
  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'Notification id of the returned notification.',
    example: v4(),
  })
  id: string;

  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Body of the returned notification.',
    example: '<p>My body</p>',
  })
  body: string;

  @IsDateString()
  @MinDate(new Date())
  @JSONSchema({
    description: 'Date the returned notification is to be sent at.',
    example: new Date(),
  })
  sendAt: Date;

  @IsDateString()
  @MaxDate(new Date())
  @JSONSchema({
    description: 'Date the returned notification was sent.',
    example: new Date(),
  })
  sentAt: Date;

  @IsString()
  @JSONSchema({
    description: 'Specify a target url to redirect when clicked.',
    example: 'https://notifications.web.cern.ch/',
  })
  link: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Summary of the returned notification.',
    example: 'My notification',
  })
  summary: string;

  @IsString()
  @JSONSchema({
    description: 'Specify an image url to display as preview when possible, eg. push notifications',
    example: 'https://home.cern/sites/default/files/logo/cern-logo.png',
  })
  imgUrl: string;

  @IsEnum(Source)
  @JSONSchema({
    description: 'Internal use only. Will be ignored.',
    example: Source.api,
    default: Source.api,
  })
  source: Source;

  @IsBoolean()
  @JSONSchema({
    description: 'Describes if the notification has targets.',
    example: false,
    default: false,
  })
  private: boolean;

  @IsBoolean()
  @JSONSchema({
    description: 'Describes if the notification targets are a result of intersection.',
    example: false,
    default: false,
  })
  intersection: boolean;

  constructor(notification: Notification) {
    this.intersection = notification.intersection;
    this.private = notification.private;
    this.intersection = notification.intersection;
    this.id = notification.id;
    this.body = notification.body;
    this.sendAt = notification.sendAt;
    this.sentAt = notification.sentAt;
    this.link = notification.link;
    this.summary = notification.summary;
    this.imgUrl = notification.imgUrl;
    this.source = notification.source;
  }
}

@JSONSchema({
  description: 'New notification json input',
  example: {
    body: '<p>My body</p>',
    summary: 'My notification',
    target: swaggerChannelId,
    priority: PriorityLevel.NORMAL,
    link: 'https://notifications.web.cern.ch/',
    imgUrl: 'https://home.cern/sites/default/files/logo/cern-logo.png',
  },
})
export class SendNotificationRequest {
  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Body of the notification to be sent. Supports raw text and HTML.',
    example: '<p>My body</p>',
  })
  body: string;

  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel ID of the channel to where the notification is to be sent.',
    example: v4(),
  })
  target: string;

  @JSONSchema({
    description:
      'Supports mixed strings emails and group names (but degraded performance therefore not the ' +
      'recommended option). Requires private set to "true".',
    example: ['user@cern.ch', 'my-group'],
  })
  @IsString({ each: true })
  @IsOptional()
  targetData: string[];

  @IsString()
  @IsOptional()
  @JSONSchema({ description: 'Internal use only. Will be ignored.' })
  sender: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Summary of the notification to be sent.',
    example: 'My notification',
  })
  summary: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => SendNotificationRequestGroup)
  @JSONSchema({
    description: 'Groups of targeted users. Requires private set to "true".',
    example: [{ groupIdentifier: 'user-group' }, { groupIdentifier: 'another-group' }],
  })
  targetGroups: SendNotificationRequestGroup[];

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => SendNotificationRequestUser)
  @JSONSchema({
    description: 'Emails or logins of targeted users. Requires private set to "true".',
    example: [{ email: 'username' }, { email: 'user@cern.ch' }],
  })
  targetUsers: SendNotificationRequestUser[];

  @IsString()
  @IsOptional()
  @JSONSchema({
    description: 'Specify an image url to display as preview when possible, eg. push notifications',
    example: 'https://home.cern/sites/default/files/logo/cern-logo.png',
  })
  imgUrl: string;

  @IsNotEmpty()
  @IsEnum(PriorityLevel)
  @JSONSchema({
    description: 'Priority with which the notification is sent.',
    example: PriorityLevel.NORMAL,
    default: PriorityLevel.NORMAL,
  })
  priority: PriorityLevel;

  @IsOptional()
  @IsEnum(Source)
  @JSONSchema({
    description: 'Internal use only. Will be ignored.',
    example: Source.api,
    default: Source.api,
  })
  source: Source;

  @IsString()
  @IsOptional()
  @JSONSchema({
    description: 'Specify a target url to redirect when clicked.',
    example: 'https://notifications.web.cern.ch/',
  })
  link: string;

  @IsBoolean()
  @IsOptional()
  @JSONSchema({
    description: 'Enable to send targeted notifications.',
    example: false,
    default: false,
  })
  private: boolean;

  @IsBoolean()
  @IsOptional()
  @JSONSchema({
    description: 'Enable to send only to intersection between channel members/groups and targeted group.',
    example: false,
    default: false,
  })
  intersection: boolean;

  @IsOptional()
  @IsDateString()
  @JSONSchema({
    description: 'Send notification at this date.',
    example: new Date(),
    default: null,
  })
  sendAt: Date;
}

@JSONSchema({
  description: 'Json object with target users.',
  example: {
    email: 'username',
  },
})
class SendNotificationRequestUser {
  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Users to whom the notification is to be sent.',
    example: '"email@cern.ch" or "username"',
  })
  email: string;
}

@JSONSchema({
  description: 'Json object with target groups.',
  example: {
    groupIdentifier: 'user-group',
  },
})
class SendNotificationRequestGroup {
  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Groups to which the notification is to be sent.',
    example: 'user-group',
  })
  groupIdentifier: string;
}

class TargetGroup {
  @IsUUID('4')
  id: string;

  @IsString()
  groupIdentifier: string;

  constructor(group: Group) {
    this.id = group.id;
    this.groupIdentifier = group.groupIdentifier;
  }
}

class TargetUser {
  @IsUUID('4')
  id: string;

  @IsOptional()
  @IsEmail()
  email: string;

  username: string;

  constructor(user: User) {
    this.id = user.id;
    this.email = user.email;
    this.username = user.username;
  }
}

export class NotificationTargetUsersResponse {
  items: TargetUser[];
  total: number;

  constructor(count: number, targetUserList: User[]) {
    this.items = targetUserList.map(user => new TargetUser(user));
    this.total = count;
  }
}

export class NotificationTargetGroupsResponse {
  items: TargetGroup[];
  total: number;

  constructor(count: number, targetGroupList: Group[]) {
    this.items = targetGroupList.map(group => new TargetGroup(group));
    this.total = count;
  }
}
