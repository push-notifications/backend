import {
  Get,
  Post,
  JsonController,
  Authorized,
  Req,
  Param,
  Put,
  OnUndefined,
  ForbiddenError,
  Body,
  Delete,
} from 'routing-controllers';
import { ServiceFactory } from '../../services/services-factory';
import { NotificationsService } from '../../services/notifications-service';
import {
  GetNotificationResponse,
  NotificationTargetUsersResponse,
  NotificationTargetGroupsResponse,
  SendNotificationRequest,
} from './dto';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { API_KEY_ACCESS_ROLE, PUBLIC_ACCESS } from '../../middleware/authorizationChecker';
import { StatusCodeDescriptions, StatusCodes } from '../../utils/status-codes';
import { JSONSchema } from 'class-validator-jsonschema';

@JsonController('/notifications')
export class NotificationsController {
  notificationsService: NotificationsService = ServiceFactory.getNotificationsService();

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE, PUBLIC_ACCESS])
  @OpenAPI({
    summary: 'Returns the Notification with the provided notification id.',
    description: 'This endpoint returns the notification with the provided notification id, if requester has access.',
    operationId: 'getNotification',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(GetNotificationResponse, { description: 'Notification requested.' })
  @Get('/:id')
  getById(@Req() req, @Param('id') notificationId: string): Promise<GetNotificationResponse> {
    return this.notificationsService.getById(notificationId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Returns a list of target groups of the Notification with the provided notification id (limited to 200)',
    description:
      'This endpoint returns a list of target groups of the notification with the provided notification id, if requester has access.',
    operationId: 'getNotificationTargetGroups',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(NotificationTargetGroupsResponse, { description: 'Notification target groups list requested.' })
  @Get('/:id/targets/groups')
  getNotificationTargetGroups(
    @Req() req,
    @Param('id') notificationId: string,
  ): Promise<NotificationTargetGroupsResponse> {
    return this.notificationsService.getNotificationTargetGroups(notificationId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Returns a list of target users of the Notification with the provided notification id (limited to 200)',
    description:
      'This endpoint returns a list of target users of the notification with the provided notification id, if requester has access.',
    operationId: 'getNotificationTargetUsers',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(NotificationTargetUsersResponse, { description: 'Notification target users list requested.' })
  @Get('/:id/targets/users')
  getNotificationTargetUsers(
    @Req() req,
    @Param('id') notificationId: string,
  ): Promise<NotificationTargetUsersResponse> {
    return this.notificationsService.getNotificationTargetUsers(notificationId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.SUPPORTER_ROLE])
  @OpenAPI({
    summary: 'Returns the audit of the Notification with the provided notification id.',
    description:
      'This endpoint returns the audit of the notification with the provided notification id, if requester has access.',
    operationId: 'getNotificationAuditById',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(JSONSchema, { description: 'Audit Notification requested.' })
  @Get('/:id/audit')
  getNotificationAuditById(@Req() req, @Param('id') notificationId: string): Promise<JSON> {
    return this.notificationsService.getNotificationAuditById(notificationId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE, API_KEY_ACCESS_ROLE])
  @OpenAPI({
    summary: 'Send a Notification.',
    description: 'Sends the Notification to the Channel with the channel id provided.',
    operationId: 'sendNotification',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(GetNotificationResponse, { description: 'Notification sent.' })
  @Post()
  sendNotification(@Body() notification: SendNotificationRequest, @Req() req): Promise<GetNotificationResponse> {
    return this.notificationsService.sendNotification(notification, req.authorizationBag);
  }

  @OnUndefined(201)
  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Retries to send Notification with provided notification id. For internal use.',
    description: 'Retries to send Notification with provided notification id.',
    operationId: 'retryNotification',
    //security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @Put('/:id/retry')
  retryNotification(@Param('id') notificationId: string, @Req() req): Promise<void> {
    if (!process.env.EXPOSE_UNAUTHENTICATED_ROUTES || process.env.EXPOSE_UNAUTHENTICATED_ROUTES === 'False') {
      throw new ForbiddenError('Unauthenticated route is not enabled');
    }
    return this.notificationsService.retryNotification(notificationId, null);
  }

  // TODO: Fix calls from mail_gateway consumer by adding a bearer token or restricting on
  @OpenAPI({
    summary: 'Sends Notification without the need for Auth. Internal use only.',
    description: 'Sends a Notification without the need of Auth.',
    operationId: 'sentNotificationUnauthenticated',
    //security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(GetNotificationResponse, { description: 'Notification sent.' })
  @Post('/unauthenticated')
  sendNotificationWithoutAuth(@Body() notification: SendNotificationRequest): Promise<GetNotificationResponse> {
    if (!process.env.EXPOSE_UNAUTHENTICATED_ROUTES || process.env.EXPOSE_UNAUTHENTICATED_ROUTES === 'False') {
      throw new ForbiddenError('Unauthenticated route is not enabled');
    }
    return this.notificationsService.sendNotification(notification, null);
  }
  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Deletes a scheduled notification.',
    description: 'Deletes the scheduled notification with the provided channel id.',
    operationId: 'deleteNotificationById',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
    },
  })
  @Delete('/:id')
  @OnUndefined(204)
  async deleteNotificationById(@Param('id') notificationId: string, @Req() req): Promise<void> {
    await this.notificationsService.deleteNotificationById(notificationId, req.authorizationBag);
  }
}
