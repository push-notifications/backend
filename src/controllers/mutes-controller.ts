import {
  JsonController,
  BodyParam,
  Authorized,
  Req,
  Post,
  Get,
  Put,
  Param,
  Delete,
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { MutesServiceInterface } from "../services/mutes-service";
import { Mute } from "../models/mute";

@JsonController("/mutes")
export class MutesController {
  mutesService: MutesServiceInterface = ServiceFactory.getMutesService();

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Post()
  createUserMute(
    @Req() req,
    @BodyParam("mute") mute: Mute
  ) {
    return this.mutesService.createUserMute(
      mute, req.authorizationBag
    );
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get()
  getGlobalUserMutes(@Req() req) {
    return this.mutesService.getUserMutes(undefined, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get("/:channelId")
  getUserMutesByChannel(
    @Req() req,
    @Param("channelId") channelId: string
  ) {
    return this.mutesService.getUserMutes(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Delete("/:muteId")
  deleteUserMuteById(
    @Req() req,
    @Param("muteId") muteId: string
  ) {
    return this.mutesService.deleteUserMuteById(
      muteId,
      req.authorizationBag
    );
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Put("/:muteId")
  updateUserMute(
    @Req() req,
    @Param("muteId") muteId: string,
    @BodyParam("mute") mute: Mute
  ) {
    return this.mutesService.updateUserMute(
      muteId, mute, req.authorizationBag
    );
  }

}
