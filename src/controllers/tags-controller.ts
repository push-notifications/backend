import {
  JsonController,
  BodyParam,
  Authorized,
  Req,
  Post,
  Get,
  Param,
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { TagsServiceInterface } from "../services/tags-service";
import { Tag } from "../models/tag";

@JsonController("/tags")
export class TagsController {
  tagsService: TagsServiceInterface = ServiceFactory.getTagsService();

  @Authorized([process.env.INTERNAL_ROLE])
  @Post()
  createTag(
    @Req() req,
    @BodyParam("tag") tag: Tag
  ) {
    return this.tagsService.createTag(tag, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get()
  getTags(
    @Req() req,
  ) {
    return this.tagsService.getTags(req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get("/:prefix")
  getTagsByPrefix(
    @Req() req,
    @Param("prefix") prefix: string
  ) {
    return this.tagsService.getTagsByPrefix(prefix, req.authorizationBag);
  }

}
