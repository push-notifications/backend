import {
  JsonController,
  BodyParam,
  Authorized,
  Req,
  Post,
  Get,
  Delete, OnUndefined, Param,
} from "routing-controllers";
import { UserChannelCollection, UserChannelCollectionType } from "../models/user-channel-collection";
import { ServiceFactory } from "../services/services-factory";
import { UserChannelCollectionServiceInterface } from "../services/user-channel-collection-service";
import { UserSettingsServiceInterface } from "../services/usersettings-service";

@JsonController("/usersettings")
export class UserSettigsController {
  userSettingsService: UserSettingsServiceInterface = ServiceFactory.getUserSettingsService();
  userChannelCollectionService: UserChannelCollectionServiceInterface = ServiceFactory.getUserChannelCollectionService();

  @Get()
  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OnUndefined(204)
  getUserSettings(
    @Req() req,
  ) {
    return this.userSettingsService.getUserSettings(
      req.authorizationBag
    );
  }

  @Post('/favorites/toggle/:channelid')
  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  toggleChannelInCollection(
    @Req() req,
    @Param("channelid") channelId: string
  ) {
    return this.userChannelCollectionService.toggleChannel(
      channelId, UserChannelCollectionType.FAVORITE, req.authorizationBag
    )
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Post('/draft')
  @OnUndefined(204)
  saveDraft(
    @Req() req,
    @BodyParam("draft") draft: string
  ) {
    return this.userSettingsService.saveDraft(
      draft, req.authorizationBag
    );
  }

  @Get('/draft')
  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OnUndefined(204)
  getDraft(@Req() req) {
    return this.userSettingsService.getDraft(req.authorizationBag);
  }

  @Delete('/draft')
  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OnUndefined(204)
  deleteDraft(@Req() req) {
    return this.userSettingsService.deleteDraft(req.authorizationBag);
  }

}
