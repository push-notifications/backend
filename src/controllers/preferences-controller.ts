import {
  JsonController,
  BodyParam,
  Authorized,
  Req,
  Post,
  Get,
  Param,
  Delete,
  Put,
  QueryParam,
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { PreferencesServiceInterface } from "../services/preferences-service";
import { Preference } from "../models/preference";

@JsonController("/preferences")
export class PreferencesController {
  preferencesService: PreferencesServiceInterface = ServiceFactory.getPreferencesService();

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Post()
  createUserPreference(
    @Req() req,
    @BodyParam("preference") preference: Preference
  ) {
    return this.preferencesService.createUserPreference(
      preference, req.authorizationBag
    );
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get()
  getGlobalUserPreferences(@Req() req) {
    return this.preferencesService.getUserPreferences(undefined, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get("/:channelId")
  getUserPreferencesByChannel(
    @Req() req,
    @Param("channelId") channelId: string
  ) {
    return this.preferencesService.getUserPreferences(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Delete("/:preferenceId")
  deleteUserPreferenceById(
    @Req() req,
    @Param("preferenceId") preferenceId: string
  ) {
    return this.preferencesService.deleteUserPreferenceById(
      preferenceId,
      req.authorizationBag
    );
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Post("/:preferenceId/disabled-channels/:channelId")
  setEnabledGlobalPreference(
    @Req() req,
    @Param("channelId") channelId: string,
    @Param("preferenceId") preferenceId: string,
    @QueryParam("isEnabled") isEnabled: true
  ) {
    return this.preferencesService.setEnabledGlobalPreference(
      req.authorizationBag,
      channelId,
      preferenceId,
      isEnabled
    );
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Put("/:preferenceId")
  updateUserPreference(
    @Req() req,
    @Param("preferenceId") preferenceId: string,
    @BodyParam("preference") preference: Preference
  ) {
    return this.preferencesService.updateUserPreference(
      preferenceId, preference, req.authorizationBag
    );
  }

}
