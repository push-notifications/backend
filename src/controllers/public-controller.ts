import { JsonController, Get, Post, BodyParam, Req, QueryParams, Param, Authorized } from 'routing-controllers';
import { ChannelsService } from '../services/channels-service';
import { ServiceFactory } from '../services/services-factory';
import { NotificationsService } from '../services/notifications-service';
import { MutesServiceInterface } from '../services/mutes-service';
import { Mute } from '../models/mute';
import { PUBLIC_ACCESS } from '../middleware/authorizationChecker';

@JsonController('/public')
export class PublicController {
  channelsService: ChannelsService = ServiceFactory.getChannelsService();
  notificationsService: NotificationsService = ServiceFactory.getNotificationsService();
  mutesService: MutesServiceInterface = ServiceFactory.getMutesService();

  @Get('/channels')
  async getAllChannels(@Req() req, @QueryParams() query) {
    const getChannelsQuery = {
      ...query,
      owner: query.ownerFilter == 'true' ? req.username : undefined,
    };

    Object.keys(getChannelsQuery).forEach(key => getChannelsQuery[key] === undefined && delete getChannelsQuery[key]);

    return this.channelsService.getAllPublicChannels(getChannelsQuery);
  }

  @Get('/channels/:id')
  async getChannelById(@Param('id') channelId: string, @Req() req) {
    return this.channelsService.getChannelById(channelId, null);
  }

  @Authorized([PUBLIC_ACCESS])
  @Get('/channels/:id/notifications')
  async findAllNotifications(@Req() req, @Param('id') channelId: string, @QueryParams() query) {
    Object.keys(query).forEach(key => query[key] === undefined && delete query[key]);

    return this.notificationsService.findAllNotifications(channelId, query, req.authorizationBag);
  }

  @Post('/unsubscribe/:blob/:email')
  unSubscribe(@Req() req, @BodyParam('mute') mute: Mute, @Param('blob') blob: string, @Param('email') email: string) {
    return this.mutesService.createUnSubscribe(mute, blob, email);
  }
}
