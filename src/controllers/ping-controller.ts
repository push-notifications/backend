import { JsonController, Req, Get } from 'routing-controllers';
import { ServiceFactory } from '../services/services-factory';
import { PingService } from '../services/ping-service';

@JsonController()
export class PingController {
  pingService: PingService = ServiceFactory.getPingService();

  @Get('/ping')
  async getAuthenticatedUser(@Req() request): Promise<string> {
    return this.pingService.ping();
  }
}
