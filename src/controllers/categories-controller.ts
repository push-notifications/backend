import { JsonController, BodyParam, Authorized, Req, Post, Get, ForbiddenError } from 'routing-controllers';
import { ServiceFactory } from '../services/services-factory';
import { CategoriesServiceInterface } from '../services/categories-service';

@JsonController('/categories')
export class TagsController {
  categoriesService: CategoriesServiceInterface = ServiceFactory.getCategoriesService();

  @Authorized([process.env.SUPPORTER_ROLE])
  @Post()
  createCategory(@Req() req, @BodyParam('category') category) {
    return this.categoriesService.createCategory(category, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @Get()
  getCategories(@Req() req) {
    return this.categoriesService.getCategories(req.authorizationBag);
  }

  @Post('/unauthenticated')
  createCategoryWithoutAuth(@Req() req, @BodyParam('category') category) {
    if (!process.env.EXPOSE_UNAUTHENTICATED_ROUTES || process.env.EXPOSE_UNAUTHENTICATED_ROUTES === 'False') {
      throw new ForbiddenError('Unauthenticated route is not enabled');
    }
    return this.categoriesService.createCategory(category, req.authorizationBag);
  }

  @Get('/unauthenticated')
  getCategoriesWithoutAuth(@Req() req) {
    if (!process.env.EXPOSE_UNAUTHENTICATED_ROUTES || process.env.EXPOSE_UNAUTHENTICATED_ROUTES === 'False') {
      throw new ForbiddenError('Unauthenticated route is not enabled');
    }
    return this.categoriesService.getCategories(req.authorizationBag);
  }
}
