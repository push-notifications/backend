import {
  JsonController,
  Post,
  BodyParam,
  Get,
  Req,
  Authorized,
  Param,
  QueryParams,
  Put,
  Delete,
  Body,
  OnUndefined,
} from 'routing-controllers';
import { Category } from '../../models/category';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { ServiceFactory } from '../../services/services-factory';
import { ChannelsService } from '../../services/channels-service';
import { NotificationsService } from '../../services/notifications-service';
import {
  ChannelResponse,
  ChannelsListResponse,
  CreateChannelRequest,
  UpdateChannelRequest,
  ChannelsQuery,
  Query,
  MembersListResponse,
  GroupsListResponse,
  GroupResponse,
  MemberResponse,
  GetChannelResponse,
  EditChannelResponse,
  NotificationsListResponse,
  ChannelStatsResponse,
  setTagsRequest,
  UpdateAdminGroupRequest,
  GetChannelPublicResponse,
} from './dto';
import { StatusCodeDescriptions, StatusCodes } from '../../utils/status-codes';
import { AuthorizedRequest } from '../../middleware/authorizationChecker';

@JsonController('/channels')
export class ChannelsController {
  channelsService: ChannelsService = ServiceFactory.getChannelsService();
  notificationsService: NotificationsService = ServiceFactory.getNotificationsService();

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Lists all of the channels matching the query.',
    description: 'Lists all channels.',
    operationId: 'listChannels',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelsListResponse, { description: 'List of queried channels.' })
  @Get()
  async getAllChannels(
    @Req() req: AuthorizedRequest,
    @QueryParams() query: ChannelsQuery,
  ): Promise<ChannelsListResponse> {
    Object.keys(query).forEach(key => query[key] === undefined && delete query[key]);
    return this.channelsService.getAllChannels(query, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Returns the channel with the provided channel id.',
    description: 'Returns a channel.',
    operationId: 'getChannelById',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(GetChannelResponse, { description: 'Requested channel (authenticated users)' })
  @ResponseSchema(GetChannelPublicResponse, { description: 'Requested channel (unauthenticated users)' })
  @Get('/:id')
  async getChannelById(
    @Param('id') channelId: string,
    @Req() req: AuthorizedRequest,
  ): Promise<GetChannelResponse | GetChannelPublicResponse> {
    return this.channelsService.getChannelById(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Fetches all channel information for the settings page.',
    description: 'Fetches channel information.',
    operationId: 'getChannelEdit',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(EditChannelResponse, { description: "Requested channel's full settings page information." })
  @Get('/:id/edit')
  async editChannelById(@Param('id') channelId: string, @Req() req: AuthorizedRequest): Promise<EditChannelResponse> {
    return this.channelsService.editChannelById(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Creates a new channel.',
    description: 'Creates a new channel.',
    operationId: 'createChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Channel created json object.' })
  @Post()
  async createChannel(@Body() channel: CreateChannelRequest, @Req() req: AuthorizedRequest): Promise<ChannelResponse> {
    return this.channelsService.createChannel(channel, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Deletes a channel.',
    description: 'Deletes the channel with the provided channel id.',
    operationId: 'deleteChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Delete('/:id')
  @OnUndefined(204)
  async deleteChannel(@Param('id') channelId: string, @Req() req: AuthorizedRequest): Promise<void> {
    await this.channelsService.deleteChannel(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Updates a channel.',
    description: 'Updates a channel with the provided information.',
    operationId: 'updateChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Updated channel json object.' })
  @Put()
  async updateChannel(
    @BodyParam('channel') channel: UpdateChannelRequest,
    @Req() req: AuthorizedRequest,
  ): Promise<ChannelResponse> {
    return this.channelsService.updateChannel(channel, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: "Fetches a channel's members.",
    description: 'Fetches the queried members from the channel with the provided channel id.',
    operationId: 'listChannelMembers',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(MembersListResponse, { description: 'Channel member list json object.' })
  @Get('/:id/members')
  async getChannelMembers(
    @Param('id') channelId: string,
    @QueryParams() query: Query,
    @Req() req: AuthorizedRequest,
  ): Promise<MembersListResponse> {
    Object.keys(query).forEach(key => query[key] === undefined && delete query[key]);
    return this.channelsService.getChannelMembers(channelId, query, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Adds a member to a channel.',
    description: 'Adds a member with the provided username to the channel with the provided channel id.',
    operationId: 'addMemberToChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(MemberResponse, { description: 'Added member json object.' })
  @Post('/:id/members/:username')
  async addMemberToChannel(
    @Param('id') channelId: string,
    @Param('username') username: string,
    @Req() req: AuthorizedRequest,
  ): Promise<MemberResponse> {
    return this.channelsService.addMemberToChannel(username, channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Removes a member from a channel.',
    description: 'Removes a member with the provided username from the channel with the provided channel id.',
    operationId: 'removeMemberFromChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.NoContent]: StatusCodeDescriptions[StatusCodes.NoContent],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
    },
  })
  @Delete('/:id/members/:userId')
  @OnUndefined(204)
  async removeMemberFromChannel(
    @Param('id') channelId: string,
    @Param('userId') memberId: string,
    @Req() req: AuthorizedRequest,
  ): Promise<void> {
    await this.channelsService.removeMemberFromChannel(memberId, channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: "Fetches a channel's member groups.",
    description:
      'Fetches the member groups of the channel with the provided channel id, according to the query parameters.',
    operationId: 'listChannelGroups',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(GroupsListResponse, { description: 'List of channel member groups.' })
  @Get('/:id/groups')
  async getChannelGroups(
    @Param('id') channelId: string,
    @QueryParams() query: Query,
    @Req() req: AuthorizedRequest,
  ): Promise<GroupsListResponse> {
    Object.keys(query).forEach(key => query[key] === undefined && delete query[key]);
    return this.channelsService.getChannelGroups(channelId, query, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Adds a group to a channel.',
    description: 'Adds a group with the provided group name to the channel with the provided channel id.',
    operationId: 'addGroupToChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(GroupResponse, { description: 'Group added to the channel as member.' })
  @Put('/:id/groups/:groupname')
  async addGroupToChannel(
    @Param('id') channelId: string,
    @Param('groupname') groupName: string,
    @Req() req: AuthorizedRequest,
  ): Promise<GroupResponse> {
    return this.channelsService.addGroupToChannel(groupName, channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: "Removes a channel's member group.",
    description: 'Removes the member group with the provided group id from the channel with the provided channel id.',
    operationId: 'removeGroupFromChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.NoContent]: StatusCodeDescriptions[StatusCodes.NoContent],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Delete('/:id/groups/:groupid')
  @OnUndefined(204)
  async removeGroupFromChannel(
    @Param('id') channelId: string,
    @Param('groupid') groupId: string,
    @Req() req: AuthorizedRequest,
  ): Promise<void> {
    await this.channelsService.removeGroupFromChannel(groupId, channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: "Sets a group as channel's admin group.",
    description: "Sets the channel's admin group to the group with the provided group name.",
    operationId: 'updateChannelAdminGroup',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Changed channel information.' })
  @Put('/:id/admingroup')
  async updateAdminGroup(
    @Param('id') channelId: string,
    @Body() newAdminGroup: UpdateAdminGroupRequest,
    @Req() req: AuthorizedRequest,
  ): Promise<ChannelResponse> {
    return this.channelsService.updateChannelAdminGroup(newAdminGroup, channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: "Removes the channel's admin group.",
    description: "Removes the channel's admin group.",
    operationId: 'removeAdminGroupToChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.NoContent]: StatusCodeDescriptions[StatusCodes.NoContent],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Delete('/:id/admingroup')
  @OnUndefined(204)
  async removeChannelAdminGroup(@Param('id') id: string, @Req() req: AuthorizedRequest): Promise<void> {
    return this.channelsService.removeChannelAdminGroup(id, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Subscribes the caller to the channel.',
    description: 'Subscribes the endpoint-calling user to the channel with the provided channel id.',
    operationId: 'subscribeToChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Put('/:id/subscribe')
  @OnUndefined(204)
  async subscribeToChannel(@Req() req: AuthorizedRequest, @Param('id') channelId: string): Promise<void> {
    await this.channelsService.subscribeToChannel(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Unsubscribes the caller from the channel.',
    description: 'Unsubscribes the endpoint-calling user from the channel with the provided channel id.',
    operationId: 'unsubcribeFromChannel',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Put('/:id/unsubscribe')
  @OnUndefined(204)
  async unsubscribeFromChannel(@Req() req: AuthorizedRequest, @Param('id') channelId: string): Promise<void> {
    return this.channelsService.unsubscribeFromChannel(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Generates an API Key for the channel.',
    description: 'Generates an API Key belonging to the channel with the provided channel id.',
    operationId: 'generateAPIKey',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Channel after API Key generation.' })
  @Put('/:id/apikey')
  async generateApiKey(@Req() req: AuthorizedRequest, @Param('id') channelId: string): Promise<string> {
    return this.channelsService.generateApiKey(channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Sets a category for the channel.',
    description: 'Sets the provided category, on the channel with the provided channel id.',
    operationId: 'setChannelCategory',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Channel after setting a category.' })
  @Put('/:id/category')
  async setCategory(
    @Req() req: AuthorizedRequest,
    @Param('id') channelId: string,
    @BodyParam('category') category: Category,
  ): Promise<ChannelResponse> {
    return this.channelsService.setCategory(channelId, category, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE])
  @OpenAPI({
    summary: 'Sets tags for the channel.',
    description: 'Sets the provided tags, on the channel with the provided channel id.',
    operationId: 'setChannelTags',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Channel after setting tags.' })
  @Put('/:id/tags')
  async setTags(
    @Req() req: AuthorizedRequest,
    @Param('id') channelId: string,
    @Body() tags: setTagsRequest,
  ): Promise<ChannelResponse> {
    return this.channelsService.setTags(channelId, tags, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: "Fetches the channel's notifications.",
    description:
      'Fetches the notifications of the channel with the provided channel id, according to the query parameters.',
    operationId: 'listChannelNotifications',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(NotificationsListResponse, { description: 'List of notifications on the channel and their count.' })
  @Get('/:id/notifications')
  async getNotifications(
    @Req() req: AuthorizedRequest,
    @Param('id') channelId: string,
    @QueryParams() query: Query,
  ): Promise<NotificationsListResponse> {
    Object.keys(query).forEach(key => query[key] === undefined && delete query[key]);
    return this.notificationsService.findAllNotifications(channelId, query, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.SUPPORTER_ROLE])
  @OpenAPI({
    summary: "Sets the channel's owner.",
    description: 'Sets the owner of the channel with the provided channel id, to the user with the provided username.',
    operationId: 'setChannelOwner',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelResponse, { description: 'Channel after setting the owner.' })
  @Put('/:id/owner')
  async setChannelOwner(
    @Param('id') channelId: string,
    @BodyParam('username') username: string,
    @Req() req: AuthorizedRequest,
  ): Promise<ChannelResponse> {
    return this.channelsService.setChannelOwner(username, channelId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: "Gets the channel's stats.",
    description: 'Gets the stats of the channel with the provided channel id.',
    operationId: 'getChannelStats',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelStatsResponse, { description: 'Channel stats json object.' })
  @Get('/:id/stats')
  async getChannelStats(@Param('id') channelId: string, @Req() req: AuthorizedRequest): Promise<ChannelStatsResponse> {
    return this.channelsService.getChannelStats(channelId, req.authorizationBag);
  }
}
