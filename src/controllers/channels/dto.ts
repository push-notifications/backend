import {
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
  IsBoolean,
  IsEnum,
  IsEmail,
  IsNumber,
  MinLength,
  MaxLength,
  IsUUID,
  Min,
  IsDateString,
  IsUrl,
} from 'class-validator';
import { Type } from 'class-transformer';
import { JSONSchema } from 'class-validator-jsonschema';
import { ChannelFlags, ChannelType, SubmissionByEmail, SubmissionByForm, Visibility } from '../../models/channel-enums';
import { Notification } from '../../models/notification';
import { Channel } from '../../models/channel';
import { Group } from '../../models/group';
import { User } from '../../models/user';
import { v4 } from 'uuid';
import { GetNotificationResponse } from '../notifications/dto';

// TODO: fix relationship counters
export class Category {
  @IsString()
  id: string;

  @IsString()
  name: string;
}

export class Tag {
  @IsString()
  id: string;

  @IsString()
  name: string;
}

@JSONSchema({
  description: 'Member groups query parameters.',
  example: {
    take: 10,
    skip: 20,
    searchText: 'groupname',
  },
})
export class Query {
  @JSONSchema({
    description: 'For pagination, number of items to skip in result. Default: 10.',
  })
  @IsOptional()
  @IsNumber()
  take: number;

  @JSONSchema({
    description: 'For pagination, number of items to skip in result. Default: 0.',
  })
  @IsOptional()
  @IsNumber()
  skip: number;

  @JSONSchema({
    description: 'Search text to be used.',
  })
  @IsOptional()
  @IsString()
  searchText: string;
}

@JSONSchema({
  description: 'New Device json input.',
  example: {
    take: 10,
    skip: 20,
    searchText: 'ITMM minutes',
    ownerFilter: false,
    subscribedFilter: true,
    favoritesFilter: false,
    tags: ['News', 'Announcements'],
    category: 'CDA',
  },
})
export class ChannelsQuery extends Query {
  @JSONSchema({
    description: 'If channels returned must be owned by the user.',
    example: false,
  })
  @IsBoolean()
  ownerFilter: boolean;

  @JSONSchema({
    description: 'If channels returned must be subscribed to by the user.',
    example: false,
  })
  @IsBoolean()
  subscribedFilter: boolean;

  @JSONSchema({
    description: 'If channels returned must be favorited by the user.',
    example: false,
  })
  @IsBoolean()
  favoritesFilter: boolean;

  @JSONSchema({
    description: 'Tags that the returned channels must have.',
  })
  @IsOptional()
  @IsString({ each: true })
  tags: string[];

  @JSONSchema({
    description: 'Category that the returned channels must have.',
  })
  @IsOptional()
  @IsString()
  category: string;
}

@JSONSchema({
  description: 'List of groups.',
  example: {
    groups: [
      {
        id: v4(),
        groupIdentifier: 'group1name',
      },
      {
        id: v4(),
        groupIdentifier: 'group2name',
      },
    ],
    totalNumberOfGroups: 2,
  },
})
export class GroupsListResponse {
  @JSONSchema({
    description: 'List of groups.',
    example: [
      {
        id: v4(),
        groupIdentifier: 'group1name',
      },
      {
        id: v4(),
        groupIdentifier: 'group2name',
      },
    ],
  })
  @ValidateNested({ each: true })
  @Type(() => GroupResponse)
  groups: GroupResponse[];

  @JSONSchema({
    description: 'Number of groups in the list.',
    example: 2,
  })
  @IsNumber()
  totalNumberOfGroups: number;

  constructor(list: Group[], count = 0) {
    this.groups = list.map(g => new GroupResponse(g));
    this.totalNumberOfGroups = count;
  }
}

@JSONSchema({
  description: 'List of channel members json object.',
  example: {
    members: [
      {
        id: v4(),
        username: 'member1username',
        email: 'member1email',
      },
      {
        id: v4(),
        username: 'member2username',
        email: 'member2email',
      },
    ],
    totalNumberOfMembers: 2,
  },
})
export class MembersListResponse {
  @ValidateNested({ each: true })
  @Type(() => MemberResponse)
  @JSONSchema({
    description: 'Members list.',
    example: [
      {
        id: v4(),
        username: 'member1username',
        email: 'member1email',
      },
      {
        id: v4(),
        username: 'member2username',
        email: 'member2email',
      },
    ],
  })
  members: MemberResponse[];

  @IsNumber()
  @JSONSchema({
    description: 'Number of channel members returned.',
    example: 2,
  })
  totalNumberOfMembers: number;

  constructor(list: User[] = [], count = 0) {
    this.members = list.map(u => new MemberResponse(u));
    this.totalNumberOfMembers = count;
  }
}

@JSONSchema({
  description: 'List of notifications json object.',
  example: 2,
})
export class NotificationsListResponse {
  @ValidateNested({ each: true })
  @Type(() => GetNotificationResponse)
  @JSONSchema({
    description: 'List of notifications.',
    example: [{}],
  })
  items: GetNotificationResponse[]; //would be NotificationResponse, but wait for other MR to be merged

  @JSONSchema({
    description: 'Number of notifications returned.',
    example: 2,
  })
  @IsNumber()
  count: number;

  constructor(list: Notification[] = [], count = 0) {
    this.items = list.map(notification => new GetNotificationResponse(notification));
    this.count = count;
  }
}

@JSONSchema({
  description: 'Channel member json object.',
  example: {
    id: v4(),
    username: 'memberusername',
    email: 'memberemail',
  },
})
export class MembersResponse {
  @IsString()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel member id.',
    example: v4(),
  })
  id: string;

  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Channel member username.',
    example: 'memberusername',
  })
  username: string;

  @IsString()
  @IsEmail()
  @JSONSchema({
    description: 'Channel member email.',
    example: 'memberemail',
  })
  email: string;

  constructor(member: User) {
    this.id = member.id;
    this.username = member.username;
    this.email = member.email;
  }
}

export class GroupsResponse {
  @IsNumber()
  count: number;

  constructor(count: number) {
    this.count = count;
  }
}

@JSONSchema({
  description: 'Channel stats object.',
  example: {
    id: v4(),
    name: 'channelname',
    members: 20,
    unsubscribed: 2,
    owner: {
      id: v4(),
      name: 'ownerusername',
    },
    creationDate: new Date(),
    lastActivityDate: new Date(),
    groups: 3,
  },
})
export class ChannelStatsResponse {
  @IsString()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel id.',
    example: v4(),
  })
  id: string;

  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Channel name.',
    example: 'channelname',
  })
  name: string;

  @ValidateNested({ each: true })
  @Type(() => User)
  @JSONSchema({
    description: 'Channel members.',
    example: 20,
  })
  members: number;

  @IsNumber()
  @Min(0)
  @JSONSchema({
    description: 'Number of unsubscribed channel members.',
    example: 2,
  })
  unsubscribed: number;

  @Type(() => User)
  @JSONSchema({
    description: 'Channel owner.',
    example: {
      id: v4(),
      name: 'ownerusername',
    },
  })
  owner: User;

  @IsDateString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'The date the channel was created.',
    example: new Date(),
  })
  creationDate: Date;

  @IsDateString()
  @IsNotEmpty()
  @JSONSchema({
    description: 'The date the channel was last active.',
    example: new Date(),
  })
  lastActivityDate: Date;

  @IsNumber()
  @Min(0)
  @JSONSchema({
    description: 'Number of channel member groups.',
    example: 3,
  })
  groups: number;

  constructor(channel: Channel) {
    this.id = channel.id;
    // this.members = channel.members.length;
    this.unsubscribed = channel.unsubscribed.length;
    this.owner = channel.owner;
    this.creationDate = channel.creationDate;
    this.lastActivityDate = channel.lastActivityDate;
    // this.groups = channel.groups.length;
  }
}

export class CountResponse {
  @IsNumber()
  count: number;

  constructor(count: number) {
    this.count = count;
  }
}

export class RelationshipResponse {
  @Type(() => CountResponse)
  notifications: CountResponse;

  @Type(() => CountResponse)
  members: CountResponse;

  @Type(() => CountResponse)
  groups: CountResponse;

  constructor(notificationsCount: number, membersCount: number, groupsCount: number) {
    this.notifications = new CountResponse(notificationsCount);
    this.members = new CountResponse(membersCount);
    this.groups = new CountResponse(groupsCount);
  }
}

export class GetChannelPublicResponse {
  @IsNotEmpty()
  @IsString()
  id: string;

  @IsNotEmpty()
  @IsString()
  slug: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(256)
  description: string;

  @IsNotEmpty()
  @IsString()
  @MinLength(4)
  @MaxLength(128)
  name: string;

  @IsNotEmpty()
  @IsEnum(Visibility)
  visibility: Visibility;

  @IsBoolean()
  archive: boolean;

  @Type(() => Category)
  @IsOptional()
  category: Category;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => Tag)
  tags: Tag[];

  @IsEnum(ChannelFlags, { each: true })
  @JSONSchema({
    description: 'List of labels related to the channel.',
    example: [ChannelFlags.critical, ChannelFlags.mandatory],
  })
  channelFlags: ChannelFlags[];

  constructor(channel: Channel) {
    this.id = channel.id;
    this.slug = channel.slug;
    this.description = channel.description;
    this.name = channel.name;
    this.visibility = channel.visibility;
    this.archive = channel.archive;
    this.category = channel.category;
    this.tags = channel.tags;
    this.channelFlags = channel.channelFlags;
  }
}

export class GetChannelResponse extends GetChannelPublicResponse {
  @IsBoolean()
  @JSONSchema({
    description: 'Whether the user is subscribed to this channel.',
    example: true,
  })
  subscribed: boolean;

  @IsBoolean()
  @JSONSchema({
    description: 'Whether the user can manage this channel.',
    example: true,
  })
  manage: boolean;

  @IsBoolean()
  @JSONSchema({
    description: 'Whether the user can send notifications to this channel.',
    example: true,
  })
  send: boolean;

  @IsOptional()
  @Type(() => RelationshipResponse)
  relationships: RelationshipResponse;

  @IsBoolean()
  @JSONSchema({
    description: 'Whether the channel allows private notifications to be sent.',
    example: false,
  })
  sendPrivate: boolean;

  constructor(channel: Channel, subscribed: boolean, manage: boolean, send: boolean) {
    super(channel);
    this.subscribed = subscribed;
    this.manage = manage;
    this.send = send;
    this.relationships = new RelationshipResponse(
      channel.notificationCount,
      0, // channel.members?.length || 0,
      0, // channel.groups?.length || 0,
    );
    this.sendPrivate = channel.sendPrivate;
  }
}

@JSONSchema({
  description: 'Device json return.',
  example: {
    channels: [
      {
        id: v4(),
        slug: 'channel1-name',
        description: 'channel1description',
        name: 'channel1 name',
        visibility: Visibility.restricted,
        archive: true,
        subscribed: true,
        manage: true,
        send: true,
        category: null,
        tags: [],
        relationships: {
          notifications: {
            count: 73,
          },
          members: {
            count: 35,
          },
          groups: {
            count: 2,
          },
        },
      },
      {
        id: v4(),
        slug: 'channel2-name',
        description: 'channel2description',
        name: 'channel2 name',
        visibility: Visibility.restricted,
        archive: true,
        subscribed: true,
        manage: true,
        send: true,
        category: null,
        tags: [],
        relationships: {
          notifications: {
            count: 13,
          },
          members: {
            count: 2,
          },
          groups: {
            count: 1,
          },
        },
      },
    ],
    count: 2,
  },
})
export class PublicChannelsListResponse {
  @JSONSchema({
    description: 'List of channels - public view.',
    example: [
      {
        id: v4(),
        slug: 'channel1-name',
        description: 'channel1description',
        name: 'channel1 name',
        visibility: Visibility.restricted,
        archive: true,
        category: null,
        tags: [],
      },
      {
        id: v4(),
        slug: 'channel2-name',
        description: 'channel2description',
        name: 'channel2 name',
        visibility: Visibility.restricted,
        archive: true,
        category: null,
        tags: [],
      },
    ],
  })
  @ValidateNested({ each: true })
  @Type(() => GetChannelPublicResponse)
  channels: GetChannelPublicResponse[];

  @IsNumber()
  @JSONSchema({
    description: 'Number of returned channels.',
    example: 2,
  })
  count: number;

  constructor(list: GetChannelPublicResponse[], count: number) {
    this.channels = list;
    this.count = count;
  }
}

export class ChannelsListResponse extends PublicChannelsListResponse {
  @JSONSchema({
    description: 'List of channels.',
    example: [
      {
        id: v4(),
        slug: 'channel1-name',
        description: 'channel1description',
        name: 'channel1 name',
        visibility: Visibility.restricted,
        archive: true,
        subscribed: true,
        manage: true,
        send: true,
        category: null,
        tags: [],
        relationships: {
          notifications: {
            count: 73,
          },
          members: {
            count: 35,
          },
          groups: {
            count: 2,
          },
        },
      },
      {
        id: v4(),
        slug: 'channel2-name',
        description: 'channel2description',
        name: 'channel2 name',
        visibility: Visibility.restricted,
        archive: true,
        subscribed: true,
        manage: true,
        send: true,
        category: null,
        tags: [],
        relationships: {
          notifications: {
            count: 13,
          },
          members: {
            count: 2,
          },
          groups: {
            count: 1,
          },
        },
      },
    ],
  })
  @ValidateNested({ each: true })
  @Type(() => GetChannelResponse)
  channels: GetChannelResponse[];

  constructor(list: GetChannelResponse[] = [], count = 0) {
    super(list, count);
  }
}

@JSONSchema({
  description: 'Channel object json.',
  example: {
    id: v4(),
    name: 'CERN Notifications Updates',
    description: 'Service updates for CERN Notifications.',
    category: {
      name: 'CDA',
      id: v4(),
    },
    tags: [
      {
        id: v4(),
        name: 'Channel tag1',
      },
      {
        id: v4(),
        name: 'Channel tag2',
      },
    ],
  },
})
@JSONSchema({
  description: 'Channel full settings page information.',
  example: {
    id: v4(),
    name: 'CERN Notifications Updates',
    slug: 'cern-notification-updates',
    owner: {
      id: v4(),
      username: 'user',
      email: 'useremail@cern.ch',
      enabled: true,
      created: new Date(),
      lastLogin: new Date(),
    },
    description: 'Service updates for CERN Notifications.',
    adminGroup: {
      id: v4(),
      groupIdentifier: 'admingroupname',
    },
    visibility: Visibility.public,
    sendPrivate: false,
    incomingEmail: 'someemail@cern.ch',
    incomingEgroup: 'notifications-service-admins@cern.ch',
    submissionByEmail: [
      SubmissionByEmail.administrators,
      SubmissionByEmail.members,
      SubmissionByEmail.email,
      SubmissionByEmail.egroup,
    ],
    submissionByForm: [SubmissionByForm.administrators, SubmissionByForm.members, SubmissionByForm.apikey],
    archive: true,
    APIKey: 'ck_' + v4() + '_' + v4(),
    tags: [
      {
        id: v4(),
        name: 'Channel tag1',
      },
      {
        id: v4(),
        name: 'Channel tag2',
      },
    ],
    category: {
      id: v4(),
      name: 'CDA',
    },
  },
})
export class EditChannelResponse {
  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel id.',
    example: v4(),
  })
  id: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Channel name.',
    example: 'CERN Notifications Updates',
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Channel slug. Usually a slugified version of the name, eg channel-name.',
    example: 'cern-notification-updates',
  })
  slug: string;

  @IsUrl()
  @JSONSchema({
    description: 'Channel short URL. Self generated, non editable.',
    example: 'cern.ch/n-abc',
  })
  shortUrl: string;

  @IsNotEmpty()
  @JSONSchema({
    description: 'Channel owner.',
    example: {
      id: v4(),
      username: 'user',
      email: 'useremail@cern.ch',
      enabled: true,
      created: new Date(),
      lastLogin: new Date(),
    },
  })
  owner: User;

  @IsString()
  @JSONSchema({
    description: 'Channel description.',
    example: 'Service updates for CERN Notifications.',
  })
  description: string;

  @IsOptional()
  @JSONSchema({
    description: 'Channel admin group.',
    example: {
      id: v4(),
      groupIdentifier: 'admingroupname',
    },
  })
  adminGroup: Group;

  @IsNotEmpty()
  @IsEnum(Visibility)
  @JSONSchema({
    description: 'Channel visibility.',
    example: Visibility.public,
  })
  visibility: Visibility;

  @IsBoolean()
  @JSONSchema({
    description: "The channel's setting for allowing private notifications.",
    example: false,
  })
  sendPrivate: boolean;

  @IsString()
  @JSONSchema({
    description: 'Emails which can send notifications by email to the channel.',
    example: 'someemail@cern.ch',
  })
  incomingEmail: string;

  @IsString()
  @JSONSchema({
    description: 'Egroup which can send notifications by email to the channel.',
    example: 'notifications-service-admins@cern.ch',
  })
  incomingEgroup: string;

  @IsEnum(SubmissionByEmail, { each: true })
  @JSONSchema({
    description: 'Determines who and how can send notifications by email.',
    example: [
      SubmissionByEmail.administrators,
      SubmissionByEmail.members,
      SubmissionByEmail.email,
      SubmissionByEmail.egroup,
    ],
  })
  submissionByEmail: SubmissionByEmail[];

  @IsEnum(SubmissionByForm, { each: true })
  @JSONSchema({
    description: 'Who can send notifications via Web or API. Default is ADMINISTRATORS.',
    example: [SubmissionByForm.administrators, SubmissionByForm.members, SubmissionByForm.apikey],
  })
  submissionByForm: SubmissionByForm[];

  @IsBoolean()
  @JSONSchema({
    description:
      "Allows enabling preservation of the channel's notifications into an external archive. Notifications are deleted from our system (not the archive) after 13 months in any case.",
    example: true,
  })
  archive: boolean;

  @IsString()
  @JSONSchema({
    description: 'API Key.',
    example: 'ck_' + v4() + '_' + v4(),
  })
  APIKey: string;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => Tag)
  @JSONSchema({
    description: "The channel's Tags.",
    example: [
      {
        id: v4(),
        name: 'Channel tag1',
      },
      {
        id: v4(),
        name: 'Channel tag2',
      },
    ],
  })
  tags: Tag[];

  @Type(() => Category)
  @IsOptional()
  @JSONSchema({
    description: "The channel's Category.",
    example: {
      id: v4(),
      name: 'CDA',
    },
  })
  category: Category;

  @IsNotEmpty()
  @IsEnum(ChannelType)
  @JSONSchema({
    description: 'Channel type.',
    example: ChannelType.personal,
  })
  channelType: ChannelType;

  @IsEnum(ChannelFlags, { each: true })
  @JSONSchema({
    description: 'List of labels related to the channel.',
    example: [ChannelFlags.critical, ChannelFlags.mandatory],
  })
  channelFlags: ChannelFlags[];

  constructor(channel: Channel) {
    this.id = channel.id;
    this.name = channel.name;
    this.slug = channel.slug;
    this.shortUrl = channel.shortUrl;
    this.owner = channel.owner;
    this.description = channel.description;
    this.adminGroup = channel.adminGroup;
    this.visibility = channel.visibility;
    this.sendPrivate = channel.sendPrivate;
    this.incomingEmail = channel.incomingEmail;
    this.incomingEgroup = channel.incomingEgroup;
    this.submissionByEmail = channel.submissionByEmail;
    this.submissionByForm = channel.submissionByForm;
    this.archive = channel.archive;
    this.tags = channel.tags;
    this.category = channel.category;
    this.APIKey = channel.APIKey;
    this.channelType = channel.channelType;
    this.channelFlags = channel.channelFlags;
  }
}

@JSONSchema({
  description: 'Channel response json object.',
  example: {
    id: v4(),
    name: 'CERN Notifications Updates',
    slug: 'cern-notification-updates',
    owner: {
      id: v4(),
      username: 'user',
      email: 'useremail@cern.ch',
      enabled: true,
      created: new Date(),
      lastLogin: new Date(),
    },
    description: 'Service updates for CERN Notifications.',
    channelType: ChannelType.personal,
    members: [
      {
        id: v4(),
        username: 'user1',
        email: 'user1email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
      {
        id: v4(),
        username: 'user2',
        email: 'user2email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
    ],
    adminGroup: {
      id: v4(),
      groupIdentifier: 'admingroupname',
    },
    unsubscribed: [
      {
        id: v4(),
        username: 'user3',
        email: 'user3email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
      {
        id: v4(),
        username: 'user4',
        email: 'user4email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
    ],
    groups: [
      {
        id: v4(),
        groupIdentifier: 'group1',
      },
      {
        id: v4(),
        groupIdentifier: 'group2',
      },
    ],
    visibility: Visibility.public,
    incomingEmail: 'someemail@cern.ch',
    incomingEgroup: 'notifications-service-admins@cern.ch',
    submissionByEmail: [
      SubmissionByEmail.administrators,
      SubmissionByEmail.members,
      SubmissionByEmail.email,
      SubmissionByEmail.egroup,
    ],
    submissionByForm: [SubmissionByForm.administrators, SubmissionByForm.members, SubmissionByForm.apikey],
    archive: true,
    APIKey: 'ck_' + v4() + '_' + v4(),
    sendPrivate: false,
    creationDate: new Date(),
    lastActivityDate: new Date(),
    deleteDate: new Date(),
    subscribed: true,
    manage: true,
    send: true,
    tags: [
      {
        id: v4(),
        name: 'Channel tag1',
      },
      {
        id: v4(),
        name: 'Channel tag2',
      },
    ],
    category: {
      id: v4(),
      name: 'CDA',
    },
    relationships: {
      notifications: {
        count: 13,
      },
      members: {
        count: 2,
      },
      groups: {
        count: 1,
      },
    },
  },
})
export class ChannelResponse {
  @IsNotEmpty()
  @JSONSchema({
    description: "The channel's id.",
    example: v4(),
  })
  id: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Channel slug. Usually a slugified version of the name, eg channel-name.',
    example: 'channelname',
  })
  slug: string;

  @IsUrl()
  @JSONSchema({
    description: 'Channel short URL. Self generated, non editable.',
    example: 'cern.ch/n-abc',
  })
  shortUrl: string;

  @IsNotEmpty()
  @JSONSchema({
    description: "The channel's owner.",
    example: {
      id: v4(),
      username: 'user',
      email: 'useremail@cern.ch',
      enabled: true,
      created: new Date(),
      lastLogin: new Date(),
    },
  })
  owner: User;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: "The channel's name.",
    example: 'Channel name',
  })
  name: string;

  @IsString()
  @JSONSchema({
    description: "The channel's description.",
    example: 'This channel is not real.',
  })
  description: string;

  @ValidateNested({ each: true })
  @Type(() => User)
  @JSONSchema({
    description: "The channel's members.",
    example: [
      {
        id: v4(),
        username: 'user1',
        email: 'user1email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
      {
        id: v4(),
        username: 'user2',
        email: 'user2email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
    ],
  })
  members: User[];

  @IsOptional()
  @JSONSchema({
    description: "The channel's admin group.",
    example: {
      id: v4(),
      groupIdentifier: 'admingroupname',
    },
  })
  adminGroup: Group;

  @ValidateNested({ each: true })
  @Type(() => User)
  @JSONSchema({
    description: "The channel's unsubscribed members.",
    example: [
      {
        id: v4(),
        username: 'user3',
        email: 'user3email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
      {
        id: v4(),
        username: 'user4',
        email: 'user4email@cern.ch',
        enabled: true,
        created: new Date(),
        lastLogin: new Date(),
      },
    ],
  })
  unsubscribed: User[];

  @ValidateNested({ each: true })
  @Type(() => Group)
  @JSONSchema({
    description: "The channel's groups.",
    example: [
      {
        id: v4(),
        groupIdentifier: 'group1',
      },
      {
        id: v4(),
        groupIdentifier: 'group2',
      },
    ],
  })
  groups: Group[];

  @IsNotEmpty()
  @IsEnum(Visibility)
  @JSONSchema({
    description: "The channel's visibility.",
    example: Visibility.restricted,
  })
  visibility: Visibility;

  @IsBoolean()
  @JSONSchema({
    description: "The channel's archival option.",
    example: true,
  })
  archive: boolean;

  @IsNotEmpty()
  @IsEnum(ChannelType)
  @JSONSchema({
    description: "The channel's type.",
    example: ChannelType.personal,
  })
  channelType: ChannelType;

  @IsString()
  @JSONSchema({
    description: "The channel's APIKey.",
    example: 'ck_' + v4() + '_' + v4(),
  })
  APIKey: string;

  @IsBoolean()
  @JSONSchema({
    description: "The channel's setting for allowing private notifications.",
    example: false,
  })
  sendPrivate: boolean;

  @IsDateString()
  @JSONSchema({
    description: "The channel's creation date.",
    example: new Date(),
  })
  creationDate: Date;

  @IsDateString()
  @JSONSchema({
    description: "The channel's last activity date.",
    example: new Date(),
  })
  lastActivityDate: Date;

  @IsEmail()
  @JSONSchema({
    description: "The channel's incoming email for notifications.",
    example: 'channelname+level@cern.ch',
  })
  incomingEmail: string;

  @IsString()
  @JSONSchema({
    description: "The channel's incoming egroup for notifications.",
    example: 'incomingegroupname@cern.ch',
  })
  incomingEgroup: string;

  @IsEnum(SubmissionByEmail, { each: true })
  @JSONSchema({
    description: 'Determines who and how can send notifications by email.',
    example: [
      SubmissionByEmail.administrators,
      SubmissionByEmail.members,
      SubmissionByEmail.email,
      SubmissionByEmail.egroup,
    ],
  })
  submissionByEmail: SubmissionByEmail[];

  @IsEnum(SubmissionByForm, { each: true })
  @JSONSchema({
    description: 'Who can send notifications via Web or API. Default is ADMINISTRATORS.',
    example: [SubmissionByForm.administrators, SubmissionByForm.members, SubmissionByForm.apikey],
  })
  submissionByForm: SubmissionByForm[];

  @IsOptional()
  @IsDateString()
  @JSONSchema({
    description: "The channel's deletion date.",
    example: new Date(),
  })
  deleteDate: Date;

  @IsBoolean()
  @IsOptional()
  @JSONSchema({
    description: 'Whether the user is subscribed to this channel.',
    example: true,
  })
  subscribed: boolean;

  @IsBoolean()
  @IsOptional()
  @JSONSchema({
    description: 'Whether the user can manage this channel.',
    example: true,
  })
  manage: boolean;

  @IsBoolean()
  @IsOptional()
  @JSONSchema({
    description: 'Whether the user can send notifications to this channel.',
    example: true,
  })
  send: boolean;

  @Type(() => Category)
  @IsOptional()
  @JSONSchema({
    description: 'Channel category.',
    example: {
      name: 'CDA',
      id: v4(),
    },
  })
  category: Category;

  @IsOptional()
  @ValidateNested({ each: true })
  @Type(() => Tag)
  @JSONSchema({
    description: "The channel's Tags.",
    example: [
      {
        id: v4(),
        name: 'Channel tag1',
      },
      {
        id: v4(),
        name: 'Channel tag2',
      },
    ],
  })
  tags: Tag[];

  @IsOptional()
  @Type(() => RelationshipResponse)
  @JSONSchema({
    description: "The channel's Tags.",
    example: {
      notifications: {
        count: 13,
      },
      members: {
        count: 2,
      },
      groups: {
        count: 1,
      },
    },
  })
  relationships: RelationshipResponse;

  @IsEnum(ChannelFlags, { each: true })
  @JSONSchema({
    description: 'List of labels related to the channel.',
    example: [ChannelFlags.critical, ChannelFlags.mandatory],
  })
  channelFlags: ChannelFlags[];

  constructor(channel: Channel) {
    this.id = channel.id;
    this.slug = channel.slug;
    this.shortUrl = channel.shortUrl;
    this.owner = channel.owner;
    this.name = channel.name;
    this.description = channel.description;
    this.adminGroup = channel.adminGroup;
    this.unsubscribed = channel.unsubscribed;
    //this.notifications = channel.notifications;
    this.visibility = channel.visibility;
    this.archive = channel.archive;
    this.APIKey = channel.APIKey;
    this.creationDate = channel.creationDate;
    this.lastActivityDate = channel.lastActivityDate;
    this.incomingEmail = channel.incomingEmail;
    this.incomingEgroup = channel.incomingEgroup;
    this.submissionByEmail = channel.submissionByEmail;
    this.submissionByForm = channel.submissionByForm;
    this.deleteDate = channel.deleteDate;
    this.category = channel.category;
    this.tags = channel.tags;
    this.sendPrivate = channel.sendPrivate;
    this.channelFlags = channel.channelFlags;
    this.channelType = channel.channelType;
  }
}

@JSONSchema({
  description: 'Channel member json object.',
  example: {},
})
export class MemberResponse {
  @IsString()
  @IsUUID('4')
  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Member id.',
    example: v4(),
  })
  id: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Member username.',
    example: 'username',
  })
  username: string;

  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @JSONSchema({
    description: 'Member email.',
    example: 'useremail@cern.ch',
  })
  email: string;

  constructor(user: User) {
    this.id = user.id;
    this.username = user.username;
    this.email = user.email;
  }
}

export class GroupResponse {
  @IsNotEmpty()
  @IsString()
  id: string;

  @IsNotEmpty()
  @IsString()
  groupIdentifier: string;

  constructor(group: Group) {
    this.id = group.id;
    this.groupIdentifier = group.groupIdentifier;
  }
}

//REQUESTS
@JSONSchema({
  description: 'Channel admin group.',
  example: {
    groupName: 'admingroupname',
  },
})
export class UpdateAdminGroupRequest {
  @IsString()
  @IsNotEmpty()
  @JSONSchema({
    description: "Channel's new admin group's name.",
  })
  newAdminGroup: string;
}

export class GroupRequest {
  @IsString()
  groupIdentifier: string;
}

@JSONSchema({
  description: 'Channel create request object.',
  example: {
    name: 'Channel name',
    slug: 'channel-name',
    description: 'This channel is not real.',
    channelType: ChannelType.personal,
    visibility: Visibility.restricted,
    archive: true,
    submissionByForm: [SubmissionByForm.administrators],
    submissionByEmail: [SubmissionByEmail.administrators],
  },
})
export class CreateChannelRequest {
  @Type(() => Category)
  @IsOptional()
  @JSONSchema({
    description: "The channel's Category.",
    example: {
      id: v4(),
      name: 'CDA',
    },
  })
  category: Category;

  @IsString()
  @JSONSchema({
    description: 'Channel name.',
    example: 'Channel name',
  })
  name: string;

  @IsString()
  @JSONSchema({
    description: 'Channel slug. Usually a slugified version of the name, eg channel-name.',
    example: 'channel-name',
  })
  slug: string;

  @IsString()
  @JSONSchema({
    description: 'Channel description.',
    example: 'This channel is not real.',
  })
  description: string;

  @IsNotEmpty()
  @IsEnum(ChannelType)
  @JSONSchema({
    description: "The channel's type.",
    example: ChannelType.personal,
  })
  channelType: ChannelType;

  @IsOptional()
  @IsString()
  @JSONSchema({
    description: 'Channel description.',
    example: 'groupname',
  })
  adminGroup: string;

  @IsEnum(Visibility)
  @JSONSchema({
    description: "Channel's visibility.",
    example: Visibility.restricted,
  })
  visibility: Visibility;

  @IsOptional()
  @IsString()
  @JSONSchema({
    description:
      "The channel's incoming egroup for notifications. Needs " +
      SubmissionByEmail.egroup +
      " to be set on 'submissionByEmail'.",
    example: 'egroupname',
  })
  incomingEgroup: string;

  @IsOptional()
  @IsEmail()
  @JSONSchema({
    description: "The channel's incoming email for notifications.",
    example: 'channelname+level@cern.ch',
  })
  incomingEmail: string;

  @IsBoolean()
  @JSONSchema({
    description:
      "Allows enabling preservation of the channel's notifications into an external archive. Notifications are deleted from our system (not the archive) after 13 months in any case.",
    example: true,
  })
  archive: boolean;

  @IsEnum(SubmissionByForm, { each: true })
  @JSONSchema({
    description: 'Who can send notifications via Web or API. Default is ADMINISTRATORS.',
    example: [SubmissionByForm.administrators, SubmissionByForm.members, SubmissionByForm.apikey],
  })
  submissionByForm: SubmissionByForm[];

  @IsEnum(SubmissionByEmail, { each: true })
  @JSONSchema({
    description:
      'Who and how can send notifications by email. To use ' +
      SubmissionByEmail.egroup +
      " option, needs 'incomingEgroup' to be set.",
    example: [
      SubmissionByEmail.administrators,
      SubmissionByEmail.members,
      SubmissionByEmail.email,
      SubmissionByEmail.egroup,
    ],
  })
  submissionByEmail: SubmissionByEmail[];
}

@JSONSchema({
  description: 'Information that the channel is to be updated with.',
  example: {
    id: v4(),
    description: 'New channel description.',
    name: 'New Channel name',
    channelType: ChannelType.personal,
    visibility: Visibility.public,
    sendPrivate: false,
    archive: true,
    submissionByEmail: [],
    submissionByForm: [SubmissionByForm.administrators],
  },
})
export class UpdateChannelRequest {
  @Type(() => Category)
  @IsOptional()
  @JSONSchema({
    description: "The channel's Category.",
    example: {
      id: v4(),
      name: 'CDA',
    },
  })
  category: Category;

  @IsBoolean()
  @JSONSchema({
    description:
      "Allows enabling preservation of the channel's notifications into an external archive. Notifications are deleted from our system (not the archive) after 13 months in any case.",
    example: true,
  })
  archive: boolean;

  @IsString()
  @JSONSchema({
    description: 'Channel description.',
    example: 'Service updates for CERN Notifications.',
  })
  description: string;

  @IsNotEmpty()
  @IsEnum(ChannelType)
  @JSONSchema({
    description: "The channel's type.",
    example: ChannelType.personal,
  })
  channelType: ChannelType;

  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel id.',
    example: v4(),
  })
  id: string;

  @IsOptional()
  @IsString()
  @JSONSchema({
    description:
      "The channel's incoming egroup for notifications. Needs " +
      SubmissionByEmail.egroup +
      " to be set on 'submissionByEmail'.",
    example: 'egroupname',
  })
  incomingEgroup: string;

  @IsOptional()
  @IsEmail()
  @JSONSchema({
    description: "The channel's incoming email for notifications.",
    example: 'channelname+level@cern.ch',
  })
  incomingEmail: string;

  @IsString()
  @JSONSchema({
    description: "The channel's name.",
    example: 'Channel name',
  })
  name: string;

  @IsBoolean()
  @JSONSchema({
    description: "The channel's setting for allowing private notifications.",
    example: false,
  })
  sendPrivate: boolean;

  @IsEnum(SubmissionByEmail, { each: true })
  @JSONSchema({
    description:
      'Determines who and how can send notifications by email. To use ' +
      SubmissionByEmail.egroup +
      " option, needs 'incomingEgroup' to be set.",
    example: [SubmissionByEmail.administrators],
  })
  submissionByEmail: SubmissionByEmail[];

  @IsEnum(SubmissionByForm, { each: true })
  @JSONSchema({
    description: 'Who can send notifications via Web or API. Default is ADMINISTRATORS.',
    example: [SubmissionByForm.administrators],
  })
  submissionByForm: SubmissionByForm[];

  @IsEnum(Visibility)
  @JSONSchema({
    description: "The channel's visibility.",
    example: Visibility.restricted,
  })
  visibility: Visibility;
}

@JSONSchema({
  description: 'List of Tag ids.',
  example: {
    tagIds: [v4(), v4()],
  },
})
export class setTagsRequest {
  @JSONSchema({
    description: 'List of Tag ids.',
    example: [v4(), v4()],
  })
  @IsUUID('4', { each: true })
  tagIds: string[];
}
