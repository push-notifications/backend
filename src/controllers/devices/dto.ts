import { IsEnum, IsNotEmpty, IsOptional, IsString, IsUUID, ValidateNested } from 'class-validator';
import { Device, DeviceStatus, DeviceSubType, DeviceType } from '../../models/device';
import { JSONSchema } from 'class-validator-jsonschema';
import { Type } from 'class-transformer';
import { v4 } from 'uuid';

@JSONSchema({
  description: 'Device json return.',
  example: {
    name: 'Mac Firefox',
    id: v4(),
    info: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0',
    type: DeviceType.BROWSER,
    subType: DeviceSubType.OTHER,
    uuid: v4(),
    token:
      '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
  },
})
export class DeviceResponse {
  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Name of the returned device.',
    example: 'Mac Firefox',
  })
  name: string;

  @IsUUID()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Id of the returned device.',
    example: v4(),
  })
  id: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Info about the returned device.',
    example: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0',
  })
  info: string;

  @IsNotEmpty()
  @IsEnum(DeviceType)
  @JSONSchema({
    description: 'Type of the returned device.',
    example: DeviceType.BROWSER,
  })
  type: DeviceType;

  @IsOptional()
  @IsEnum(DeviceSubType)
  @JSONSchema({
    description: 'Subtype of the returned device.',
    example: DeviceSubType.OTHER,
  })
  subType: DeviceSubType;

  @IsUUID()
  @IsOptional()
  @JSONSchema({
    description: 'Apple device identifier needed for browser registration management in Apple cloud.',
    example: v4(),
  })
  uuid: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Target identifier to use when sending a notification.',
    example:
      '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
  })
  token: string;

  @IsNotEmpty()
  @IsEnum(DeviceStatus)
  @JSONSchema({
    description: 'Status of the returned device.',
    example: DeviceStatus.OK,
  })
  status: DeviceStatus;

  constructor(device: Device) {
    this.id = device.id;
    this.name = device.name;
    this.info = device.info;
    this.type = device.type;
    this.subType = device.subType;
    this.uuid = device.uuid;
    this.token = device.token;
    this.status = device.status;
  }
}
@JSONSchema({
  description: 'Json response with list of devices.',
  example: {
    userDevices: [
      new DeviceResponse(
        new Device({
          name: 'Mattermost',
          id: v4(),
          info: 'useremail@cern.ch',
          type: DeviceType.APP,
          subType: DeviceSubType.MATTERMOST,
          uuid: null,
          token: 'useremail@cern.ch',
          status: DeviceStatus.OK,
        }),
      ),
      new DeviceResponse(
        new Device({
          name: 'useremail@cern.ch',
          id: v4(),
          info: 'useremail@cern.ch',
          type: DeviceType.MAIL,
          subType: null,
          uuid: null,
          token: 'useremail@cern.ch',
          status: DeviceStatus.OK,
        }),
      ),
      new DeviceResponse(
        new Device({
          name: 'Max Firefox',
          id: v4(),
          info: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0.',
          type: DeviceType.BROWSER,
          subType: DeviceSubType.OTHER,
          uuid: v4(),
          token:
            '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
          status: DeviceStatus.EXPIRED,
        }),
      ),
    ],
  },
})
export class GetDevicesResponse {
  @ValidateNested({ each: true })
  @Type(() => DeviceResponse)
  @JSONSchema({
    description: 'Device list ',
    example: [
      new DeviceResponse(
        new Device({
          name: 'Mattermost',
          id: v4(),
          info: 'useremail@cern.ch',
          type: DeviceType.APP,
          subType: DeviceSubType.MATTERMOST,
          uuid: null,
          token: 'useremail@cern.ch',
          status: DeviceStatus.OK,
        }),
      ),
      new DeviceResponse(
        new Device({
          name: 'useremail@cern.ch',
          id: v4(),
          info: 'useremail@cern.ch',
          type: DeviceType.MAIL,
          subType: null,
          uuid: null,
          token: 'useremail@cern.ch',
          status: DeviceStatus.OK,
        }),
      ),
      new DeviceResponse(
        new Device({
          name: 'Max Firefox',
          id: v4(),
          info: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0.',
          type: DeviceType.BROWSER,
          subType: DeviceSubType.OTHER,
          uuid: v4(),
          token:
            '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
          status: DeviceStatus.EXPIRED,
        }),
      ),
    ],
  })
  userDevices: DeviceResponse[];

  constructor(list: DeviceResponse[]) {
    this.userDevices = list;
  }
}
@JSONSchema({
  description: 'New Device json input.',
  example: {
    name: 'Mac Firefox',
    info: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0.',
    type: DeviceType.BROWSER,
    subType: DeviceSubType.OTHER,
    uuid: v4(),
    token:
      '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
  },
})
export class DeviceRequest {
  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Name of the device to be created.',
    example: 'Mac Firefox',
  })
  name: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Info about the returned device.',
    example: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0',
  })
  info: string;

  @IsNotEmpty()
  @IsEnum(DeviceType)
  @JSONSchema({
    description: 'Type of the returned device.',
    example: DeviceType.BROWSER,
  })
  type: DeviceType;

  @IsOptional()
  @IsEnum(DeviceSubType)
  @JSONSchema({
    description: 'Subtype of the returned device.',
    example: DeviceSubType.OTHER,
  })
  subType: DeviceSubType;

  @IsUUID()
  @IsOptional()
  @JSONSchema({
    description: 'Apple device identifier needed for browser registration management in Apple cloud.',
    example: v4(),
  })
  uuid: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Target identifier to use when sending a notification.',
    example:
      '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
  })
  token: string;
}

@JSONSchema({
  description: 'New Device json input.',
  example: {
    name: 'Mac Firefox',
    info: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0.',
    token:
      '{"endpoint":"https://updates.push.services.mozilla.com/wpush/v2/asfhijbsaSDArandomuuidasyrigbdsjyc","expirationTime":null,"keys":{"auth":"ahahyouwish","p256dh":"nopenopenope"}}',
  },
})
export class DeviceValuesRequest {
  @IsOptional()
  @IsString()
  @JSONSchema({
    description: 'Name the device is to be changed to.',
    example: 'Mac Firefox',
  })
  name: string;

  // @IsNotEmpty()
  @IsOptional()
  @IsString()
  @JSONSchema({
    description: 'Info the device is to be changed to.',
    example: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11) Firefox/97.0',
  })
  info: string;

  @IsOptional()
  @IsString()
  @JSONSchema({
    description: 'Token identifying the device for the messaging service.',
    example: 'json for push registration in firebase',
  })
  token: string;
}
