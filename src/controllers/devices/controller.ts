import {
  Authorized,
  Body,
  Delete,
  ForbiddenError,
  Get,
  JsonController,
  OnUndefined,
  Param,
  Patch,
  Post,
  Put,
  Req,
} from 'routing-controllers';
import { ServiceFactory } from '../../services/services-factory';

import { DevicesServiceInterface } from '../../services/devices-service';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { DeviceRequest, DeviceResponse, DeviceValuesRequest, GetDevicesResponse } from './dto';
import { StatusCodes, StatusCodeDescriptions } from '../../utils/status-codes';
import { DeviceStatus } from '../../models/device';

@JsonController('/devices')
export class DevicesController {
  devicesService: DevicesServiceInterface = ServiceFactory.getDevicesService();

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Creates a new user device as specified in the body param. For internal use.',
    description: 'Creates a new user device.',
    operationId: 'createDevice',
    //security: [{oauth2: []}], Not usable through swagger UI
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(DeviceResponse, { description: 'Created device' })
  @Post()
  createUserDevice(@Req() req, @Body() device: DeviceRequest): Promise<DeviceResponse> {
    return this.devicesService.createUserDevice(device, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: "List user's devices",
    description: "Lists all of the user's devices.",
    operationId: 'listDevices',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(GetDevicesResponse, {
    description: 'A list of user devices',
  })
  @Get()
  getUserDevices(@Req() req): Promise<GetDevicesResponse> {
    return this.devicesService.getUserDevices(req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    description: "Delete user's device with with provided device id.",
    operationId: 'deleteDevice',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.NoContent]: StatusCodeDescriptions[StatusCodes.NoContent],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Delete('/:deviceId')
  @OnUndefined(204)
  async deleteUserDeviceById(@Req() req, @Param('deviceId') deviceId: string): Promise<void> {
    await this.devicesService.deleteUserDeviceById(deviceId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Send test notification',
    description: "Sends a test notification to the user's device with the provided device id.",
    operationId: 'testDevice',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.NoContent]: StatusCodeDescriptions[StatusCodes.NoContent],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @Post('/:deviceId')
  @OnUndefined(204)
  tryBrowserPushNotification(@Req() req, @Param('deviceId') deviceId: string): Promise<void> {
    return this.devicesService.tryBrowserPushNotification(deviceId, req.authorizationBag);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Update device.',
    description: 'Updates the device with the provided device ID, using the provided name and info.',
    operationId: 'updateDevice',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(DeviceResponse, {
    description: 'The updated device.',
  })
  @Patch('/:deviceId')
  updateUserDeviceById(
    @Req() req,
    @Param('deviceId') deviceId: string,
    @Body() newDeviceValues: DeviceValuesRequest,
  ): Promise<DeviceResponse> {
    return this.devicesService.updateUserDeviceById(deviceId, req.authorizationBag, newDeviceValues);
  }

  // Backend-Internal access only, to be called by consumers if device is expired or dead.
  @OpenAPI({
    summary: 'Update device Status. Service internal use only.',
    description: 'Updates the device status with the provided device ID, using the provided status value.',
    operationId: 'updateDeviceStatus',
    // security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(DeviceResponse, {
    description: 'The updated device.',
  })
  @Put('/unauthenticated/:deviceId/status/:status')
  updateUserDeviceStatusById(
    @Param('deviceId') deviceId: string,
    @Param('status') status: DeviceStatus,
  ): Promise<DeviceResponse> {
    if (!process.env.EXPOSE_UNAUTHENTICATED_ROUTES || process.env.EXPOSE_UNAUTHENTICATED_ROUTES === 'False') {
      throw new ForbiddenError('Unauthenticated route is not enabled');
    }
    return this.devicesService.updateUserDeviceStatusById(deviceId, null, status);
  }
}
