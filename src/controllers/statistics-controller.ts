import {
  JsonController,
  Req,
  Get,
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { StatisticsServiceInterface } from "../services/statistics-service";

@JsonController("/statistics")
export class ServiceStatusController {
  statisticsService: StatisticsServiceInterface = ServiceFactory.getStatisticsService();

  @Get()
  async getStatistics(@Req() req) {
    return this.statisticsService.getStatistics();
  }
}
