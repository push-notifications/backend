import { IsDateString, IsNotEmpty, IsOptional, IsString, IsUUID, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { JSONSchema } from 'class-validator-jsonschema';
import { ChannelRecommendation } from '../../models/channel-recommendation';
import { v4 } from 'uuid';

@JSONSchema({
  description: 'Channel Recommendation JSON response object.',
  example: {
    id: v4(),
    userId: v4(),
    channelId: v4(),
    channelName: 'Channel Name',
    channelDescription: 'Channel Description',
    source: 'ML Model A',
    ignoredAt: new Date(),
    subscribedAt: new Date(),
    creationDate: new Date(),
  },
})
export class ChannelRecommendationResponse {
  @IsUUID()
  @IsNotEmpty()
  @JSONSchema({
    description: 'Id of the channel recommendation object.',
    example: v4(),
  })
  id: string;

  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'User ID of the channel recommendation',
    example: v4(),
  })
  userId: string;

  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel ID of the channel recommendation',
    example: v4(),
  })
  channelId: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Unique String Identifier of the recommendation.',
    example: 'ML Model A',
  })
  channelName: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Unique String Identifier of the recommendation.',
    example: 'ML Model A',
  })
  channelDescription: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Unique String Identifier of the recommendation.',
    example: 'ML Model A',
  })
  source: string;

  @IsDateString()
  @IsOptional()
  @JSONSchema({
    description: 'Date when the recommendation was ignored',
    example: new Date(),
    default: null,
  })
  ignoredAt: Date;

  @IsDateString()
  @IsOptional()
  @JSONSchema({
    description: 'Date when the recommendation was accepted',
    example: new Date(),
    default: null,
  })
  subscribedAt: Date;

  @IsDateString()
  @IsOptional()
  @JSONSchema({
    description: 'Date when the recommendation was created',
    example: new Date(),
    default: null,
  })
  creationDate: Date;

  constructor(channelRecommendation: ChannelRecommendation) {
    this.id = channelRecommendation.id;
    this.source = channelRecommendation.source;
    this.channelId = channelRecommendation.channel.id;
    this.channelName = channelRecommendation.channel.name;
    this.channelDescription = channelRecommendation.channel.description;
    this.userId = channelRecommendation.user.id;
    this.ignoredAt = channelRecommendation.ignoredAt;
    this.subscribedAt = channelRecommendation.subscribedAt;
  }
}

@JSONSchema({
  description: 'Json response with list of channel recommendations.',
  example: {
    channelRecommendations: [
      new ChannelRecommendationResponse(
        new ChannelRecommendation({
          id: v4(),
          source: 'ML Model A',
          ignoredAt: null,
          subscribedAt: new Date(),
          channel: { id: v4(), name: 'Channel Name', description: 'Channel Description' },
          user: { id: v4() },
        }),
      ),
      new ChannelRecommendationResponse(
        new ChannelRecommendation({
          id: v4(),
          source: 'ML Model A',
          ignoredAt: new Date(),
          subscribedAt: null,
          channel: { id: v4(), name: 'Channel Name', description: 'Channel Description' },
          user: { id: v4() },
        }),
      ),
    ],
  },
})
export class GetChannelRecommendationsResponse {
  @ValidateNested({ each: true })
  @Type(() => ChannelRecommendationResponse)
  @JSONSchema({
    description: 'Channel Recommendations list ',
    example: [
      new ChannelRecommendationResponse(
        new ChannelRecommendation({
          id: v4(),
          source: 'ML Model A',
          ignoredAt: null,
          subscribedAt: new Date(),
          creationDate: new Date(),
          channel: { id: v4(), name: 'Channel Name', description: 'Channel Description' },
          user: { id: v4() },
        }),
      ),
      new ChannelRecommendationResponse(
        new ChannelRecommendation({
          id: v4(),
          source: 'ML Model A',
          ignoredAt: new Date(),
          subscribedAt: null,
          creationDate: null,
          channel: { id: v4(), name: 'Channel Name', description: 'Channel Description' },
          user: { id: v4() },
        }),
      ),
    ],
  })
  channelRecommendations: ChannelRecommendationResponse[];

  constructor(list: ChannelRecommendationResponse[]) {
    this.channelRecommendations = list;
  }
}

@JSONSchema({
  description: 'New Channel Recommendation JSON Request object.',
  example: {
    source: 'ML Model A',
    userId: v4(),
    channelId: v4(),
  },
})
export class ChannelRecommendationRequest {
  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'User ID of the channel recommendation',
    example: v4(),
  })
  userId: string;

  @IsNotEmpty()
  @IsUUID('4')
  @JSONSchema({
    description: 'Channel ID of the channel recommendation',
    example: v4(),
  })
  channelId: string;

  @IsNotEmpty()
  @IsString()
  @JSONSchema({
    description: 'Unique String Identifier of the recommendation.',
    example: 'ML Model A',
  })
  source: string;
}

@JSONSchema({
  description: 'Channel Recommendation Patch Request object.',
  example: {
    source: 'ML Model A',
    ignoredAt: null,
    subscribedAt: new Date(),
  },
})
export class ChannelRecommendationAttributesRequest {
  @IsDateString()
  @IsOptional()
  @JSONSchema({
    description: 'Date when the recommendation was ignored',
    example: new Date(),
    default: null,
  })
  ignoredAt: Date;

  @IsDateString()
  @IsOptional()
  @JSONSchema({
    description: 'Date when the recommendation was accepted',
    example: new Date(),
    default: null,
  })
  subscribedAt: Date;
}
