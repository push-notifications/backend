import { Get, Post, JsonController, Authorized, Req, Param, Body, Patch } from 'routing-controllers';
import {
  ChannelRecommendationAttributesRequest,
  ChannelRecommendationRequest,
  ChannelRecommendationResponse,
  GetChannelRecommendationsResponse,
} from './dto';
import { OpenAPI, ResponseSchema } from 'routing-controllers-openapi';
import { StatusCodeDescriptions, StatusCodes } from '../../utils/status-codes';
import { ServiceFactory } from '../../services/services-factory';
import { ChannelRecommendationServiceInterface } from '../../services/channel-recommendation-service';
import { AuthorizedRequest, RECOMMENDER_SYSTEM_ROLE } from '../../middleware/authorizationChecker';
import { ChannelsService } from '../../services/channels-service';

@JsonController('/channel-recommendations')
export class ChannelRecommendationController {
  channelsService: ChannelsService = ServiceFactory.getChannelsService();
  channelRecommendationService: ChannelRecommendationServiceInterface =
    ServiceFactory.getChannelRecommendationService();

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Returns Channel Recommendations of the current User.',
    description:
      "This endpoint returns current user's channel recommendations that have not been ignored or subscribed to, if requester has access.",
    operationId: 'getAllChannelRecommendations',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
    },
  })
  @ResponseSchema(GetChannelRecommendationsResponse, { description: 'List of ChannelRecommendations.' })
  @Get()
  getAllChannelRecommendations(@Req() req: AuthorizedRequest): Promise<GetChannelRecommendationsResponse> {
    return this.channelRecommendationService.getChannelRecommendations(req.authorizationBag);
  }

  @Authorized([process.env.SUPPORTER_ROLE, RECOMMENDER_SYSTEM_ROLE])
  @OpenAPI({
    summary: 'Creates a new channel recommendation as specified in the body param.',
    description: 'Creates a new channel recommendation.',
    operationId: 'createChannelRecommendation',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelRecommendationResponse, { description: 'Created Channel Recommendation Object' })
  @Post()
  createChannelRecommendation(
    @Body() channelRecommendation: ChannelRecommendationRequest,
  ): Promise<ChannelRecommendationResponse> {
    return this.channelRecommendationService.createChannelRecommendation(channelRecommendation);
  }

  @Authorized([process.env.INTERNAL_ROLE, process.env.VIEWER_ROLE])
  @OpenAPI({
    summary: 'Updates ChannelRecommendation ignoredAt or subscribedAt attributes',
    description: 'Updates ChannelRecommendation ignoredAt or subscribedAt attributes',
    operationId: 'patchChannelRecommendation',
    security: [{ oauth2: [] }],
    responses: {
      [StatusCodes.OK]: StatusCodeDescriptions[StatusCodes.OK],
      [StatusCodes.BadRequest]: StatusCodeDescriptions[StatusCodes.BadRequest],
      [StatusCodes.NotFound]: StatusCodeDescriptions[StatusCodes.NotFound],
      [StatusCodes.Unauthorized]: StatusCodeDescriptions[StatusCodes.Unauthorized],
      [StatusCodes.Forbidden]: StatusCodeDescriptions[StatusCodes.Forbidden],
    },
  })
  @ResponseSchema(ChannelRecommendationResponse, { description: 'Channel Recommendation Object' })
  @Patch('/:id')
  async patchChannelRecommendation(
    @Param('id') channelRecommendationId: string,
    @Body() channelRecommendationAttributes: ChannelRecommendationAttributesRequest,
    @Req() req: AuthorizedRequest,
  ): Promise<ChannelRecommendationResponse> {
    const channelRecommendation = await this.channelRecommendationService.updateChannelRecommendation(
      channelRecommendationId,
      channelRecommendationAttributes,
      req.authorizationBag,
    );

    if (channelRecommendation.subscribedAt) {
      await this.channelsService.subscribeToChannel(channelRecommendation.channelId, req.authorizationBag);
    }

    return channelRecommendation;
  }
}
