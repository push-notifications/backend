import { JsonController, Authorized, Req, Get, Param } from 'routing-controllers';
import { ServiceFactory } from '../services/services-factory';
import { AuditServiceInterface } from '../services/audit-service';

@JsonController('/audit')
export class AuditController {
  auditService: AuditServiceInterface = ServiceFactory.getAuditService();

  @Authorized([process.env.SUPPORTER_ROLE])
  @Get('/notifications/:id')
  getAuditNotification(@Req() req, @Param('id') id: string) {
    return this.auditService.getAuditNotifications(id, req.authorizationBag);
  }

  @Authorized([process.env.SUPPORTER_ROLE])
  @Get('/channels/:id')
  getAuditChannel(@Req() req, @Param('id') id: string) {
    return this.auditService.getAuditChannels(id, req.authorizationBag);
  }

  @Authorized([process.env.SUPPORTER_ROLE])
  @Get('/security/:id')
  getAuditIPAdress(@Req() req, @Param('id') IPAddress: string) {
    return this.auditService.getAuditIPAdress(IPAddress, req.authorizationBag);
  }
}
