import { AfterLoad, Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn, RelationId } from 'typeorm';
import { User } from './user';
import { UserChannelCollection, UserChannelCollectionType } from './user-channel-collection';
import { AppDataSource } from '../app-data-source';

@Entity('UserSettings')
export class UserSettings {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(type => User, user => user.settings)
  @JoinColumn()
  user: User;

  @RelationId((userSettings: UserSettings) => userSettings.user)
  userId: string;

  @Column({ nullable: true })
  draft: string;

  // readonly
  favoriteList: string[];

  @AfterLoad()
  async getFavoriteList() {
    const result = await this.getCollectionIds(UserChannelCollectionType.FAVORITE);
    this.favoriteList = result?.map(r => r.id);
  }

  async getCollectionIds(type) {
    return await AppDataSource.createQueryBuilder(UserChannelCollection, 'collection')
      .leftJoinAndSelect('collection.channel', 'channel')
      .leftJoinAndSelect('collection.user', 'user')
      .where('collection.type = :type', { type: type })
      .andWhere('user.id = :userId', { userId: this.userId })
      .select('channel.id', 'id') // select id
      .getRawMany();
  }

  constructor(userSettings) {
    if (userSettings) {
      this.user = userSettings.user;
      this.draft = userSettings.draft;
      this.favoriteList = userSettings.favoriteList;
    }
  }
}
