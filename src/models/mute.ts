import { Entity, ManyToOne, PrimaryGeneratedColumn, Column, RelationId } from 'typeorm';
import { User } from './user';
import { Channel } from './channel';
import { BadRequestError } from 'routing-controllers';
import { EntityManager } from 'typeorm';
import { AESCypher } from '../middleware/aes-cipher';
import { Device } from './device';

@Entity('Mutes')
export class Mute {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => User, user => user.mutes)
  user: User;

  @RelationId((device: Device) => device.user)
  userId: string;

  @ManyToOne(type => Channel, { nullable: true })
  target: Channel;

  @Column({
    enum: ['PERMANENT', 'RANGE'],
  })
  type: string;

  // Mute range start
  @Column({ type: 'timestamptz', nullable: true })
  start: Date;

  // Mute range end
  @Column({ type: 'timestamptz', nullable: true })
  end: Date;

  constructor(mute) {
    if (mute) {
      this.id = mute.id;
      this.user = mute.user;
      this.target = mute.target;
      this.type = mute.type;
      this.start = mute.start;
      this.end = mute.end;
    }
  }

  async validateMute(transactionManager: EntityManager) {
    // If PERMANENT, clean start and end date if any
    if (this.type === 'PERMANENT') {
      this.start = null;
      this.end = null;
    }

    if (this.type === 'RANGE' && !this.end)
      throw new BadRequestError('Invalid Mute End Date: if mute range selected, end date must be set');

    if (this.start === this.end && this.start && this.end)
      throw new BadRequestError('Invalid Mute Date Range: start date and end date must be different');

    if (this.end && this.end < new Date()) {
      throw new BadRequestError("Invalid Mute End Date: end time can't be in the past");
    }

    if (this.start && this.end && this.start >= this.end) {
      throw new BadRequestError('Invalid Mute dates: start must be before end');
    }

    // Check is a PERMANENT Global mute already exists for this user
    if (this.type === 'PERMANENT' && this.target == null) {
      if (
        await transactionManager.findOne(Mute, {
          relations: ['target'],
          where: {
            user: {
              id: this.user,
            },
            type: 'PERMANENT',
            target: null,
          },
        })
      )
        throw new BadRequestError('A Global Permanent Mute already exists for this user.');
    }

    return true;
  }
}

export function decodeEmail(encryptedEmail: string) {
  if (!encryptedEmail) throw new BadRequestError('Invalid encrypted data');

  return AESCypher.decrypt(encryptedEmail);
}
