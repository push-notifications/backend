import * as memoize from 'memoizee';
import { postWithTimeout } from '../utils/fetch';

export class CernOauth {
  private static OAUTH_CLIENT_SECRET: string;
  private static OAUTH_REDIRECT_URI: string;
  private static OAUTH_CLIENT_ID: string;

  constructor() {
    CernOauth.OAUTH_CLIENT_SECRET = process.env.OAUTH_CLIENT_SECRET;
    CernOauth.OAUTH_REDIRECT_URI = process.env.OAUTH_REDIRECT_URI;
    CernOauth.OAUTH_CLIENT_ID = process.env.OAUTH_CLIENT_ID;
  }

  async getToken(code: string): Promise<any> {
    const params = new URLSearchParams(
      'code=' +
        code +
        '&grant_type=authorization_code&client_secret=' +
        CernOauth.OAUTH_CLIENT_SECRET +
        '&redirect_uri=' +
        CernOauth.OAUTH_REDIRECT_URI +
        '&client_id=' +
        CernOauth.OAUTH_CLIENT_ID,
    );

    return await postWithTimeout(process.env.OAUTH_TOKEN_URI, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: params,
    });
  }

  async refreshTokens(refreshToken: string): Promise<any> {
    const params = new URLSearchParams(
      'grant_type=refresh_token&client_secret=' +
        CernOauth.OAUTH_CLIENT_SECRET +
        '&client_id=' +
        CernOauth.OAUTH_CLIENT_ID +
        '&refresh_token=' +
        refreshToken,
    );
    return await postWithTimeout(process.env.OAUTH_TOKEN_URI, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: params,
    });
  }

  static async verifyToken_NoCache(token: string): Promise<boolean> {
    console.debug(`verifyToken miss cache for: ...${token.slice(-10)}`);
    const basicToken = Buffer.from(`${process.env.OAUTH_CLIENT_ID}:${process.env.OAUTH_CLIENT_SECRET}`).toString(
      'base64',
    );

    const response = await postWithTimeout(`${process.env.OAUTH_TOKEN_URI}/introspect`, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        authorization: `Basic ${basicToken}`,
      },
      body: new URLSearchParams(`token=${token}`),
    });
    console.debug(`verifyToken caching new entry: ...${token.slice(-10)}`);

    return response.active;
  }

  static memoizedVerifyToken = memoize(CernOauth.verifyToken_NoCache, {
    promise: true,
    maxAge: Number(process.env.CACHE_TOKEN_TTL || 5 * 60) * 1000, // means we might run with an expired token for up to 5 minutes
    preFetch: false, // Prefetch is useless if token is expired it will fail to verify
  });

  static async verifyToken(token: string): Promise<boolean> {
    return CernOauth.memoizedVerifyToken(token);
  }

  static async authenticateAsService(): Promise<string> {
    const basicToken = Buffer.from(`${process.env.OAUTH_CLIENT_ID}:${process.env.OAUTH_CLIENT_SECRET}`).toString(
      'base64',
    );

    const response = await postWithTimeout(process.env.OAUTH_TOKEN_URI, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        authorization: `Basic ${basicToken}`,
      },
      body: new URLSearchParams('grant_type=client_credentials'),
    });

    return response.access_token;
  }
}
