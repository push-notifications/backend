import {
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  OneToMany,
  JoinTable,
  BeforeInsert,
  BeforeUpdate,
  AfterLoad,
  RelationId,
} from 'typeorm';
import { User } from './user';
import { Channel } from './channel';
import { PriorityLevel } from './notification-enums';
import { Device } from './device';
import { BadRequestError } from 'routing-controllers';

const allowedScheduledTimes = ['09:00:00', '13:00:00', '17:00:00', '21:00:00'];

export enum ScheduledDay {
  Monday = 0,
  Tuesday = 1,
  Wednesday = 2,
  Thurday = 3,
  Friday = 4,
  Saturday = 5,
  Sunday = 6,
}

@Entity('Preferences')
export class Preference {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  name: string;

  @ManyToOne(type => User, user => user.preferences)
  user: User;

  @RelationId((device: Device) => device.user)
  userId: string;

  @ManyToOne(type => Channel, { nullable: true })
  target: Channel;

  @Column({
    enum: ['LIVE', 'DAILY', 'WEEKLY', 'MONTHLY'],
  })
  type: string;

  @ManyToMany(type => Device)
  // @JoinTable()
  @JoinTable({ name: 'preferences_devices__devices' })
  devices: Device[];

  @Column('simple-array', {
    nullable: true,
  })
  notificationPriority: PriorityLevel[];

  // Uppercase priorities values
  @BeforeUpdate()
  @BeforeInsert()
  async priorityToUpperCase(): Promise<void> {
    if (!this.notificationPriority) return;
    for (let i = 0; i < this.notificationPriority.length; i++)
      this.notificationPriority[i] = this.notificationPriority[i].toUpperCase() as PriorityLevel;
  }

  @Column({
    nullable: true,
    type: 'time without time zone',
  })
  rangeStart: string;

  @Column({
    nullable: true,
    type: 'time without time zone',
  })
  rangeEnd: string;

  @Column({
    nullable: true,
    type: 'time without time zone',
  })
  scheduledTime: string;

  @Column({
    enum: ScheduledDay,
    nullable: true,
  })
  scheduledDay: number;

  @BeforeInsert()
  @AfterLoad()
  async bcScheduledTime(): Promise<void> {
    if (this.type === 'DAILY' && !this.scheduledTime) this.scheduledTime = allowedScheduledTimes[0];
  }

  @ManyToMany(type => Channel, {
    cascade: true,
  })
  // @JoinTable()
  @JoinTable({ name: 'preferences_disabled_channels__channels' })
  disabledChannels: Channel[];

  @Column({
    enum: ['ALL', 'GENERAL', 'DIRECT'],
    default: 'ALL',
  })
  private: string;

  constructor(preference) {
    if (preference) {
      this.id = preference.id;
      this.name = preference.name;
      this.user = preference.user;
      this.target = preference.target;
      this.type = preference.type;
      this.notificationPriority = preference.notificationPriority;
      this.rangeStart = preference.rangeStart;
      this.rangeEnd = preference.rangeEnd;
      this.scheduledTime = preference.scheduledTime;
      this.scheduledDay = preference.scheduledDay;
      this.disabledChannels = preference.disabledChannels;
      this.devices = preference.devices;
      this.private = preference.private;
    }
  }

  validatePreference() {
    if (!this.name) {
      throw new BadRequestError("Invalid Name: preference name can't be empty");
    }

    if (this.notificationPriority.length === 0) {
      throw new BadRequestError('Invalid Priority: at least one Priority is required');
    }

    if (this.rangeStart === this.rangeEnd && this.rangeStart && this.rangeEnd) {
      throw new BadRequestError('Invalid Preference Time Range: start time and end time must be different');
    } else if ((this.rangeStart && !this.rangeEnd) || (this.rangeEnd && !this.rangeStart)) {
      throw new BadRequestError("Invalid Preference Time Range: start time and end time can't be empty");
    }

    if (
      (this.type === 'DAILY' || this.type === 'WEEKLY' || this.type === 'MONTHLY') &&
      !allowedScheduledTimes.includes(this.scheduledTime)
    ) {
      throw new BadRequestError('Invalid Scheduled Time: scheduled time can be 09:00, 13:00, 17:00 or 21:00');
    }

    if (
      (this.type === 'WEEKLY' || this.type === 'MONTHLY') &&
      !Object.values(ScheduledDay).includes(this.scheduledDay)
    ) {
      throw new BadRequestError('Invalid Scheduled Day: scheduled day can be 0, 1, 2, 3, 4, 5, 6');
    }

    if (this.devices.length === 0) {
      throw new BadRequestError('Invalid Target Device: at least one Target Device is required');
    }

    return true;
  }
}
