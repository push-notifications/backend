import * as memoize from 'memoizee';
import { User } from './user';
import { getWithTimeout, postWithTimeout } from '../utils/fetch';

export interface Group {
  groupId: string; // uuid
  groupIdentifier: string; //slug
}

export class CernAuthorizationService {
  static async getAuthTokenNoCache(): Promise<{ access_token: string; refresh_token: string }> {
    return postWithTimeout(process.env.AUTHORIZATION_SERVICE_API_TOKEN_URL, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: new URLSearchParams(
        `grant_type=client_credentials&client_id=${process.env.OAUTH_CLIENT_ID}&client_secret=${process.env.OAUTH_CLIENT_SECRET}&audience=${process.env.AUTHORIZATION_SERVICE_API_AUDIENCE}`,
      ),
    });
  }

  static memoizedGetAuthToken = memoize(CernAuthorizationService.getAuthTokenNoCache, {
    promise: true,
    maxAge: Number(5 * 60) * 1000,
    preFetch: true,
  });

  static async getAuthToken(): Promise<{ access_token: string; refresh_token: string }> {
    return CernAuthorizationService.memoizedGetAuthToken();
  }

  static async getShortURLAuthToken(): Promise<{ authorization: string }> {
    const shortUrlToken = await postWithTimeout(process.env.AUTHORIZATION_SERVICE_API_TOKEN_URL_SHORT_URL, {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: new URLSearchParams(
        `grant_type=client_credentials&client_id=${process.env.OAUTH_CLIENT_ID}&client_secret=${process.env.OAUTH_CLIENT_SECRET_SHORT_URL}&audience=${process.env.SHORT_URL_AUDIENCE}`,
      ),
    });
    const authorization = {
      'Content-Type': 'application/json',
      authorization: `Bearer ${shortUrlToken['access_token']}`,
    };

    return authorization;
  }

  static async getHeaders() {
    const accessToken = (await CernAuthorizationService.getAuthToken())['access_token'];
    return {
      'Content-Type': 'application/json',
      authorization: `Bearer ${accessToken}`,
    };
  }

  static async getUser(target: string) {
    let endpointUri;
    if (target.indexOf('@') > 0)
      endpointUri = `${process.env.AUTHORIZATION_SERVICE_API_BASE_URL}/api/v1.0/Identity/by_email/${target}`;
    else endpointUri = `${process.env.AUTHORIZATION_SERVICE_API_BASE_URL}/api/v1.0/Identity/${target}`;

    // Specify required fields.
    // primaryAccountEmail must be in fields or it will be null because coming from a joined table
    endpointUri += '?field=primaryAccountEmail&field=upn';

    try {
      const user = (
        await getWithTimeout(endpointUri, {
          headers: await CernAuthorizationService.getHeaders(),
        })
      ).data;

      // Check if email is returned, if not we can't add this user
      if (user && user[0] && user[0].primaryAccountEmail) {
        return {
          upn: user[0].upn,
          mail: user[0].primaryAccountEmail,
        };
      }
      if (user && user.primaryAccountEmail) {
        return {
          upn: user.upn,
          mail: user.primaryAccountEmail,
        };
      }
    } catch (ex) {
      try {
        const message = JSON.parse(ex.error).message;
        if (!message.includes('Could not find Identity with ID'))
          console.error('Authz service getUser failed:', target, message);
      } catch (e) {
        console.error(ex.error);
      }
    }
    return undefined; //search was unsuccesful
  }

  static async getGroupIdNoCache(groupIdentifier: string) {
    console.debug(`getGroupId miss cache for: ${groupIdentifier}`);
    const groups = (
      await getWithTimeout(
        `${process.env.AUTHORIZATION_SERVICE_API_BASE_URL}/api/v1.0/Group/?filter=groupIdentifier:${groupIdentifier}&field=groupIdentifier&field=id`,
        { headers: await CernAuthorizationService.getHeaders() },
      )
    ).data;
    console.debug(`getGroupId caching new entry: ${groupIdentifier}`);

    return groups.length === 1 ? groups[0].id : undefined;
  }

  static memoizedGetGroupId = memoize(CernAuthorizationService.getGroupIdNoCache, {
    promise: true,
    maxAge: Number(process.env.CACHE_GRAPPA_TTL || 5 * 60) * 1000,
    preFetch: true,
  });

  static async getGroupId(groupIdentifier: string) {
    return CernAuthorizationService.memoizedGetGroupId(groupIdentifier);
  }

  // GetMembers from Group
  static async _getMembersNoCache(groupIdentifier: string): Promise<any[]> {
    //    let res = [];
    console.debug(`getMembers miss cache: ${groupIdentifier}`);
    // Using /precomputed endpoint for faster recursive resolution (uses a 15 minutes cached computed result)
    const res = await CernAuthorizationService._getMembersNoCachePaginated(
      `/api/v1.0/Group/${groupIdentifier}/memberidentities/precomputed?field=primaryAccountEmail&field=upn`,
    );
    console.debug(`getMembers caching results: ${groupIdentifier}`);
    // if res is undefined, make it an empty array so cache is effective and caller .map runs ok
    return res || [];
  }

  // Recursive paginated result handling. Takes absolutePath as parameter to align with next field value from response
  static async _getMembersNoCachePaginated(absolutePath: string): Promise<any[]> {
    let res = [];
    try {
      const grappadata = await getWithTimeout(
        // Using /precomputed endpoint for faster recursive resolution (uses a 15 minutes cached computed result)
        `${process.env.AUTHORIZATION_SERVICE_API_BASE_URL}${absolutePath}`,
        { headers: await CernAuthorizationService.getHeaders() },
      );

      res = grappadata.data.map(
        u =>
          new User({
            username: u.upn,
            email: u.primaryAccountEmail,
          }),
      );
      // Recurse to next page
      if (grappadata.pagination?.next) {
        console.debug('getMembersNoCachePaginated next offset', grappadata.pagination.offset);
        res = [...res, ...(await CernAuthorizationService._getMembersNoCachePaginated(grappadata.pagination.next))];
      }
    } catch (grappaEx) {
      console.error('Error getMembers from GRAPPA', grappaEx.message);
    }
    // if res is undefined, make it an empty array so cache is effective and caller .map runs ok
    return res || [];
  }

  // maxAge in ms
  static _memoizedGetMembers = memoize(CernAuthorizationService._getMembersNoCache, {
    promise: true,
    maxAge: Number(process.env.CACHE_GRAPPA_TTL || 20 * 60) * 1000,
    preFetch: true,
  });

  static async getMembers(groupIdentifier: string) {
    return CernAuthorizationService._memoizedGetMembers(groupIdentifier);
  }

  // getCurrentUserGroups for current User
  static async _getCurrentUserGroupsNoCache(userName: string): Promise<Group[]> {
    if (!userName) return [];
    console.debug(`getCurrentUserGroups miss cache: ${userName}`);
    // Using /precomputed endpoint for faster recursive resolution (uses a 15 minutes cached computed result)
    const res = await CernAuthorizationService._getCurrentUserGroupsNoCachePaginated(
      // `/api/v1.0/Identity/${userName}/groups?field=groupIdentifier&field=id&recursive=true`,
      `/api/v1.0/IdentityMembership/${userName}/precomputed?field=groupIdentifier&field=id`,
    );
    console.debug(`getCurrentUserGroups caching results: ${userName}`);
    // if res is undefined, make it an empty array so cache is effective and caller .map runs ok
    return res || [];
  }

  // Recursive paginated result handling. Takes absolutePath as parameter to align with next field value from response
  static async _getCurrentUserGroupsNoCachePaginated(nextUri: string): Promise<Group[]> {
    let res = [];
    try {
      const grappadata = await getWithTimeout(
        // Using /precomputed endpoint for faster recursive resolution (uses a 15 minutes cached computed result)
        `${process.env.AUTHORIZATION_SERVICE_API_BASE_URL}${nextUri}`,
        { headers: await CernAuthorizationService.getHeaders() },
      );

      res = grappadata.data;
      // Recurse to next page
      if (grappadata.pagination?.next) {
        console.debug('getCurrentUserGroupsNoCachePaginated next offset', grappadata.pagination.offset);
        res = res.concat(
          await CernAuthorizationService._getCurrentUserGroupsNoCachePaginated(grappadata.pagination.next),
        );
      }
    } catch (grappaEx) {
      console.error('Error getCurrentUserGroups from GRAPPA', grappaEx.message);
    }
    // if res is undefined, make it an empty array so cache is effective and caller .map runs ok
    return res || [];
  }

  // maxAge in ms
  static _memoizedGetCurrentUserGroups = memoize(CernAuthorizationService._getCurrentUserGroupsNoCache, {
    promise: true,
    maxAge: Number(process.env.CACHE_USERGROUPS_TTL || 20 * 60) * 1000,
    preFetch: true,
  });

  static async getCurrentUserGroups(userName: string): Promise<Group[]> {
    return CernAuthorizationService._memoizedGetCurrentUserGroups(userName);
  }
}
