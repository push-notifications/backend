import { Entity, PrimaryGeneratedColumn, Column, Index, ManyToOne } from 'typeorm';
import { User } from './user';
import { Channel } from './channel';

@Entity('ChannelRecommendation')
export class ChannelRecommendation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => User, user => user.recommendations)
  user: User;

  @ManyToOne(type => Channel, { nullable: false })
  channel: Channel;

  @Column({ nullable: false })
  @Index()
  source: string;

  @Column({ nullable: true })
  ignoredAt: Date;

  @Column({ nullable: true })
  subscribedAt: Date;

  @Column({
    default: () => 'CURRENT_TIMESTAMP',
  })
  creationDate: Date;

  constructor(channelRecommendation) {
    if (channelRecommendation) {
      this.source = channelRecommendation.source;
      this.user = channelRecommendation.user;
      this.channel = channelRecommendation.channel;
      this.ignoredAt = channelRecommendation.ignoredAt;
      this.subscribedAt = channelRecommendation.subscribedAt;
      this.creationDate = channelRecommendation.creationDate;
    }
  }
}
