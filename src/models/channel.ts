import {
  BeforeInsert,
  BeforeUpdate,
  Brackets,
  Column,
  DeleteDateColumn,
  Entity,
  EntityManager,
  EntitySubscriberInterface,
  EventSubscriber,
  In,
  Index,
  JoinTable,
  LoadEvent,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  Raw,
  RelationId,
} from 'typeorm';
import { IsEmail, IsOptional, IsUrl, Validate } from 'class-validator';
import { User } from './user';
import { Tag } from './tag';
import { Category } from './category';
import { Notification } from './notification';
import { Group } from './group';
import { AlphaNumericLowercase, AlphaNumericPunctuationChannelName, FromEmailSet } from './channel-validators';
import { AuthorizationBag } from './authorization-bag';
import {
  SubmissionByEmail,
  SubmissionByForm,
  SubscriptionPolicy,
  Visibility,
  ChannelFlags,
  ChannelType,
} from './channel-enums';
import { ChannelHelpers } from './channel-helpers';
import { Type } from 'class-transformer';
import { ApiKeyObject } from './api-key-object';
import { APIKeyTypeEnum } from '../middleware/api-key';
import { SelectQueryBuilder } from 'typeorm/query-builder/SelectQueryBuilder';
import { UserChannelCollection, UserChannelCollectionType } from './user-channel-collection';
import { Group as AuthGroup } from './cern-authorization-service';
import { ForbiddenError } from 'routing-controllers';
import { createShortURL } from '../services/impl/channels/manage-short-url';
import { UpdateResult } from 'typeorm/query-builder/result/UpdateResult';

@Entity('Channels')
@Index(['slug', 'deleteDate'], { unique: true })
export class Channel extends ApiKeyObject {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Validate(AlphaNumericLowercase)
  @Index({ unique: true })
  @Column()
  slug: string;

  @IsOptional()
  @IsUrl()
  @Index({ unique: true })
  @Column({ nullable: true })
  shortUrl: string;

  @ManyToOne(type => User, user => user.ownedChannels)
  owner: User;

  @RelationId((channel: Channel) => channel.owner)
  ownerId: string;

  @Validate(AlphaNumericPunctuationChannelName)
  @Index({ unique: true })
  @Column()
  name: string;

  @Column()
  description: string;

  @ManyToMany(type => User, user => user.subscriptions, {
    cascade: true,
  })
  //  @JoinTable()
  @JoinTable({ name: 'channels_members__users' })
  @Type(() => User)
  members: Promise<User[]>;

  @IsOptional()
  @ManyToOne(type => Group, { cascade: true })
  adminGroup: Group;

  @ManyToMany(type => User, user => user.unsubscribed, {
    cascade: true,
  })
  @JoinTable({ name: 'channels_unsubscribed__users' })
  @Type(() => User)
  unsubscribed: User[];

  @ManyToMany(type => Group, group => group.channelGroups, {
    cascade: true,
  })
  @JoinTable({ name: 'channels_groups__groups' })
  @Type(() => Group)
  groups: Promise<Group[]>;

  @OneToMany(type => Notification, notification => notification.target, {
    cascade: true,
    onDelete: 'CASCADE',
  })
  @Type(() => Notification)
  notifications: Notification[];

  @Column({ enum: Visibility, default: Visibility.restricted })
  visibility: Visibility;

  @Column({
    enum: SubscriptionPolicy,
    default: SubscriptionPolicy.selfSubscription,
  })
  subscriptionPolicy: SubscriptionPolicy;

  @Column({ default: false })
  archive: boolean;

  @Column({
    default: () => 'CURRENT_TIMESTAMP',
  })
  creationDate: Date;

  @Column({
    default: () => 'CURRENT_TIMESTAMP',
  })
  lastActivityDate: Date;

  @BeforeUpdate()
  @BeforeInsert()
  async updateLastActivityDate(): Promise<void> {
    this.lastActivityDate = new Date();
    return;
  }

  @IsOptional()
  @IsEmail()
  @Column({ nullable: true })
  incomingEmail: string;

  @IsOptional()
  @IsEmail()
  @Column({ nullable: true })
  incomingEgroup: string;

  @Validate(FromEmailSet)
  @Column({
    type: 'enum',
    enum: SubmissionByEmail,
    array: true,
    nullable: false,
    default: [],
  })
  //@Type(() => enum)
  submissionByEmail: SubmissionByEmail[];

  @Column({
    type: 'enum',
    enum: SubmissionByForm,
    array: true,
    nullable: false,
    default: [SubmissionByForm.administrators],
  })
  //@Type(() => SubmissionByForm)
  submissionByForm: SubmissionByForm[];

  @DeleteDateColumn()
  deleteDate: Date;

  @IsOptional()
  @ManyToOne(type => Category, { nullable: true })
  category: Category;

  @ManyToMany(type => Tag)
  // @JoinTable()
  @JoinTable({ name: 'channels_tags__tags' })
  tags: Tag[];

  notificationCount: number;

  @Column({ default: false })
  sendPrivate: boolean;

  @Column({
    type: 'enum',
    enum: ChannelFlags,
    array: true,
    nullable: false,
    default: [],
  })
  channelFlags: ChannelFlags[];

  @Column({ enum: ChannelType, default: ChannelType.personal })
  channelType: ChannelType;

  constructor(channel) {
    super();
    if (channel) {
      this.id = channel.id;
      this.slug = channel.slug;
      this.shortUrl = channel.shortUrl;
      this.name = channel.name;
      this.description = channel.description;
      this.category = channel.category;
      this.tags = channel.tags || [];
      this.owner = channel.owner;
      this.members = channel.members;
      this.unsubscribed = channel.unsubscribed || [];
      this.groups = channel.groups || [];
      this.adminGroup = channel.adminGroup;
      this.visibility = channel.visibility;
      if (channel.visibility === Visibility.restricted) this.subscriptionPolicy = SubscriptionPolicy.dynamic;
      else this.subscriptionPolicy = channel.subscriptionPolicy;
      this.archive = channel.archive;
      this.APIKey = channel.APIKey;
      this.incomingEmail = channel.incomingEmail;
      this.incomingEgroup = channel.incomingEgroup;
      this.submissionByEmail = channel.submissionByEmail;
      this.submissionByForm = channel.submissionByForm;
      this.sendPrivate = channel.sendPrivate;
      this.channelFlags = channel.channelFlags;
      this.channelType = channel.channelType;
    }
  }

  static MAX_DESCRIPTION_SIZE = 10000;
  static MAX_NAME_SIZE = 128;
  static MIN_NAME_SIZE = 4;

  // Checks if user has admin access to this channel
  // Or if current user has superadmin role (e.g. notifications-service-supporters)
  // Needs joined columns to be loaded:
  //    relations: ["owner", "adminGroup"],
  async isAdmin(authorizationBag: AuthorizationBag) {
    // anonymous means no access.
    if (!authorizationBag || authorizationBag.isAnonymous) return false;

    // Roles defined and match SUPERUSER_ROLE means access.
    if (authorizationBag.isSupporter) {
      console.debug('WARNING: SUPPORTER isAdmin bypass by user %s', authorizationBag.userId);
      return true;
    }

    // User is owner or member of adminGroup means access.
    // First check owner to avoid group expansion is possible
    return (
      (this.adminGroup && (await this.adminGroup.isMember(authorizationBag.user))) ||
      this.owner.id === authorizationBag.userId
    );
  }

  // Checks if user has admin access to this channel
  // Or if current user has superadmin role (e.g. notifications-service-supporters)
  // Needs joined columns to be loaded:
  //    relations: ["owner", "adminGroup"],
  async isAdminWithCache(authorizationBag: AuthorizationBag, userGroups) {
    // anonymous means no access.
    if (!authorizationBag || authorizationBag.isAnonymous) return false;

    // Roles defined and match SUPERUSER_ROLE means access.
    if (authorizationBag.isSupporter) {
      console.debug('WARNING: SUPPORTER isAdmin bypass by user %s', authorizationBag.userId);
      return true;
    }

    if (this.owner.id === authorizationBag.userId) {
      return true;
    }

    if (this.adminGroup && ChannelHelpers.memoizedIsInAdminGroups(this.adminGroup, userGroups)) {
      return true;
    }

    return false;
  }

  // Checks if user has access to this channel content
  // Or if current user has superadmin role (e.g. notifications-service-supporters)
  // Needs joined columns to be loaded:
  //    relations: ["members", "groups", "owner", "adminGroup"],
  async hasAccess(transactionManager: EntityManager, authBag: AuthorizationBag, userGroups: any[]) {
    // public is accessible to all inc. anonymous
    if (this.visibility === Visibility.public) return true;
    // Anonymous is not allowed for the next possibilities
    if (!authBag || authBag.isAnonymous) return false;
    // internal is for CERN Users only (filter done via application-portal and internal role)
    if (this.visibility === Visibility.internal && authBag.isInternal) return true;
    // lightweight / guest account
    if (
      this.visibility === Visibility.internal &&
      authBag.isViewer &&
      !authBag.isInternal &&
      (await this.hasMemberAccess(transactionManager, authBag.userId, userGroups))
    )
      return true;
    // SUPERUSER: Username is member of supporter role SUPPORTER_ROLE, and is authorized to do all.
    if (authBag.isSupporter) {
      console.debug('WARNING: SUPPORTER hasAccess bypass by user %s', authBag.userId);
      return true;
    }
    // Or user is channel admin
    if (await this.hasAdminAccess(transactionManager, authBag, userGroups)) return true;
    // Or restricted mean you must be a subscribed member
    if (
      this.visibility === Visibility.restricted &&
      (await this.hasMemberAccess(transactionManager, authBag.userId, userGroups))
    )
      return true;

    // Or else you are not authorized
    return false;
  }

  // Checks if user has access to this channel content
  // Or if current user has superadmin role (e.g. notifications-service-supporters)
  // Needs joined columns to be loaded:
  //    relations: ["members", "groups", "owner", "adminGroup"],
  async hasAccessWithCache(authorizationBag: AuthorizationBag, userGroups) {
    // public is accessible to all inc. anonymous
    if (this.visibility === Visibility.public) return true;
    // Anonymous is not allowed for the next possibilities
    if (!authorizationBag || authorizationBag.isAnonymous) return false;
    // internal is for CERN Users only (filter done via application-portal and internal role)
    if (this.visibility === Visibility.internal && authorizationBag.isInternal) return true;
    // Or user is channel admin
    if (await this.isAdminWithCache(authorizationBag, userGroups)) return true;
    // Or restricted mean you must be a subscribed member
    if (this.visibility === Visibility.restricted && (await this.isMemberWithCache(authorizationBag.user, userGroups)))
      return true;

    // Or else you are not authorized
    return false;
  }

  // Checks if user has authorization to send Notification to the channel
  // via Form/API
  async canSendByForm(transactionManager: EntityManager, authorizationBag: AuthorizationBag, userGroups: any[]) {
    // No permission to send by Form set
    if (!this.submissionByForm) return false;

    // No authorizationBag means no send
    if (!authorizationBag) return false;

    // If user is an APIKey and sendByForm APIKey is allowed
    const hasApiKeyAccess = this.hasApiKeyAccess(authorizationBag);
    console.debug('hasApiKeyAccess', hasApiKeyAccess);
    if (hasApiKeyAccess) return true;

    // If user is channel admin
    if (this.submissionByForm.includes(SubmissionByForm.administrators)) {
      const hasAdminAccess = await this.hasAdminAccess(transactionManager, authorizationBag, userGroups);
      console.debug('hasAdminAccess', hasAdminAccess);
      if (hasAdminAccess) return true;
    }

    // If user is channel member
    if (this.submissionByForm.includes(SubmissionByForm.members)) {
      const hasMemberAccess = await this.isSubscribed(transactionManager, authorizationBag, userGroups);
      console.debug('hasMemberAccess', hasMemberAccess);
      if (hasMemberAccess) return true;
    }

    // Or else you are not authorized
    return false;
  }

  // Get QB to match with tags
  static qbMatchTags(tagIds: string[]): Brackets {
    return new Brackets(tagsQB => {
      tagsQB.where('tags.id In (:...tags)', { tags: tagIds });
    });
  }

  // Get QB to match with search category ids
  static qbMatchCategory(categoryIds: string[]): Brackets {
    return new Brackets(categoryQB => {
      categoryQB.where('channel.category.id In (:...categoryIds)', { categoryIds: categoryIds });
    });
  }

  // Get QB Brackets query search text match
  static qbSearchText(searchText: string): Brackets {
    return new Brackets(searchTextQB => {
      searchTextQB.where('channel.description ILIKE :searchText or channel.name ILIKE :searchText', {
        searchText: `%${searchText}%`,
      });
    });
  }

  static qbChannelsMemberViaUser(qb: SelectQueryBuilder<unknown>, userId: string, restricted = false): string {
    const subQuery = this.qbFilterChannelsByMember(qb, userId);
    if (restricted) {
      qb.andWhere('channel.visibility = :visibility', {
        visibility: Visibility.restricted,
      });
    }
    return 'channel.id IN ' + subQuery.getQuery();
  }

  static qbFilterChannelsByUserGroups(
    qb: SelectQueryBuilder<unknown>,
    userGroups: AuthGroup[],
  ): SelectQueryBuilder<Channel> {
    return qb
      .subQuery()
      .select('channel.id')
      .from(Channel, 'channel')
      .innerJoin('channel.groups', 'groups')
      .where('groups.id IN (:...userGroups)', {
        userGroups: userGroups.map(g => g.groupId),
      })
      .distinct();
  }

  static qbFilterChannelsByMember(qb: SelectQueryBuilder<unknown>, userId: string): SelectQueryBuilder<Channel> {
    return qb
      .subQuery()
      .select('channel.id')
      .from(Channel, 'channel')
      .leftJoin('channel.members', 'members')
      .where(':channelMemberId IN (members.id)', {
        channelMemberId: userId,
      })
      .distinct();
  }

  static qbFilterChannelsByUnsubscribed(qb: SelectQueryBuilder<unknown>, userId: string): SelectQueryBuilder<Channel> {
    return qb
      .subQuery()
      .select('channel.id')
      .from(Channel, 'channel')
      .leftJoin('channel.unsubscribed', 'unsubscribed')
      .where(':unsubscribedMemberId IN (unsubscribed.id)', {
        unsubscribedMemberId: userId,
      })
      .distinct();
  }

  static qbChannelsMemberViaGroup(
    qb: SelectQueryBuilder<unknown>,
    userGroups: AuthGroup[],
    restricted = false,
  ): string {
    const subQuery = this.qbFilterChannelsByUserGroups(qb, userGroups);
    if (restricted) {
      subQuery.andWhere('channel.visibility = :visibility', {
        visibility: Visibility.restricted,
      });
    }

    return 'channel.id IN ' + subQuery.getQuery();
  }

  static qbPublicInternalNotSubscribedUserChannels(userId: string, userGroups: AuthGroup[]): Brackets {
    return new Brackets(qb => {
      qb.where('channel.visibility In (:...visibleTypesPublicInternal)', {
        visibleTypesPublicInternal: [Visibility.public, Visibility.internal],
      });
      qb.andWhere(
        new Brackets(bqb => {
          bqb.andWhere(sqb => {
            const subQuery = this.qbFilterChannelsByMember(sqb, userId).getSql();
            return 'channel.id NOT IN ' + subQuery;
          });

          if (userGroups?.length > 0) {
            bqb.andWhere(sqb => {
              const subQuery = this.qbFilterChannelsByUserGroups(sqb, userGroups).getQuery();
              return 'channel.id NOT IN ' + subQuery;
            });
          }

          bqb.orWhere(sqb => {
            const subQuery = this.qbFilterChannelsByUnsubscribed(sqb, userId).getQuery();
            return 'channel.id IN ' + subQuery;
          });
        }),
      );
    });
  }

  static qbRestrictedNotSubscribedUserChannels(userId: string, userGroups: AuthGroup[]): Brackets {
    return new Brackets(qb => {
      qb.where('channel.visibility In (:...visibleTypeRestricted)', { visibleTypeRestricted: [Visibility.restricted] });

      qb.andWhere(sqb => {
        const subQuery = sqb
          .subQuery()
          .select('channel.id')
          .from(Channel, 'channel')
          .leftJoin('channel.unsubscribed', 'unsubscribed')
          .where(':unsubscribedUserId IN (unsubscribed.id)', {
            unsubscribedUserId: userId,
          })
          .getQuery();
        return 'channel.id IN ' + subQuery;
      });

      const userMemberOrGroupMember = new Brackets(bqb => {
        bqb.andWhere(sqb => {
          const subQuery = this.qbFilterChannelsByMember(sqb, userId).getSql();
          return 'channel.id IN ' + subQuery;
        });

        if (userGroups?.length > 0) {
          bqb.orWhere(sqb => {
            const subQuery = this.qbFilterChannelsByUserGroups(sqb, userGroups).getQuery();
            return 'channel.id IN ' + subQuery;
          });
        }
      });

      qb.andWhere(userMemberOrGroupMember);
    });
  }

  // Filter channels by QB according to the subscribed/owned/favorite toggle
  static filterChannels(
    userGroups: AuthGroup[],
    authBag: AuthorizationBag,
    isOwner: boolean,
    isSubscribed: boolean,
    isFavorite: boolean,
  ): Brackets {
    const userId = authBag.userId;
    const supporter = authBag.isSupporter;
    const viewer = authBag.isViewer;
    const internal = authBag.isInternal;
    return new Brackets(filterQB => {
      if (isFavorite) {
        filterQB.andWhere(sqb => {
          const subQuery = sqb
            .subQuery()
            .select('collection.channelId')
            .from(UserChannelCollection, 'collection')
            .where('collection.type = :type', {
              type: UserChannelCollectionType.FAVORITE,
            })
            .andWhere('collection.userId = :userId', {
              userId: userId,
            })
            .getQuery();
          return 'channel.id IN ' + subQuery;
        });

        return;
      }

      if (isOwner) {
        filterQB.andWhere(
          new Brackets(qb => {
            qb.where('owner.id = :userId', {
              userId: userId,
            });
            if (userGroups?.length > 0)
              qb.orWhere('adminGroup.id IN (:...userGroups)', {
                userGroups: userGroups.map(g => g.groupId),
              });
          }),
        );
        return;
      }

      if (isSubscribed) {
        filterQB.andWhere(sqb => {
          const subQuery = sqb
            .subQuery()
            .select('channel.id')
            .from(Channel, 'channel')
            .leftJoin('channel.unsubscribed', 'unsubscribed')
            .where(':userId IN (unsubscribed.id)', {
              userId: userId,
            })
            .getQuery();
          return 'channel.id NOT IN ' + subQuery;
        });

        filterQB.andWhere(
          new Brackets(qb => {
            qb.where(sqb => this.qbChannelsMemberViaUser(sqb, userId));
            if (userGroups?.length > 0) {
              qb.orWhere(sqb => this.qbChannelsMemberViaGroup(sqb, userGroups));
            }
          }),
        );

        return;
      }

      if (supporter) {
        return;
      }

      // restricted, owned channels
      filterQB.orWhere(
        new Brackets(qb => {
          qb.where('owner.id = :userId', {
            userId: userId,
          });
          if (userGroups?.length > 0)
            qb.orWhere('adminGroup.id IN (:...userGroups)', {
              userGroups: userGroups.map(g => g.groupId),
            });
        }),
      );

      // restricted, subscribed channels
      filterQB.orWhere(sqb => this.qbChannelsMemberViaUser(sqb, userId, true));

      if (userGroups?.length > 0) {
        filterQB.orWhere(sqb => this.qbChannelsMemberViaGroup(sqb, userGroups, true));
      }

      filterQB.orWhere('channel.visibility In (:...visibleTypes)', {
        visibleTypes: viewer && internal ? [Visibility.public, Visibility.internal] : [Visibility.public],
      });
    });
  }

  // Get QB Brackets channels by access: where user is Admin
  static qbUserIsAdmin(userId: string, userGroups: any[]): Brackets {
    return new Brackets(accessQB => {
      accessQB.where('owner.id = :userId', { userId: userId });
      if (userGroups?.length > 0) {
        accessQB.orWhere('adminGroup.id IN (:...userGroups)', {
          userGroups: userGroups.map(g => g.groupId),
        });
      }
    });
  }

  // Get QB Brackets channels by access: where user is Member
  static qbUserIsMember(userId: string, userGroups: any[]): Brackets {
    return new Brackets(accessQB => {
      accessQB.where(':userId IN (members.id)', {
        userId: userId,
      });
      if (userGroups?.length > 0) {
        accessQB.orWhere('groups.id IN (:...userGroups)', {
          userGroups: userGroups.map(g => g.groupId),
        });
      }
    });
  }

  // Filter channels by access: if user has authorization to send Notification to the channel via Form/API
  static qbFilterByCanSendByForm(
    qb: SelectQueryBuilder<unknown>,
    authorizationBag: AuthorizationBag,
    userGroups: any[],
    isTargetedNotification = false,
    channelId: string,
  ): void {
    // Filter by access
    qb.andWhere(
      // Administrators
      new Brackets(submissiontypeQB => {
        submissiontypeQB.where(
          new Brackets(adminQB => {
            adminQB.where(':administrators = ANY(channel.submissionByForm)', {
              administrators: SubmissionByForm.administrators,
            });
            adminQB.andWhere(
              new Brackets(adminGroupQB => {
                adminGroupQB.where('owner.id = :userId', { userId: authorizationBag.userId });
                if (userGroups?.length > 0) {
                  adminGroupQB.orWhere('adminGroup.id IN (:...userGroups)', {
                    userGroups: userGroups.map(g => g.groupId),
                  });
                }
              }),
            );
          }),
        );

        // Members
        // Targeted notifications are not allowed for members
        if (!isTargetedNotification) {
          submissiontypeQB.orWhere(
            new Brackets(memberQB => {
              memberQB.where(':members = ANY(channel.submissionByForm)', {
                members: SubmissionByForm.members,
              });
              memberQB.andWhere(
                new Brackets(userQB => {
                  userQB.where(':userId IN (members.id)', {
                    userId: authorizationBag.userId,
                  });
                  if (userGroups?.length > 0) {
                    userQB.orWhere('groups.id IN (:...userGroups)', {
                      userGroups: userGroups.map(g => g.groupId),
                    });
                  }
                }),
              );
            }),
          );
        }

        // If user is an APIKey and sendByForm APIKey is allowed
        if (
          authorizationBag.isApiKey &&
          authorizationBag.APIKey.type == APIKeyTypeEnum.Channel &&
          authorizationBag.APIKey.id == channelId
        ) {
          submissiontypeQB.orWhere(':apikey = ANY(channel.submissionByForm)', {
            apikey: SubmissionByForm.apikey,
          });
        }
      }),
    );
  }

  async canSendCritical(authorizationBag: AuthorizationBag) {
    if (!authorizationBag) return false;
    if (!this.channelFlags?.includes(ChannelFlags.critical)) return false;

    return false;
  }

  isUnsubscribed(user: User) {
    return this.unsubscribed.some(u => u.compareTo(user));
  }

  private async getMembers(subscribedOnly: boolean): Promise<User[]> {
    const channelMembers = await this.members;
    let members = channelMembers.map(
      u =>
        new User({
          id: u.id,
          username: u.username,
          email: u.email,
        }),
    );

    if (subscribedOnly && this.unsubscribed) {
      members = members.filter(u => !this.isUnsubscribed(u), this);
    }

    const channelGroups = await this.groups;
    for (const group of channelGroups) {
      let groupMembers = await group.getMembers();
      if (subscribedOnly && this.unsubscribed) {
        groupMembers = groupMembers.filter(u => !this.isUnsubscribed(u), this);
      }
      for (const member of groupMembers) {
        const isInMembers = members.some(u => u.compareTo(member));
        if (!isInMembers) {
          members.push(member);
        }
      }
    }
    return members;
  }

  async isMember(user: User): Promise<boolean> {
    if (!user) return false;
    if (user.username) {
      return (await this.getMembers(false)).map(member => member.username).includes(user.username);
    } else if (user.email) {
      return (await this.getMembers(false)).map(member => member.email).includes(user.email);
    }
    return false;
  }

  async isMemberWithCache(user: User, userGroups): Promise<boolean> {
    if (!user) return false;

    if (ChannelHelpers.memoizedIsInMembers(this.members, user)) {
      return true;
    }

    if (ChannelHelpers.memoizedIsInGroups(this.groups, userGroups)) {
      return true;
    }

    return false;
  }

  // --------- NEW TYPEORM ACCESS QUERIES -------------- //
  //
  // Most implementations use transactionManager.find instead of queryBuilder due to performance
  // limitations in queryBuilder and relations.
  // Guidelines:
  // - avoid selecting relation arrays without implementing pagination
  // - avoid using queryBuilder with multiple relation selects
  // - avoid returning array relations in transactionManager find methods
  // ---------------------------------------------------
  hasApiKeyAccess(authorizationBag: AuthorizationBag): boolean {
    return (
      this.submissionByForm.includes(SubmissionByForm.apikey) &&
      !!this.APIKey &&
      authorizationBag.isApiKey &&
      authorizationBag.APIKey.hasRights(APIKeyTypeEnum.Channel, this.id)
    );
  }

  async isUnsubscribedPerformant(transactionManager: EntityManager, userId: string): Promise<boolean> {
    const isUnsubscribed = await transactionManager.findOne(Channel, {
      relations: {
        unsubscribed: true,
      },
      where: {
        id: this.id,
        unsubscribed: {
          id: Raw(alias => `:userId IN (${alias})`, { userId: userId }),
        },
      },
      select: {
        id: true,
        members: {},
      },
    });

    return !!isUnsubscribed;
  }

  async isSubscribed(
    transactionManager: EntityManager,
    authorizationBag: AuthorizationBag,
    userGroups: any[],
  ): Promise<boolean> {
    const isUsubscribed = await this.isUnsubscribedPerformant(transactionManager, authorizationBag.userId);
    console.debug('isUnsubscribed', isUsubscribed);
    if (isUsubscribed) return false;

    return await this.hasMemberAccess(transactionManager, authorizationBag.userId, userGroups);
  }

  async hasMemberAccess(transactionManager: EntityManager, userId: string, userGroups: any[]): Promise<boolean> {
    // check is member
    const hasMemberAccessViaUser = await this.isMemberViaUser(transactionManager, userId);
    console.debug('hasMemberAccessViaUser', hasMemberAccessViaUser);
    if (hasMemberAccessViaUser) return true;

    const hasMemberAccessViaGroup = await this.isMemberViaGroup(transactionManager, userGroups);
    console.debug('hasMemberAccessViaGroup', hasMemberAccessViaGroup);
    return hasMemberAccessViaGroup;
  }

  async isMemberViaUser(transactionManager: EntityManager, userId: string): Promise<boolean> {
    const isMemberViaUser = await transactionManager.findOne(Channel, {
      relations: {
        members: true,
      },
      where: {
        id: this.id,
        members: {
          id: Raw(alias => `:userId IN (${alias})`, { userId: userId }),
        },
      },
      select: {
        id: true,
        members: {},
      },
    });

    return !!isMemberViaUser;
  }

  async isMemberViaGroup(transactionManager: EntityManager, userGroups: any[]): Promise<boolean> {
    const isMemberViaGroup = await transactionManager.findOne(Channel, {
      relations: {
        groups: true,
      },
      where: {
        id: this.id,
        groups: {
          id: In(userGroups.map(g => g.groupId)),
        },
      },
      select: {
        id: true,
        groups: {},
      },
    });

    return !!isMemberViaGroup;
  }

  async isGroupSubscribed(transactionManager: EntityManager, group: Group): Promise<boolean> {
    const isGroupSubscribed = await transactionManager.findOne(Channel, {
      relations: {
        groups: true,
      },
      where: {
        id: this.id,
        groups: {
          id: Raw(alias => `:groupId IN (${alias})`, { groupId: group.id }),
        },
      },
      select: {
        id: true,
        groups: {},
      },
    });

    return !!isGroupSubscribed;
  }

  async isUserSubscribed(transactionManager: EntityManager, user: User): Promise<boolean> {
    const isMemberSubscribed = await transactionManager.findOne(Channel, {
      relations: {
        members: true,
      },
      where: {
        id: this.id,
        members: {
          id: Raw(alias => `:userId IN (${alias})`, { userId: user.id }),
        },
      },
      select: {
        id: true,
        members: {},
      },
    });

    return !!isMemberSubscribed;
  }

  async isOwner(transactionManager: EntityManager, authorizationBag: AuthorizationBag): Promise<boolean> {
    const isOwner = await transactionManager.findOne(Channel, {
      relations: {
        owner: true,
      },
      where: {
        id: this.id,
        owner: {
          id: authorizationBag.userId,
        },
      },
    });

    return !!isOwner;
  }

  async isAdminViaGroups(
    transactionManager: EntityManager,
    authorizationBag: AuthorizationBag,
    userGroups: any[],
  ): Promise<boolean> {
    const isAdmin = await transactionManager.findOne(Channel, {
      relations: {
        adminGroup: true,
      },
      where: {
        id: this.id,
        adminGroup: {
          id: In(userGroups.map(g => g.groupId)),
        },
      },
    });

    return !!isAdmin;
  }

  async hasAdminAccess(txm: EntityManager, authBag: AuthorizationBag, userGroups: any[]): Promise<boolean> {
    // queries with optional relations and where by relation property, the join changes from "left" to "inner" incorrectly
    // https://github.com/typeorm/typeorm/issues/9395
    // const hasAdminAccess = await txm.findOne(Channel, {
    //   where: [
    //     {
    //       id: this.id,
    //       owner: {
    //         id: authBag.userId,
    //       }
    //     },
    //     {
    //       id: this.id,
    //       adminGroup: {
    //         id: In(userGroups.map(g => g.groupId)),
    //       },
    //     },
    //   ],
    // });

    return (await this.isOwner(txm, authBag)) || (await this.isAdminViaGroups(txm, authBag, userGroups));
  }

  async addGroup(transactionManager: EntityManager, group: Group): Promise<void> {
    await transactionManager.createQueryBuilder().relation(Channel, 'groups').of(this).add(group);
  }

  async removeGroup(transactionManager: EntityManager, group: Group): Promise<void> {
    await transactionManager.createQueryBuilder().relation(Channel, 'groups').of(this).remove(group);
  }

  async subscribeSelf(transactionManager: EntityManager, user: User, userGroups: any[]): Promise<void> {
    await transactionManager.createQueryBuilder().relation(Channel, 'unsubscribed').of(this).remove(user);
    if (await this.hasMemberAccess(transactionManager, user.id, userGroups)) {
      return;
    }

    await transactionManager.createQueryBuilder().relation(Channel, 'members').of(this).add(user);
  }

  async unsubscribeSelf(transactionManager: EntityManager, user: User, userGroups: any[]) {
    if (!(await this.hasMemberAccess(transactionManager, user.id, userGroups)))
      throw new ForbiddenError("You're not member of this channel");

    await transactionManager.createQueryBuilder().relation(Channel, 'unsubscribed').of(this).add(user);
  }

  async addUser(transactionManager: EntityManager, user: User): Promise<void> {
    await transactionManager.createQueryBuilder().relation(Channel, 'members').of(this).add(user);
  }

  async removeUser(transactionManager: EntityManager, user: User): Promise<void> {
    await transactionManager.createQueryBuilder().relation(Channel, 'members').of(this).remove(user);
  }
}

@EventSubscriber()
export class ChannelSubscriber implements EntitySubscriberInterface {
  /**
   * Indicates that this subscriber only listen to Post events.
   */
  listenTo(): typeof Channel {
    return Channel;
  }

  /**
   * Fill Short URL field: needed for initialization of existing channels.
   * Need for automatic retry/recovery in case of the short URL API being down or some failure happening.
   *
   * @param entity
   * @param event
   */
  async _fillShortURL(entity: Channel, event: LoadEvent<Channel>): Promise<UpdateResult> {
    if (!Object.prototype.hasOwnProperty.call(entity, 'shortUrl')) {
      console.debug(`Short Url: Channel obj not fully loaded : ${JSON.stringify(entity)} - skipping!`);
      return;
    }

    if (entity.shortUrl) {
      console.debug(`Short url for channel ${entity.name} already exists: ${entity.shortUrl} - skipping!`);
      return;
    }

    console.debug(`Generating short url for channel ${entity.name}!`);

    try {
      const shortUrl = await createShortURL(entity.id);
      await event.manager.update(Channel, { id: entity.id }, { shortUrl });
    } catch (e) {
      console.error(`Error creating the short URL for ${entity.name} channel on after load event`, e);
    }
  }

  /**
   * Called after entity is loaded.
   */
  async afterLoad(entity: Channel, event: LoadEvent<Channel>): Promise<void> {
    if (!entity.id) {
      console.debug(`Short Url: Could not find channel for : ${JSON.stringify(entity)} - skipping!`);
      return;
    }

    await this._fillShortURL(entity, event);
  }
}
