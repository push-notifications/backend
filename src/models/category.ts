import { Entity, PrimaryGeneratedColumn, Column, Index, Tree, TreeChildren, TreeParent, EntityManager } from 'typeorm';

@Entity('Categories')
@Tree('closure-table')
export class Category {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index()
  @Column({ unique: true, nullable: false })
  name: string;

  @Column({ nullable: true })
  info: string;

  @TreeChildren()
  children: Category[];

  @TreeParent()
  parent: Category;

  constructor(category) {
    if (category) {
      this.name = category.name;
      this.info = category.info;
    }
  }

  static async getDescendantIds(tm: EntityManager, categoryId: string): Promise<string[]> {
    const searchParamCategory = new Category({});
    searchParamCategory.id = categoryId;
    const descendants = await tm.getTreeRepository(Category).findDescendants(searchParamCategory);
    const descendantIds = descendants.map(cat => cat.id);
    return descendantIds;
  }
}
