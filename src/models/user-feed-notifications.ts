import { Entity, ManyToOne, Index, Column, PrimaryColumn } from 'typeorm';
import { User } from './user';
import { Notification } from './notification';

// Used in the notifications-routing and notifications-routing projects
// Specified here for prototype simplification
@Entity({ name: 'UserFeedNotifications' })
@Index(['deliveryTime'])
export class UserFeedNotification {
  @PrimaryColumn()
  userId: string;

  @ManyToOne(type => User)
  user: User;

  @PrimaryColumn()
  notificationId: string;

  @ManyToOne(type => Notification)
  notification: Notification;

  @PrimaryColumn({
    enum: ['DAILY', 'WEEKLY', 'MONTHLY'],
  })
  frequencyType: string;

  @Column({ type: 'timestamptz' })
  creationDatetime: Date;

  @Column({ type: 'time without time zone', nullable: true })
  deliveryTime: string;

  @Column({
    enum: [0, 1, 2, 3, 4, 5, 6],
    nullable: true,
  })
  deliveryDayOfTheWeek: number;
}
