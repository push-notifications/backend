import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToMany,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Channel } from './channel';
import { Preference } from './preference';
import { Mute } from './mute';
import { Device } from './device';
import { UserSettings } from './user-settings';
import { ChannelRecommendation } from './channel-recommendation';
import { Notification } from './notification';

@Entity({ name: 'Users' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  // unique login, but can be null
  @Index()
  @Column({ unique: true, nullable: true })
  username: string;

  @Column({ unique: true })
  email: string;

  @ManyToMany(type => Channel, channel => channel.members)
  subscriptions: Channel[];

  @ManyToMany(type => Channel, channel => channel.unsubscribed)
  unsubscribed: Channel[];

  @OneToMany(type => Channel, channel => channel.owner)
  ownedChannels: Channel[];

  @OneToMany(type => Preference, rule => rule.user)
  preferences: Preference[];

  @OneToMany(type => Mute, rule => rule.user)
  mutes: Mute[];

  @OneToMany(type => Device, rule => rule.user)
  devices: Device[];

  @Column() enabled: boolean;

  @ManyToMany(type => Notification, notification => notification.targetUsers)
  notificationsTargeting: Notification[];

  // User creation date (automatic fill)
  @CreateDateColumn({ type: 'timestamptz' })
  created: Date;

  // User lastlogin date
  @Column({ type: 'timestamptz', nullable: true })
  lastLogin: Date;

  @OneToOne(type => UserSettings, rule => rule.user)
  settings: UserSettings;

  @OneToMany(type => ChannelRecommendation, recommendation => recommendation.user)
  recommendations: ChannelRecommendation[];

  /**
   *
   * @param user With the following information:
   * @param username  Username of the user
   * @param email Email of the user
   */
  constructor(user: any) {
    if (user) {
      this.id = user.id;
      this.username = user.username?.toLowerCase().trim();
      this.email = user.email?.toLowerCase().trim();
      this.enabled = true;
    }
  }

  // User Comparer.
  // Compares IDs if they are DB objects, otherwise compare username AND email for cases when one is undefined
  compareTo(user: User): boolean {
    if (!user) return false;

    if (this.id && user.id) return this.id === user.id;

    return this.username === user.username && this.email === user.email;
  }
}

export class AdUser extends User {
  phone: string;

  constructor(user: any) {
    super(user);
    this.phone = user.mobile;
  }
}
