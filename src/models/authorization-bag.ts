import { User } from '../models/user';
import { APIKey } from '../middleware/api-key';

export class AuthorizationBag {
  userId: string = null;
  userName: string = null;
  email: string = null;
  ip: string = null;
  isAnonymous = true;
  // internal (Data classification) = CERN Users + Edugain (maybe also HEP Trusted)
  isInternal = false;
  // viewer = authenticated users
  isViewer = false;
  // supporters
  isSupporter = false;
  readonly APIKey: APIKey = null;

  get isApiKey(): boolean {
    return !!this.APIKey;
  }

  constructor(ip: string, userId: string, userName: string, roles: string[] = []) {
    this.ip = ip;

    // Anonymous
    if (!userId) {
      this.isAnonymous = true;
      return;
    }
    // Authenticated
    this.userId = userId;
    this.userName = userName;
    this.isAnonymous = false;
    // Checking Internal role
    if (roles.includes(process.env.INTERNAL_ROLE)) {
      //console.debug("AuthorizationBag: role %s accepted for user %s", process.env.INTERNAL_ROLE, userId);
      this.isInternal = true;
    }
    // Checking Viewer role
    if (roles.includes(process.env.VIEWER_ROLE)) {
      //console.debug("AuthorizationBag: role %s accepted for user %s", process.env.VIEWER_ROLE, userId);
      this.isViewer = true;
    }
    // Checking SuperUser role
    if (roles.includes(process.env.SUPPORTER_ROLE)) {
      //console.debug("AuthorizationBag: role %s accepted for user %s", process.env.SUPPORTER_ROLE, userId);
      this.isSupporter = true;
    }
  }

  // Returns current user as User object
  get user(): User {
    if (!this.userId) return null;
    return new User({
      id: this.userId,
      username: this.userName,
      email: this.email,
    });
  }
}
