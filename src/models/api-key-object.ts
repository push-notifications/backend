import { Column, PrimaryGeneratedColumn } from 'typeorm';
import { HexBase64BinaryEncoding } from 'crypto';

const crypto = require('crypto');

export abstract class ApiKeyObject {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  //this is a hash of the API key and the salt
  @Column({ nullable: true })
  APIKey: string;

  @Column({ nullable: true })
  salt: HexBase64BinaryEncoding;

  generateAPIkey(): string {
    const APIuuid = crypto.randomUUID(); //v4 UUID (random, not time based)
    const salt = crypto.randomBytes(128);

    //hash salt and APIuuid together
    const hash = crypto.pbkdf2Sync(APIuuid, salt, 10000, 64, 'sha512');

    this.APIKey = hash.toString('base64');
    this.salt = salt.toString('base64');
    return APIuuid;
  }

  verifyKey(id: string, key: string): boolean {
    const hash = crypto.pbkdf2Sync(key, Buffer.from(this.salt, 'base64'), 10000, 64, 'sha512');

    return hash.toString('base64') == this.APIKey;
  }
}
