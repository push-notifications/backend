import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  ManyToMany,
  JoinTable,
  Index,
  Brackets,
  DeleteDateColumn,
  RelationId,
} from 'typeorm';
import { Channel } from './channel';
import { User } from './user';
import { Group } from './group';
import { AuthorizationBag } from './authorization-bag';
import { Source, PriorityLevel } from './notification-enums';
import { Group as AuthGroup } from './cern-authorization-service';
import { Type } from 'class-transformer';
import { Device } from './device';

@Entity({ name: 'Notifications' })
export class Notification {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  body: string;

  @Column({ nullable: true })
  @Index()
  sendAt: Date;

  @Column({ nullable: true })
  @Index()
  sentAt: Date;

  @ManyToOne(type => Channel, channel => channel.notifications)
  target: Channel;

  @RelationId((notification: Notification) => notification.target)
  targetId: string;

  @Column({ nullable: true }) link: string;
  @Column({ nullable: true }) summary: string;
  @Column({ nullable: true }) imgUrl: string;
  @Column({ nullable: true }) sender: string;
  @Column({ nullable: true }) contentType: string;
  @Column({ enum: PriorityLevel, default: PriorityLevel.NORMAL })
  priority: PriorityLevel;

  @Column({ enum: Source, default: Source.api })
  source: Source;

  @Column({ default: false })
  private: boolean;

  @Column({ default: false })
  intersection: boolean;

  @DeleteDateColumn()
  deleteDate: Date;

  @ManyToMany(type => User, user => user.notificationsTargeting, {
    cascade: true,
  })
  @JoinTable({ name: 'notifications_target_users__users' })
  @Type(() => User)
  targetUsers: User[];

  @ManyToMany(type => Group, group => group.notificationsTargeting, {
    cascade: true,
  })
  @JoinTable({ name: 'notifications_target_groups__groups' })
  @Type(() => Group)
  targetGroups: Group[];

  constructor(notification: any) {
    if (notification) {
      this.id = notification.id;
      this.target = notification.target;
      this.sender = notification.sender;
      this.body = notification.body;
      this.sendAt = notification.sendAt;
      this.sentAt = notification.sendAt ? null : new Date();
      this.link = notification.link;
      this.summary = notification.summary;
      this.imgUrl = notification.imgUrl;
      this.priority = notification.priority;
      this.source = notification.source;
      this.private = notification.private || false;
      this.intersection = notification.intersection || false;
      this.targetUsers = notification.targetUsers || [];
      this.targetGroups = notification.targetGroups || [];
    }
  }

  // Returns true if current user is allowed to see this notification
  async isUserTargeted(authorizationBag: AuthorizationBag) {
    if (this.private) {
      if (!authorizationBag) return false;
      if (this.targetUsers && this.targetUsers.some(u => u.compareTo(authorizationBag.user))) {
        return true;
      }
      if (!this.targetGroups) return false;
      for (const g of this.targetGroups) {
        if (await g.isMember(authorizationBag.user)) {
          return true;
        }
      }

      return false;
    }

    return true;
  }

  // New methods relating to TypeORM changes for performance improvements
  static qbSearchText(searchText: string): Brackets {
    return new Brackets(searchTextQB => {
      searchTextQB.andWhere('notification.summary ILIKE :searchText OR notification.body ILIKE :searchText', {
        searchText: `%${searchText}%`,
      });
    });
  }

  static filterNotifications(userGroups: AuthGroup[], userId: string): Brackets {
    return new Brackets(filterQB => {
      filterQB.andWhere(
        new Brackets(privateQB => {
          privateQB.where('notification.private = false');
          if (userGroups?.length > 0) {
            privateQB.orWhere(sqb => {
              //subquery to get notification the user is targeted in
              const subQuery = sqb
                .subQuery()
                .select('notification.id')
                .from(Notification, 'notification')
                .leftJoin('notification.targetGroups', 'targetGroups')
                .where('targetGroups.id IN (:...userGroups)', { userGroups: userGroups.map(g => g.groupId) });
              return 'notification.id IN ' + subQuery.getQuery();
            });
          }

          if (userId) {
            privateQB.orWhere(sqb => {
              const subQuery = sqb
                .subQuery()
                .select('notification.id')
                .from(Notification, 'notification')
                .leftJoin('notification.targetUsers', 'targetUsers')
                .where(':userId IN (targetUsers.id)', { userId: userId });
              return 'notification.id IN ' + subQuery.getQuery();
            });
          }
        }),
      );
      //showing only already sent scheduled notifs
      filterQB.andWhere(sqb => {
        const subQuery = sqb
          .subQuery()
          .select('notification.id')
          .from(Notification, 'notification')
          .where('(notification.sendAt is not null AND notification.sendAt < :now)', {
            now: new Date().toISOString(),
          })
          .orWhere('notification.sendAt is null');
        return 'notification.id IN ' + subQuery.getQuery();
      });
    });
  }
}
