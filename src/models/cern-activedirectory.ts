import { AdUser } from './user';
import * as ActiveDirectory from 'activedirectory2';
import * as ldapEscape from 'ldap-escape';
import * as memoize from 'memoizee';

const AD_TIMEOUT = 5;

// Anonymous XLDAP
const CERN_XLDAP_config = {
  url: 'ldap://xldap.cern.ch',
  baseDN: 'dc=cern,dc=ch',
  timeout: AD_TIMEOUT * 1000,
  connectTimeout: AD_TIMEOUT * 1000,
  idleTimeout: AD_TIMEOUT * 1000,
  timeLimit: AD_TIMEOUT,
};

// Authenticated CERNDC, required for EGroups details
const CERN_AD_config = {
  url: 'ldap://cerndc.cern.ch',
  username: process.env.AD_USERNAME,
  password: process.env.AD_PASSWORD,
  baseDN: 'dc=cern,dc=ch',
  timeout: AD_TIMEOUT * 1000,
  connectTimeout: AD_TIMEOUT * 1000,
  idleTimeout: AD_TIMEOUT * 1000,
  timeLimit: AD_TIMEOUT,
};

export class EGroupRestrictions {
  identifier: string;
  postingGroups: string[];
  postingUsers: string[];
  owner: string;
  administratorEgroup: string;

  constructor(
    identifier: string,
    postingGroups: string[],
    postingUsers: string[],
    owner: string,
    administratorEgroup: string,
  ) {
    this.identifier = identifier;
    this.postingGroups = postingGroups;
    this.postingUsers = postingUsers;
    this.owner = owner;
    this.administratorEgroup = administratorEgroup;
  }
}

// Temporary XLDAP query for Mail aliases until Authz service API supports it
export class CERNActiveDirectory {
  static async getUser(mailTarget: string) {
    const filter = ldapEscape.filter`(&\
(|(mail=${mailTarget})(proxyAddresses=smtp:${mailTarget}))\
(objectClass=user)\
(|\
(memberOf=CN=cern-accounts-primary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-secondary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-service,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
))`;
    const opts = {
      filter: filter,
      attributes: ['cn', 'mail', 'mobile'],
      sizeLimit: 1,
    };

    const ad = new ActiveDirectory.promiseWrapper(CERN_XLDAP_config);

    const result = await ad.find(opts).catch(err => {
      console.error('CERNActiveDirectory.getUser: ERROR: ' + err);
      throw err;
    });

    if (result) {
      if (result.users.length > 0) {
        return new AdUser({
          username: result.users[0].cn,
          email: result.users[0].mail,
          mobile: result.users[0].mobile,
        });
      }
    }
  }

  static memoizedGetGroupRestrictions = memoize(CERNActiveDirectory.getGroupRestrictions, {
    maxAge: Number(process.env.CACHE_TOKEN_TTL || 5) * 1000, // means we need default 5 seconds for changes to manual membership to be visible
    preFetch: false, // Prefetch is useless if token is expired it will fail to verify
  });

  static async getGroupRestrictions(groupIdentifier: string): Promise<EGroupRestrictions | null> {
    const filter = ldapEscape.filter`(&(cn=${groupIdentifier})(objectClass=group))`;
    const opts = {
      filter: filter,
      attributes: ['cn', 'mail', 'dLMemSubmitPerms', 'authOrig', 'hideDLMembership', 'managedBy', 'owner'],
      sizeLimit: 1,
    };
    const ad = new ActiveDirectory.promiseWrapper(CERN_AD_config);
    const result = await ad.find(opts).catch(err => {
      console.error('CERNActiveDirectory.getGroup: ERROR: ' + err);
      throw err;
    });

    if (!result || result.groups.length != 1) {
      // any reason for no result?
      return null;
    }

    const ADGroup = result.groups[0];

    const hasRestrictions = !!(ADGroup.dLMemSubmitPerms || ADGroup.authOrig);
    if (!hasRestrictions) {
      return null;
    }

    let restrictionUsers;
    if (ADGroup.authOrig) {
      restrictionUsers = Array.isArray(ADGroup.authOrig)
        ? ADGroup.authOrig.map(user => {
            return user.replace('CN=', '').split(',')[0];
          })
        : [ADGroup.authOrig.replace('CN=', '').split(',')[0]];
    }

    let restrictionGroups;
    if (ADGroup.dLMemSubmitPerms) {
      restrictionGroups = Array.isArray(ADGroup.dLMemSubmitPerms)
        ? ADGroup.dLMemSubmitPerms.map(group => {
            return group.replace('CN=', '').split(',')[0];
          })
        : [ADGroup.dLMemSubmitPerms.replace('CN=', '').split(',')[0]];

      // Filter out current e-group and admin e-group
      restrictionGroups = restrictionGroups.filter(group => {
        return group != ADGroup.cn && group != 'egroup-mail-master';
      });
    }

    //const private = result.groups[0].hideDLMembership === 'TRUE' ? true : false,

    return new EGroupRestrictions(
      ADGroup.cn,
      restrictionGroups,
      restrictionUsers,
      ADGroup.managedBy && ADGroup.managedBy.replace('CN=', '').split(',')[0],
      ADGroup.owner && ADGroup.owner.replace('CN=', '').split(',')[0],
    );
  }

  static memoizedGetUserDepartment = memoize(CERNActiveDirectory.getUserDepartment, {
    maxAge: Number(process.env.CACHE_TOKEN_TTL || 5) * 1000, // means we need default 5 seconds for changes to manual membership to be visible
    preFetch: false, // Prefetch is useless if token is expired it will fail to verify
  });

  static async getUserDepartment(mailTarget: string) {
    const filter = ldapEscape.filter`(&\
(|(mail=${mailTarget})(proxyAddresses=smtp:${mailTarget}))\
(objectClass=user)\
(|\
(memberOf=CN=cern-accounts-primary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-secondary,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
(memberOf=CN=cern-accounts-service,OU=e-groups,OU=Workgroups,DC=cern,DC=ch)\
))`;
    const opts = {
      filter: filter,
      attributes: ['cn', 'mail', 'division'],
      sizeLimit: 1,
    };

    const ad = new ActiveDirectory.promiseWrapper(CERN_AD_config);

    const result = await ad.find(opts).catch(err => {
      console.error('CERNActiveDirectory.getUserDepartment: ERROR: ' + err);
      throw err;
    });

    if (result) {
      if (result.users.length > 0) {
        return result.users[0].division;
      }
    }
  }
}
