type LastStatistics = {
  channels_created: number;
  notifications_sent: number;
  users_active: number;
  users_active_per_department: Record<string, number>;
};

export class Statistics {
  channels: number;
  channels_public: number;
  channels_restricted: number;
  channels_internal: number;
  notifications: number;
  users: number;
  users_active: number;
  devices: number;
  preferences: number;
  mutes: number;
  categories: number;
  tags: number;
  last_day: LastStatistics = <LastStatistics>{};
  last_month: LastStatistics = <LastStatistics>{};

  top_channels_with_self_subscription: Record<string, number>;
  top_channels_with_notifications: Record<string, number>;
  top_latest_created_channels: Record<string, Date>;
  avg_notifications_per_channel: number;

  top_max_devices: Record<string, number>;
  top_max_preferences: Record<string, number>;
  top_max_mutes: Record<string, number>;
  avg_devices_per_user: number;
  avg_preferences_per_user: number;
  avg_mutes_per_user: number;

  active_users_per_department: Record<string, number>;
  channels_per_department: Record<string, number>;

  last_10_notifications_reach: Record<string, Record<string, number>>;
  last_100_user_reach_per_department: Record<string, number>;

  avg_users_reached_per_notification_weekly: number;
  avg_devices_reached_per_notification_weekly: number;
  total_end_notifications_weekly: number;
  total_end_notifications_weekly_across_devices: number;

  total_notifications_created_last_week: number;
  total_notifications_created_last_month: number;
}
