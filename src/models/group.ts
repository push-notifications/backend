import { Column, Entity, Unique, PrimaryColumn, ManyToMany } from 'typeorm';
import { CernAuthorizationService } from './cern-authorization-service';
import { Channel } from './channel';
import { User } from './user';
import { Notification } from './notification';
import { CERNActiveDirectory, EGroupRestrictions } from './cern-activedirectory';
import { ForbiddenError } from 'routing-controllers';

@Entity({ name: 'Groups' })
@Unique(['groupIdentifier'])
export class Group {
  @PrimaryColumn() id: string;
  @Column() groupIdentifier: string;

  @ManyToMany(type => Channel, channel => channel.groups)
  channelGroups: Channel[];

  @ManyToMany(type => Notification, notification => notification.targetGroups)
  notificationsTargeting: Notification[];

  constructor(group) {
    if (group) {
      this.id = group.id;
      this.groupIdentifier = group.groupIdentifier;
    }
  }

  async getMembers(): Promise<User[]> {
    return await CernAuthorizationService.getMembers(this.id);
  }

  async exists(): Promise<boolean> {
    this.id = await CernAuthorizationService.getGroupId(this.groupIdentifier);
    return this.id !== undefined;
  }

  async isMember(user: User): Promise<boolean> {
    if (!user) return false;
    return (await this.getMembers()).find(u => u.compareTo(user)) !== undefined;
  }

  async getRestrictions(): Promise<EGroupRestrictions | null> {
    return await CERNActiveDirectory.memoizedGetGroupRestrictions(this.groupIdentifier);
  }

  async validateEgroupPostingRestrictionsOrThrowError(
    userName: string,
    userGroups: any[],
    channel_id: string,
  ): Promise<void> {
    // Temp fix, hardcoded bypass until we implement exception similar to egroup-notification-master: https://e-groups.cern.ch/e-groups/Egroup.do?egroupName=egroup-notification-master
    // OKD4 PAAS Alerts: https://notifications.web.cern.ch/channels/871a63ea-ad54-46aa-bcb2-7ed3895f8f81/notifications
    // OKD4 PAAS-STG Alerts: https://notifications.web.cern.ch/channels/ee93f6f9-a491-419b-88da-403e3f95485a/notifications
    // Library newsletter: https://notifications.web.cern.ch/channels/1f7e6e8d-a356-4dcb-84da-45e87c740900/notifications
    const bypassPostingRestrictions = [
      '871a63ea-ad54-46aa-bcb2-7ed3895f8f81',
      'ee93f6f9-a491-419b-88da-403e3f95485a',
      '1f7e6e8d-a356-4dcb-84da-45e87c740900',
    ];
    if (process.env.OAUTH_CLIENT_ID == 'notifications-prod' && bypassPostingRestrictions.includes(channel_id)) {
      return;
    }

    // If restrictions are set, deny adding to people not in the restriction list or admins of the EGroup.
    const restrictions = await this.getRestrictions();
    console.debug('restrictions', restrictions);

    if (!restrictions) {
      return;
    }

    // Restrictions are set on this e-group
    // Check if current user trying to add is:
    //    - Owner or member of e-group admins
    //    - Listed in the restrictions, by username or member of another e-group (except e-group members)
    if (userName === restrictions.owner) {
      console.debug('authorized as e-group owner', userName);
      return;
    }

    if (
      restrictions.administratorEgroup &&
      userGroups.some(group => group.groupIdentifier === restrictions.administratorEgroup)
    ) {
      console.debug('authorized as e-group admins member', userName);
      return;
    }

    if (restrictions.postingUsers && restrictions.postingUsers.includes(userName)) {
      console.debug('authorized as e-group restrictionUser member', userName);
      return;
    }

    if (
      restrictions.postingGroups &&
      userGroups.some(group => restrictions.postingGroups.includes(group.groupIdentifier))
    ) {
      console.debug('authorized as e-group restrictionGroups member', userName);
      return;
    }

    throw new ForbiddenError(
      `E-group ${this.groupIdentifier} has posting restrictions and you don't have the rights to use it.`,
    );
  }
}
