import { Entity, CreateDateColumn, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "ServiceStatus" })
export class ServiceStatus {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column({ nullable: false })
  message: string;

  @CreateDateColumn()
  created: Date;

  // Validity in days
  @Column({ nullable: false, default: 3 })
  validity: number;

  constructor(servicestatus) {
    if (servicestatus) {
      this.id = servicestatus.id;
      this.message = servicestatus.message;
      this.created = servicestatus.created;
      this.validity = servicestatus.validity;
    }
  }
}
