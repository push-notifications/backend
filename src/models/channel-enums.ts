export enum Visibility {
  public = 'PUBLIC',
  internal = 'INTERNAL',
  restricted = 'RESTRICTED',
}

export enum SubscriptionPolicy {
  selfSubscription = 'SELF_SUBSCRIPTION',
  selfSubscriptionApproval = 'SELF_SUBSCRIPTION_APPROVAL',
  dynamic = 'DYNAMIC',
}

export enum SubmissionByEmail {
  administrators = 'ADMINISTRATORS',
  members = 'MEMBERS',
  email = 'EMAIL',
  egroup = 'EGROUP',
}

export enum SubmissionByForm {
  administrators = 'ADMINISTRATORS',
  members = 'MEMBERS',
  apikey = 'APIKEY',
}

export enum ChannelFlags {
  mandatory = 'MANDATORY',
  critical = 'CRITICAL',
}

export enum ChannelType {
  official = 'OFFICIAL',
  personal = 'PERSONAL',
}
