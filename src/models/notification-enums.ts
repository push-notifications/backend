const SendDateFormat = 'YYYY-MM-DDTHH:mm:ss.sssZ';
export { SendDateFormat };

export enum PriorityLevel {
  CRITICAL = 'CRITICAL',
  IMPORTANT = 'IMPORTANT',
  NORMAL = 'NORMAL',
  LOW = 'LOW',
}

export enum Source {
  email = 'EMAIL',
  web = 'WEB',
  api = 'API',
}

export enum Times {
  morning = 9,
  lunch = 13,
  afternoon = 17,
  night = 21,
}
