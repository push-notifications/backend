import { Entity, ManyToOne, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, RelationId } from 'typeorm';
import { User } from './user';

export enum DeviceType {
  BROWSER = 'BROWSER',
  APP = 'APP',
  MAIL = 'MAIL',
  PHONE = 'PHONE',
}

export const DefaultDevices = [DeviceType.PHONE, DeviceType.APP, DeviceType.MAIL];

export enum DeviceSubType {
  SAFARI = 'SAFARI',
  OTHER = 'OTHER',
  IOS = 'IOS',
  ANDROID = 'ANDROID',
  WINDOWS = 'WINDOWS',
  LINUX = 'LINUX',
  MAC = 'MAC',
  MATTERMOST = 'MATTERMOST',
  PRIMARY = 'PRIMARY',
  PHONE_VOICE = 'VOICE',
  PHONE_SMS = 'SMS',
  PHONE_MAIL2SMS = 'MAIL2SMS',
}

export enum DeviceStatus {
  OK = 'OK',
  EXPIRED = 'EXPIRED',
  BLACKLISTED = 'BLACKLISTED',
  INVALID = 'INVALID',
}

@Entity('Devices')
export class Device {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: false })
  name: string;

  @ManyToOne(type => User, user => user.devices)
  user: User;

  @RelationId((device: Device) => device.user)
  userId: string;

  @Column({ nullable: true })
  info: string;

  @Column('varchar')
  type: DeviceType;

  @Column('varchar', {
    nullable: true,
  })
  subType: DeviceSubType;

  // Contains a changeable random anonymous id, mainly for Apple devices
  @Column({ type: 'uuid', nullable: true })
  uuid: string;

  @Column({ nullable: false })
  token: string;

  @Column({ enum: DeviceStatus, default: DeviceStatus.OK })
  status: DeviceStatus;

  // Trim token in case it's between quotes (mostly Safari business)
  @BeforeUpdate()
  @BeforeInsert()
  async trimToken(): Promise<void> {
    if (!this.token) return;
    if (this.token.charAt(0) === '"' && this.token.charAt(this.token.length - 1) === '"')
      this.token = this.token.substr(1, this.token.length - 2);
  }

  constructor(device) {
    if (device) {
      this.name = device.name;
      this.user = device.user;
      this.info = device.info;
      this.type = device.type;
      this.subType = device.subType;
      this.uuid = device.uuid;
      this.token = device.token;
      this.status = device.status;
    }
  }
}
