import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments } from 'class-validator';
import {SubmissionByEmail} from "./channel-enums";

@ValidatorConstraint({ name: 'alphaNumeric', async: false })
export class AlphaNumericLowercase implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    // Allow only 0-9a-z-_
    return !(/[^0-9a-z-_]/.test(text));
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Text ($value) contains invalid characters, only [a-z][0-9][-_] are allowed!';
  }
}

// Ensures an email is set if notification from email is set
@ValidatorConstraint({ name: 'fromEmailSet', async: false })
export class FromEmailSet implements ValidatorConstraintInterface {
  validate(submissionByEmail: string[], args: ValidationArguments) {
    if (submissionByEmail && submissionByEmail.includes(SubmissionByEmail.email)) {
      if (args && args.object["incomingEmail"]) {
        return true;
      }
      return false;
    }
    if (submissionByEmail && submissionByEmail.includes(SubmissionByEmail.egroup)) {
      if (args && args.object["incomingEgroup"]) {
        return true;
      }
      return false;
    }

    return true;
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Notification from e-mail or e-group was set without e-mail or e-group address set!';
  }
}

@ValidatorConstraint({ name: 'alphaNumericPunctiationChannelName', async: false })
export class AlphaNumericPunctuationChannelName implements ValidatorConstraintInterface {
  validate(text: string, args: ValidationArguments) {
    // Allow only 0-9a-zA-Z.,:;-_!?()[]\\#@"'=#@/& 
    return ( !(/[^0-9a-zA-Z.,:;\-_!?()[\]\\#@"'=#@/& ]/.test(text)));
  }

  defaultMessage(args: ValidationArguments) {
    // here you can provide default error message if validation failed
    return 'Text ($value) contains invalid characters, only [a-z][A-Z][0-9][.,:;-_!?()[]#@"\'=#@/& ] are allowed.';
  }
}