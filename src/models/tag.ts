import { Entity, PrimaryGeneratedColumn, Column, Index, ManyToMany } from "typeorm";

@Entity('Tags')
export class Tag {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Index()
  @Column({ unique: true, nullable: false })
  name: string;

  constructor(tag) {
    if (tag) {
      this.name = tag.name;
    }
  }
}
