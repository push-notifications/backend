import { Entity, Index, JoinColumn, ManyToOne, PrimaryColumn, RelationId } from 'typeorm';
import { Channel } from './channel';

import { User } from './user';

export enum UserChannelCollectionType {
  FAVORITE = 'FAVORITE',
}

@Entity('UserChannelCollection')
export class UserChannelCollection {
  @PrimaryColumn('varchar')
  type: UserChannelCollectionType;

  @PrimaryColumn()
  @RelationId((userChannelCollection: UserChannelCollection) => userChannelCollection.user)
  userId: string;

  @PrimaryColumn()
  @RelationId((userChannelCollection: UserChannelCollection) => userChannelCollection.channel)
  channelId: string;

  @ManyToOne(type => User)
  @JoinColumn()
  @Index()
  user: User;

  @Index()
  @ManyToOne(type => Channel)
  @JoinColumn()
  channel: Channel;

  constructor(collection: UserChannelCollection) {
    if (collection) {
      this.channel = collection.channel;
      this.type = collection.type;
      this.user = collection.user;
    }
  }
}
