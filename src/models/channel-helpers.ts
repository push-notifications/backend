import * as memoize from 'memoizee';

export class ChannelHelpers {
  static groupsAreInUserGroups = function (channelGroups, userGroups) {
    for (const group of userGroups) {
      if (channelGroups.some(g => g.id === group.groupId)) {
        return true;
      }
    }

    return false;
  };

  static adminGroupIsInUserGroups = function (adminGroup, userGroups) {
    if (userGroups.some(g => g.groupId === adminGroup.id)) {
      return true;
    }
    return false;
  };

  static userIsInMembers = function (channelMembers, user) {
    if (channelMembers.some(m => m.username === user.username || m.email === user.email)) {
      return true;
    }
    return false;
  };

  static memoizedIsInGroups = memoize(ChannelHelpers.groupsAreInUserGroups, {
    maxAge: Number(process.env.CACHE_TOKEN_TTL || 5) * 1000, // means we need default 5 seconds for changes to manual membership to be visible
    preFetch: false, // Prefetch is useless if token is expired it will fail to verify
  });

  static memoizedIsInAdminGroups = memoize(ChannelHelpers.adminGroupIsInUserGroups, {
    maxAge: Number(process.env.CACHE_TOKEN_TTL || 5) * 1000, // means we need default 5 seconds for changes to manual membership to be visible
    preFetch: false, // Prefetch is useless if token is expired it will fail to verify
  });

  static memoizedIsInMembers = memoize(ChannelHelpers.userIsInMembers, {
    maxAge: Number(process.env.CACHE_TOKEN_TTL || 5) * 1000, // means we need default 5 seconds for changes to manual membership to be visible
    preFetch: false,
  });
}
