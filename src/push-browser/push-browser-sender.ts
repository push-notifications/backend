import * as webpush from 'web-push';

export async function testBrowserPush(endpoint: string, encoding?: string): Promise<string> {
  return await sendBrowserPush(
    endpoint,
    'Notification Sample',
    'This is a notification sample sent by CERN Notification Service.',
    'http://home.cern',
    'https://cds.cern.ch/images/CMS-PHO-GEN-2008-026-1/file?size=large',
    encoding,
  );
}

export async function sendBrowserPush(
  endpoint: string,
  title: string,
  message: string,
  url?: string,
  image?: string,
  encoding?: string,
): Promise<string> {
  // setting VAPID keys
  webpush.setVapidDetails(
    'mailto:' + process.env.VAPID_EMAIL,
    process.env.VAPID_PUBLICKEY,
    process.env.VAPID_PRIVATEKEY,
  );

  // function to send the notification to the subscribed device
  const sendNotification = async (subscription, dataToSend, encoding?): Promise<string> => {
    const options = encoding
      ? {
          contentEncoding: encoding,
        }
      : undefined;

    return await webpush
      .sendNotification(subscription, dataToSend, options)
      .then(() => {
        return 'OK';
      })
      .catch(error => {
        if (error.statusCode === 410) {
          console.debug('CLEANUP Needed: ' + error.body + ' ' + error.endpoint);
          return 'EXPIRED';
        } else {
          console.error(error);
          return 'ERROR';
        }
      });
  };

  //const message = 'Simple piece of body text.\nSecond line of body text :)';
  const blob = JSON.stringify({
    title: title,
    message: message,
    url: url,
    image: image,
  });

  console.debug('Sending push to: ' + endpoint);
  return await sendNotification(JSON.parse(endpoint), blob, encoding);
}
