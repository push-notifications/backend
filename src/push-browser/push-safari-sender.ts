import * as apn from 'node-apn';

export function testSafariPush(endpoint: string) {
  sendSafariPush(
    endpoint,
    'Notification Sample',
    'This is a notification sample sent by CERN Notification Service.',
    'http://home.cern',
    'https://cds.cern.ch/images/CMS-PHO-GEN-2008-026-1/file?size=large',
  );
}

export function sendSafariPush(endpoint: string, title: string, message: string, url?: string, image?: string) {
  const options = {
    // Soon we'll have to switch to TOKEN auth but for now we can't get a new TOKEN
    // TBC with Apple Team
    // token: {
    //   key: "path/to/APNsAuthKey_XXXXXXXXXX.p8",
    //   keyId: "key-id",
    //   teamId: "developer-team-id"
    // },
    // Until then we use the old certificate based auth
    cert: process.env.APPLE_SAFARI_PUSH_CERT, // Certificate
    key: process.env.APPLE_SAFARI_PUSH_CERT_KEY, // Private Key
    ca: [
      // https://developer.apple.com/news/?id=7gx0a2lp (Change in March 2021)
      process.env.APPLE_SAFARI_PUSH_ROOTCERT, // Root certs to trust for TLS connection to apple servers
    ],
    // For Safari push notifications always set production to true
    production: true,
  };

  const apnProvider = new apn.Provider(options);
  const deviceToken = endpoint;

  const note = new apn.Notification();

  // setters not working, unclear
  //note.title = title;
  //note.body = message;
  //note.action = "Read more";
  // Do all manually
  if (typeof note.aps.alert !== 'object') note.aps.alert = { body: note.aps.alert };
  note.aps.alert.body = message;
  note.aps.alert.title = title;
  note.aps.alert.action = 'Read more';

  // pass url without https://
  // because url defined in the pushPackage is https://%@
  note.urlArgs = ['home.cern'];

  console.debug('Sending safari push to: ' + endpoint);
  apnProvider.send(note, deviceToken).then(result => {
    console.debug(JSON.stringify(result));
    // Careful here, it seems it does not always exit as there is pooling
    // TBC on prototype server if sockets stay open and/or pile up
  });
}
