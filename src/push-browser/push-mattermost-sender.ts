import { BadRequestError } from 'routing-controllers';
import { getWithTimeout, postWithTimeout } from '../utils/fetch';

export async function testMattermost(endpoint: string) {
  await sendMattermost(
    endpoint,
    'Notification Sample',
    'This is a notification sample sent by CERN Notification Service.',
    'http://home.cern',
    'https://cds.cern.ch/images/CMS-PHO-GEN-2008-026-1/file?size=large',
  );
}

export async function sendMattermost(endpoint: string, title: string, message: string, url?: string, image?: string) {
  // Get notification (current) user ID http://our-mattermost-url.com/api/v4/users/me
  const notificationsUser = await getWithTimeout(`https://${process.env.MATTERMOST_SERVER}/api/v4/users/me`, {
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${process.env.MATTERMOST_TOKEN}`,
    },
  });

  // Get target user by email http://your-mattermost-url.com/api/v4/users/email/{email}
  const targetUser = await getWithTimeout(`https://${process.env.MATTERMOST_SERVER}/api/v4/users/email/${endpoint}`, {
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${process.env.MATTERMOST_TOKEN}`,
    },
  });

  if (!(notificationsUser?.id && targetUser?.id))
    throw new BadRequestError('Failed to test Mattermost, unable to identify target account.');

  // Create direct message channel http://your-mattermost-url.com/api/v4/channels/direct
  const dmObject = await postWithTimeout(`https://${process.env.MATTERMOST_SERVER}/api/v4/channels/direct`, {
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${process.env.MATTERMOST_TOKEN}`,
    },
    body: JSON.stringify([notificationsUser.id, targetUser.id]),
  });

  // Build Markdown message
  const blob = `## [${title}](${url})\r\n${message}\r\n![image](${image})\r\n[${url}](${url})`;

  console.debug('Sending mattermost to: ' + endpoint);
  // Post Message http://your-mattermost-url.com/api/v4/posts
  const postResult = await postWithTimeout(`https://${process.env.MATTERMOST_SERVER}/api/v4/posts`, {
    headers: {
      'Content-Type': 'application/json',
      authorization: `Bearer ${process.env.MATTERMOST_TOKEN}`,
    },
    body: JSON.stringify({
      channel_id: dmObject.id,
      message: blob,
    }),
  });
}
