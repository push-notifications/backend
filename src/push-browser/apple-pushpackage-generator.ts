import * as fs from "fs";
import * as path from "path";
import * as pushLib from "safari-push-notifications";

export function createPushPackage(authenticationToken: string) {

    // pushPackage builder
    // https://github.com/MySiteApp/node-safari-push-notifications

    var websiteJson = pushLib.websiteJSON(
        process.env.APPLE_SAFARI_PUSH_WEBSITENAME, 
        process.env.APPLE_SAFARI_PUSH_WEBSITEPUSHID, 
        process.env.APPLE_SAFARI_PUSH_ALLOWEDDOMAINS.split(','), 
        process.env.APPLE_SAFARI_PUSH_URLFORMATSTRING, // https://%@
        authenticationToken,  
        process.env.APPLE_SAFARI_PUSH_WEBSERVICEURL, // Must be https!
    );

    // Returns a stream
    return pushLib.generatePackage(
        websiteJson,
        path.join(__dirname, 'safari_assets'), // Folder containing the iconset
        process.env.APPLE_SAFARI_PUSH_CERT, 
        process.env.APPLE_SAFARI_PUSH_CERT_KEY,
        process.env.APPLE_SAFARI_PUSH_CERT_KEY_INTERMEDIATE
    );
}
