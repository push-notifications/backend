export class Configuration {
    public static activeMQ: {
      port: string;
      host: string;
      connectHeaders: {
        login: string;
        passcode: string
      }
    };

    static load() {
        this.activeMQ = {
            host: process.env.ACTIVEMQ_HOST,
            port: process.env.ACTIVEMQ_PORT,
            connectHeaders: {
                login: process.env.ACTIVEMQ_LOGIN,
                passcode: process.env.ACTIVEMQ_PASSWORD,
            },
        }
    }
}