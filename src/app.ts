/* eslint-disable */
// lint disable because typescript does not recognize Response and Request properties

import { validationMetadatasToSchemas } from 'class-validator-jsonschema';
import 'reflect-metadata'; // this shim is required
import { ExpressMiddlewareInterface, getMetadataArgsStorage, Middleware, useExpressServer } from 'routing-controllers';
import { routingControllersToSpec } from 'routing-controllers-openapi';
import * as http from 'http';
import * as express from 'express';
import { AuthorizationChecker } from './middleware/authorizationChecker';
import { Configuration } from './config/configuration';
import * as sentry from './log/sentry';
import * as swaggerUiExpress from 'swagger-ui-express';
import { NotificationsController } from './controllers/notifications/controller';
import { DevicesController } from './controllers/devices/controller';
import { ChannelsController } from './controllers/channels/controller';
import { ChannelRecommendationController } from './controllers/channel-recommendations/controller';
import * as fs from 'fs';
import * as https from 'https';
import { audit } from './middleware/audit';
import * as oa from 'openapi3-ts';

Configuration.load();

// Note https://github.com/typestack/class-transformer/issues/563
import { defaultMetadataStorage as classTransformerDefaultMetadataStorage } from 'class-transformer/cjs/storage';
import { AppDataSource } from './app-data-source';

const routingControllersOptions = {
  controllers: [DevicesController, NotificationsController, ChannelsController, ChannelRecommendationController],
};

// Check ROLES are defined in ENV for controller Authorizations
// If not exit now to prevent security issues on controllers with no authz
if (!(process.env.INTERNAL_ROLE && process.env.VIEWER_ROLE && process.env.SUPPORTER_ROLE))
  throw new Error('Missing ROLE(s) in Environment, controllers cannot run without proper Authorization, stopping.');

// check valid origin URLs
const statOrigins = ['http://localhost', 'https://localhost:3000'];
const origins =
  'CORS_ORIGINS_ALLOWED' in process.env
    ? process.env.CORS_ORIGINS_ALLOWED.split(',')
      .map(origin => {
        try {
          const originURL = new URL(origin);
          return originURL.origin.endsWith('.cern.ch') ? origin : undefined;
        } catch {
          console.debug('Invalid origin: ', origin);
          return undefined;
        }
      })
      .filter(origin => origin !== undefined)
      .concat(statOrigins)
    : statOrigins;

//create express server
const app = express();

// Sentry initialize
sentry.sentryInit(app);
sentry.sentryHandleRequests(app);

// eslint-disable-next-line @typescript-eslint/no-var-requires
app.use(require('express-status-monitor')());

// Nicely formatted json (debug only)
if (process.env.NODE_ENV == 'development')
  app.set('json spaces', 2)

let server;

// Parse class-validator classes into JSON Schema:
const schemas = validationMetadatasToSchemas({
  classTransformerMetadataStorage: classTransformerDefaultMetadataStorage,
  refPointerPrefix: '#/components/schemas/',
});

// Parse routing-controllers classes into OpenAPI spec:
const storage = getMetadataArgsStorage();
const spec = routingControllersToSpec(storage, routingControllersOptions, {
  components: {
    schemas: schemas as Record<string, oa.SchemaObject | oa.ReferenceObject>,
    securitySchemes: {
      oauth2: {
        type: 'oauth2',
        description:
          'Implicit Authentication. Should redirect back from CERN SSO, or skip it entirely if already authenticated.',
        flows: {
          implicit: {
            // The notification[-dev|qa] oauth registration must be implicit in application-portal,
            // but not public: needs to be done by Auth team  (see Hannah Short)
            // Then client-id to use here is the app itself, like notification-dev
            authorizationUrl: process.env.OAUTH_AUTHORIZATION_URI,
            scopes: [],
          },
        },
      },
    },
  },
  info: {
    description: 'API documentation for the CERN Notification Service',
    title: 'Notification Service API',
    version: '1.0.0',
  },
  servers: [
    {
      url: process.env.SWAGGER_SERVER,
    },
  ],
});

const swaggerOptions = {
  swaggerOptions: {
    url: '/swagger/swagger.json',
    oauth2RedirectUrl: process.env.SWAGGER_REDIRECT_URL,
    oauth: {
      //clientId: "notifications-dev-clientapi",
      // Use the app client-id instead, that must be configured implicit but not public (see above)
      clientId: process.env.OAUTH_CLIENT_ID,
    },
    filter: true,
  },
};

app.get('/swagger/swagger.json', (req, res) => res.json(spec));
app.use('/swagger', swaggerUiExpress.serveFiles(null, swaggerOptions), swaggerUiExpress.setup(null, swaggerOptions));

// Render spec on root:
app.get('/', (_req, res) => {
  res.json(spec);
});

if (process.env.NODE_ENV == 'development') {
  const httpsOptions = {
    cert: fs.readFileSync('./localhost.pem'),
    key: fs.readFileSync('./localhost-key.pem'),
  };
  server = https.createServer(httpsOptions, app);
  console.debug('Creating server on HTTPS');
} else {
  server = http.createServer(app);
  console.debug('Creating server on HTTP');
}

@Middleware({ type: 'after' })
export class DebugMiddleware implements ExpressMiddlewareInterface {
  async use(req: any, _res: any, next: any): Promise<void> {
    const auditData = {
      // @ts-ignore
      originalUrl: req.originalUrl, // @ts-ignore
      protocol: req.protocol, // @ts-ignore
      body: req.body, // @ts-ignore
      method: req.method,
    };
    //console.debug('after: ', auditData);
  }
}

@Middleware({ type: 'before' })
export class AuditMiddleware implements ExpressMiddlewareInterface {
  async use(req: any, _res: any, next: any): Promise<void> {
    await audit(req, _res, next).catch(err => {
      console.error('etcd error, continuing without auditing:', err.message);
      next();
    });
  }
}

// creates express app, registers all controller routes and returns you express app instance
useExpressServer(app, {
  cors: {
    origin: origins,
    optionsSuccessStatus: 200,
    credentials: true,
  },
  controllers: [__dirname + '/controllers/*/controller.ts', __dirname + '/controllers/*.ts'], // we specify controllers we want to use
  authorizationChecker: AuthorizationChecker.check,
  middlewares: [AuditMiddleware, DebugMiddleware],
});

sentry.sentryHandleErrors(app);

// Create db connection and run express application on port 8080
AppDataSource.initialize()
  .then(() => {
    server.listen(8080, () => console.debug('Server running on port 8080'));
  })
  .catch(error => console.debug(error));
