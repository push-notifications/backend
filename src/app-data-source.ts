// Create db connection and run express application on port 8080
import { DataSource } from 'typeorm';
import * as os from 'os';

export const AppDataSource = new DataSource({
  type: 'postgres',
  host: process.env.TYPEORM_HOST,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  schema: process.env.TYPEORM_SCHEMA,
  port: Number(process.env.TYPEORM_PORT),
  synchronize: Boolean(process.env.TYPEORM_SYNCHRONIZE === 'true' || false),
  logging: Boolean(process.env.TYPEORM_LOGGING === 'true' || false),
  entities: [process.env.TYPEORM_ENTITIES],
  uuidExtension: 'pgcrypto',
  connectTimeoutMS: Number(process.env.TYPEORM_CONNECT_TIMEOUT || 10) * 1000,
  maxQueryExecutionTime: Number(process.env.TYPEORM_MAX_QUERY_EXEC_TIMEOUT || 10) * 1000,
  poolSize: Number(process.env.TYPEORM_POOL_SIZE || 20),
  extra: {
    max: Number(process.env.TYPEORM_POOL_SIZE || 20),
  },
  applicationName: `${process.env.APP_NAME} - ${os.hostname()}`,
  subscribers: ['src/models/*.ts'],
});
