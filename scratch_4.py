import requests
import json

url = "https://localhost:8080/notifications"

payload = json.dumps({
  "target": "6f1cb518-3c7c-4da5-b062-c43ca63ec8d5",
  "summary": "test",
  "priority": "IMPORTANT",
  "body": "<html><style>should be removed</style><p>Test</p></html>"
})
headers = {
  'Authorization': 'Bearer ck_6f1cb518-3c7c-4da5-b062-c43ca63ec8d5_b16e3550-84a1-4374-be4c-25aa2e9ea1c1',
  'Content-Type': 'application/json'
}

for n in range(10):
  requests.request("POST", url, headers=headers, data=payload, verify=False)



COUNT    | DISKZISE
-----------------------------------------------------
     749 | 1G
     430 | 2000Mi
     363 | 1Gi
     133 | 2Gi
      64 | 3Gi
      43 | 4Gi
      29 | 5Gi
      23 | 6Gi
      22 | 10Gi
      15 | 2G
      12 | 16Gi
      10 | 7Gi
      10 | 9Gi
       8 | 8Gi
       7 | 12Gi
       7 | 19Gi
       7 | 3G
       6 | 4.0Gi
       4 | 24Gi
       4 | 251Gi
       4 | 27Gi
       4 | 32Gi
       4 | 39Gi
       4 | 56Gi
       3 | 190Gi
       3 | 26Gi
       3 | 40Gi
       2 | 100Gi
       2 | 11Gi
       2 | 13G
       2 | 14Gi
       2 | 20Gi
       2 | 3000Mi
       2 | 4G
       1 | 1.5Gi
       1 | 1000Gi
       1 | 2.0Gi
       1 | 5.0Gi
       1 | 60Gi
       1 | 80Gi
       1 | 8G
       1 | 9.0Gi
-----------------------------------------------------

