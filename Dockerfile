FROM registry.cern.ch/docker.io/library/node:18-alpine3.16

RUN apk add python3 alpine-sdk

WORKDIR /usr/src/app
COPY --chown=node:root . .

RUN npm install && chown -R node:root /usr/src/app && chmod -R 775 /usr/src/app
USER node
ENV HOME /usr/src/app

CMD ["npm","run","start:prod"]

EXPOSE 8080
