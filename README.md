# Push notifications backend

## Install instructions

Requirements:
- npm
- node:lts
- docker

### Automatically Fix Code in Editor

For a good developer experience, it's useful to setup your editor to automatically run ESLint's automatic fix command (i.e. eslint --fix) whenever a file is saved. 

#### JetBrains IDE's
`eslint` comes out of the box and automatic configuration should be enabled as follow [webstorm-docs](https://www.jetbrains.com/help/webstorm/eslint.html#ws_js_eslint_automatic_configuration). It's the same for all jetbrains ide's that support js files, eg pycharm...

#### VS Code
For VS Code, it is necessary to add the ESLint Plugin and a config to the ```settings.json``` file in VS Code for automatic fixing on save:

VS Code [ESLint Plugin](https://github.com/Microsoft/vscode-eslint):
```yaml
Name: ESLint
Id: dbaeumer.vscode-eslint
Description: Integrates ESLint JavaScript into VS Code.
Version: 2.2.2
Publisher: Microsoft
VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
```
Configuration for Linting on Save:

```json
{
  "editor.codeActionsOnSave": {
    "source.fixAll.eslint": true
  },
}`
```
<br/>

### Local app + (Docker Postgres and ActiveMQ)


**Fill your local DB with real data**

In order to work with real data, you can fill your Postgres container's DB with a copy of the current data from the [development environment](https://openshift.cern.ch/console/project/notifications-dev/overview). Before deploying the local env, you can `pg_dump` data by doing:

  ```bash
  make dbdump
  ```

Notice that on the terminal some errors will appear since this database has inserts in other schemas that are not relevant for the local deployment.

Once `schema.sql` contains the DB data, you can deploy the env running:
```bash
make env
```
Access https://localhost:8080/api/ping


### Local complete infra (backend + web-portal + notifications-routing + notifications-consumer(s))
- backend:
  - fix your ```.env.staging``` environment file
  - ```make env-shared-staging```
- web-portal
  - ```npm start```
- notifications-routing:
  - ```make env-shared```
- notifications-consumer:
  - ```make env-shared```


### Full docker

Requirements:
- docker

Run instructions:

```bash
make env-full
```

Access http://localhost:8080/api/ping

## Environment Variables

### Add allowed origins

  `CORS_ORIGINS_ALLOWED` environment variable holds a list of allowed origins delimited by `,` . This list is used to configure the `Access-Control-Allow-Origin` response header. It can be managed through the [notification-infra project](https://gitlab.cern.ch/push-notifications/notifications-infra).

### VAPID keys for browser Push
To (re)generate the VAPID keys required for push:
```
web-push generate-vapid-keys
```

## Service Status Message
The Web-Portal will display a banner if a valid Service Status entry is available in the DB.
Messages added have a default validity of 3 days after creation date. A different validity can be set at creation time.
If multiple valid messages are present, only the latest will show.

Entries management: via OpenShift ```backend-internal``` pod using ```curl```:
- Get current message if any: ```curl http://localhost:8080/servicestatus```
- Get all messages: ```curl http://localhost:8080/servicestatus/all```
- Delete one message: ```curl -X DELETE http://localhost:8080/servicestatus/:serviceStatusId```
- Add a new message, with default or specific validity: 
    - ```curl -X POST http://localhost:8080/servicestatus -H "Content-Type: application/json" --data '{"message":"Service will be upgraded tonight at 20pm"}'```
    - ```curl -X POST http://localhost:8080/servicestatus -H "Content-Type: application/json" --data '{"message":"Service will be upgraded tonight at 20pm", "validity": 1}'```

## Short URL

Channel's short URLs will be created during channel creation, channel editing and notification sending.
In case of short url service disruption, the flow will continue. For recovery and initial setup of existing channels, a subscriber has been registered.

[Web Redirector API](https://gitlab.cern.ch/webservices/web-redirector-v2/-/blob/master/app/api/README.md)
